<?php 
	include "nucleo/padroes_demonstra/header.php";
?>
<!--
<div class="page-header">
    <h1>Demonstra <small></small></h1>
</div>
-->
<div class="page-header">
	<h1>Demonstra</h1>
</div>

<p class="nome_ambiente text-right"><small><b>Versão:</b> 3.0<br/><b>Ambiente:</b></small></p>

<br/><br/>

<a name="novo_tutorial" id="novo_tutorial" class="btn btn-primary btn-block btn-large" href="#container_criar_novo_tutorial" role="button" data-toggle="modal">Criar novo tutorial</a>

<div id="container_criar_novo_tutorial" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="container_criar_novo_tutorialLabel" aria-hidden="true" >
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="container_criar_novo_tutorialLabel">Novo tutorial</h3>
	</div>
	<form action="<?php echo $dir_raiz."nucleo/cria_tutorial.php"; ?>" method="post" enctype="multipart/form-data" id='form_cadastro_tutorial'>
		<div class="modal-body">
			  <fieldset class="dados_tutorial" id="dados_tutorial">
				<p><label for="nome_sistema">Nome do sistema</label><input type="text" name="nome_sistema" id="nome_sistema" ></p>
				<p><label for="nome_responsavel">Responsável pela criação</label><input type="text" name="nome_responsavel" id="nome_responsavel" ></p>
				<!--<p><label for="apresentacao">Apresentação do tutorial</label><textarea class="curta" name="apresentacao_tutorial" id="apresentacao_tutorial" ></textarea></p>-->
			  </fieldset>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
			<button class="btn btn-primary" name='cria_tutorial'>Criar tutorial</button>
		</div>
	</form>	
</div>	

<br/><br/><br/>
	

<table id='tabela_catalogo'>
		<thead>
			<tr>
				<th>Catálogo de tutoriais</th>
			</tr>
		</thead>
		<tbody>
		<?php
		
		// pega o endereço do diretório 
		$diretorio = "DEMOS";
		// abre o diretório
		$ponteiro  = opendir($dir_raiz.$diretorio);
	
		// monta os vetores com os itens encontrados na pasta
		while ($nome_itens = readdir($ponteiro)) 
		{
			if($nome_itens!="."&&$nome_itens!="..")
			{
				$cursos[]=$nome_itens;
			}
		}
                if(!empty($cursos)){
  		sort($cursos);
  		foreach ($cursos as $nome_itens) 
  		
  		{	
  			//busca no xml mais recente o nome completo 
  			$recente = busca_xml_mais_recente($nome_itens, $dir_raiz);
  			$conteudo_arquivo = le($recente);
  			
  			//encontra o rótulo do sistema
  			$pattern = '/rotulo_sistema=[\'|"](.|\r\n)*?[\'|\"]/';
  			$xml_novo = preg_match($pattern,$conteudo_arquivo,$matches);
  			$rotulo_sistema = preg_split("[\'|\"]",$matches[0]);
  			//echo $rotulo_sistema[1]."<--"."<br>";	
  			
  			//encontra o responsavel pelo tutorial
  			$pattern = '/responsavel=[\'|"](.|\r\n)*?[\'|\"]/';
  			$xml_novo = preg_match($pattern,$conteudo_arquivo,$matches);
  			$responsavel = preg_split("[\'|\"]",$matches[0]);
  			
  			//data da última edição
  			$partes = explode('/', $recente);
  			$ultima_edicao = end($partes);
  			$partes = explode('.xml', $ultima_edicao);
  			$ultima_edicao = $partes[0];
  			$ultima_edicao = str_replace('-','',$ultima_edicao);			
  			$ultima_edicao = date('d/m/Y H:i:s', strtotime($ultima_edicao));	
  			//echo date('d/m/Y H:i:s', strtotime($ultima_edicao))." - ".$ultima_edicao."<br>";	
  			
  			echo "<tr>
  					<td>					  
  					  <a id='link_".$nome_itens."' class='link_tutorial' href='".$dir_raiz."DEMOS/".$nome_itens."/professor/' data-toggle='popover' data-content='<small><b>Última edição:</b> ".$ultima_edicao."</small><br/><small><b>Responsável:</b> ".$responsavel[1]."</small>' data-original-title='".$rotulo_sistema[1]."'>".$rotulo_sistema[1]."</a>
  					  
  					  <div id='".$nome_itens."' class='catalogo_ferramentas_tutorial modal hide fade' tabindex='-1' role='dialog' aria-labelledby='".$nome_itens."Label' aria-hidden='true'>
  						<div class='modal-header'>
  							<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
  							<h3 id='".$nome_itens."Label'>".$rotulo_sistema[1]."</h3>
  						</div>
  						<div class='modal-body'>
  							<div class='alert alert-success'>
  								<small><h4>Tutorial criado com sucesso!</h4>
  								Clique no botão abaixo para iniciar a edição.</small>
  							</div>
  							<div class='text-center'>
  								<p class='btn-group'>
  									<a class='btn btn-primary btn-large' href='".$dir_raiz."DEMOS/".$nome_itens."/professor'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Modo professor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
  								</p>
  							</div>	
  						</div>
  						<div class='modal-footer'>
  							<button class='btn' data-dismiss='modal' aria-hidden='true'>Fechar</button>
  						</div>
  					  </div>
  					</td>
  				  </tr>";
  		  }
  		}
		
		?>
	</tbody>
	<tfoot>
	</tfoot>
</table>			
				



	
<?php include "nucleo/padroes_demonstra/footer.php"; ?>
