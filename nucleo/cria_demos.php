<?php 

//obs: require_once no lugar do include para evitar que os arquivos sejam chamados mais de uma vez (em arquivos diferentes)
require_once("padroes_demonstra/header.php");



//Recebe dados do formulário. se foi selecionada uma demo já existente, atualiza o xml e transforma de novo. se não, cria as pastas padrão e então processa como tutorial_existente. 

//
if(isset($_POST["tutorial_existente"]))
{
	
	
	//encontra o nome_curto da demo e o Id da demo a ser atualizada. Se não tem Id, cria uma nova.
		//o nome curto deve ser igual ao nome do arquivo zip. por enquanto. 
	$nome_arquivo_zip = explode(".",$_FILES["imagens_base"]["name"]);
	$nome_curto_demo = $nome_arquivo_zip[0]; 
	$sistema = $_POST["tutorial_existente"];
	//echo "sdf".$sistema;
	//abre o zip com as imagens, grava no diretório de imagens/nome_curto . Função retorna array com nome de arquivos ordenados pela ordem zipada --> ver a ordem.	
	$lista_arquivos = grava_imagens($sistema, $_FILES["imagens_base"], $dir_raiz);
	
	$filetime2 = busca_xml_mais_recente($sistema, $dir_raiz);

	//abre xml mais recente
	$arquivo = fopen($filetime2, "r");
	$conteudo_arquivo = fread($arquivo, filesize($filetime2));
	fclose($arquivo);
	
	//extrai do xml nós <interacao> e os distribui em um array
	$pattern = '/<interacao>(.|\r\n)*?<\/interacao>/i';
	$xml_novo = preg_match_all($pattern,$conteudo_arquivo,$matches);
	$xml_form = implode(",", $matches[0]);
	
	
	//echo "<textarea>$conteudo_arquivo</textarea>";
	///////////////////////////colocar Ids novos
	//encontra valor do maior <Id_interacao>
	$pattern = '/<interacao>(.|\r\n)*?<\/interacao>/i';
	$matches = array();
	preg_match_all($pattern, $conteudo_arquivo, $matches);
	$array_nohs= $matches[0];
	$filetime22=count($array_nohs);
	//echo "<textarea>".$array_nohs[$filetime22-1]."</textarea>";
	
	

	//pegar ids do nó
	$noh = $array_nohs[$filetime22-1];
	
	//echo "<textarea>".$array_nohs[$filetime22-1]."</textarea>";
	//obtem o id
		$i = explode('<Id_modulo><![CDATA[',$noh);
	//echo "iii".$i[1];
		$j = explode(']]></Id_modulo>', $i[1]);
		$id = $j[0];
	$novo_id_modulo = $id+1;
	
	//obtem o id
		$i = explode('<Id_topico><![CDATA[',$noh);
		$j = explode(']]></Id_topico>', $i[1]);
		$id = $j[0];
	$novo_id_topico = $id+1;
	
	//obtem o id
		$i = explode('<Id_subtopico><![CDATA[',$noh);
		$j = explode(']]></Id_subtopico>', $i[1]);
		$id = $j[0];
	$novo_id_subtopico = $id+1;
	
	//obtem o id
		$i = explode('<Id_passo><![CDATA[',$noh);
		$j = explode(']]></Id_passo>', $i[1]);
		$id = $j[0];
	$novo_id_passo = $id+1;
	
	//obtem o id
		$i = explode('<Id_interacao><![CDATA[',$noh);
		$j = explode(']]></Id_interacao>', $i[1]);
		$id = $j[0];
	$novo_id_interacao = $id+1;
	
	//obtem o id
		$i = explode('<rotulo_interacao><![CDATA[',$noh);
		$j = explode(']]></rotulo_interacao>', $i[1]);
		$id = $j[0];
	$novo_rotulo_interacao = $id+1;
	
	
	//cria linhas par inserir no xml
	foreach($lista_arquivos as $print){
		
		//echo "<br>print:".$print;
		$registros .= "<interacao> <Id_modulo><![CDATA[".$novo_id_modulo."]]></Id_modulo> <Id_topico><![CDATA[".$novo_id_topico."]]></Id_topico> <Id_subtopico><![CDATA[".$novo_id_subtopico."]]></Id_subtopico> <Id_passo><![CDATA[".$novo_id_passo."]]></Id_passo> <Id_interacao><![CDATA[".$novo_id_interacao."]]></Id_interacao> <ordem_topico><![CDATA[]]></ordem_topico> <ordem_subtopico><![CDATA[]]></ordem_subtopico> <ordem_passo><![CDATA[]]></ordem_passo> <ordem_interacao><![CDATA[]]></ordem_interacao> <modulo><![CDATA[(Novo)Módulo:".$novo_id_modulo."]]></modulo> <topico><![CDATA[(Novo)Tópico:".$novo_id_topico."]]></topico> <subtopico><![CDATA[".$_POST['demonstracao']."]]></subtopico> <transicao><![CDATA[]]></transicao> <rotulo_topico><![CDATA[]]></rotulo_topico> <rotulo_interacao><![CDATA[".$novo_rotulo_interacao."]]></rotulo_interacao> <apres_funcao><![CDATA[]]></apres_funcao> <apres_import><![CDATA[]]></apres_import> <instrucao><![CDATA[Instrução - clique para editar o texto.]]></instrucao> <informacao><![CDATA[Informação - clique para editar o texto.]]></informacao> <print_tela><![CDATA[".$print."]]></print_tela> <tipo_passo><![CDATA[demonstracao]]></tipo_passo> <tipo_destaque><![CDATA[borda]]></tipo_destaque> <coordenada><![CDATA[1_1_100_100_123_3_-16_10]]></coordenada> </interacao>";
		
		$novo_id_interacao++;
		$novo_id_passo++;		
		$novo_rotulo_interacao++;
	}
	
	//concatena novas interações  
	$xml_form .= $registros;			
	
	//echo "<textarea>cria demos: $xml_form</textarea>";
	
	//echo $sistema." - ".$dir_raiz;
	remonta_xml($xml_form, $sistema, $dir_raiz);
	
	//redireciona para índice
	redireciona($dir_raiz."DEMOS/".$sistema."/professor/index.html");
	
}
else { echo "deu else";

}
include("../footer.php");
?>

