<?php 

include("padroes_demonstra/header.php");

$versao_atual_xml = "V3MIKE";

if(isset($_POST['cria_tutorial'])){
    $apresentacao_tutorial = $_POST['apresentacao_tutorial']; 
    $sistema_extenso = $_POST['nome_sistema'];
    $nome_responsavel = $_POST['nome_responsavel'];
    //echo "sdfsdf".$nome_responsavel;
    //define nome curto
    $sistema = substr(retirar_acentos($_POST['nome_sistema']),0,9);
    
    //echo  "nome".$sistema_extenso;
    //ve se já existe
        // pega o endereço do diretório 
        $diretorio = $dir_raiz."DEMOS";
        // abre o diretório
        $ponteiro  = opendir($diretorio);
        // monta os vetores com os itens encontrados na pasta
        while ($nome_itens = readdir($ponteiro)) 
        {
            //monta array com todos os nomes curtos da pasta
            $array_nomes_curtos[] = $nome_itens;            
        }
    
        //verifica se já existe o nome do tutorial e interrompe se já existir
        foreach ($array_nomes_curtos as $nome_itens) 
        {   
            //busca no xml mais recente o nome completo 
            $recente = busca_xml_mais_recente($nome_itens, $dir_raiz);
            $conteudo_arquivo = le($recente);
            //encontra o pattern do rotulo
            $pattern = '/rotulo_sistema=[\'|"](.|\r\n)*?[\'|\"]/';
            //echo "<textarea>".$conteudo_arquivo."</textarea>";
            $xml_novo = preg_match($pattern,$conteudo_arquivo,$matches);
            //echo $matches[0];         
            //obtem o id
            $i = preg_split("[\'|\"]",$matches[0]);         
            
            //echo $i[1]."<--"."<br>";
            $array_nomes_completos[] = $i[1];
        }
  
        //verifica se nome completo já existe
        
        if (in_array( $sistema_extenso, $array_nomes_completos)){
        echo "Já existe um tutorial com este nome.</br> <a href='".$dir_raiz."'>Voltar</a>";
    }
    else
        {
            //verifica se o nome curto já existia na pasta  
        if (in_array($sistema, $array_nomes_curtos)){
            $sistema = $sistema.date('ymd');
        }
    
        $aa = 1;
        //copia pasta de demonstração matricial, substituindo pelo nome curto do tutorial - obs: passa valores pelo parametro e precisa passar de novo para a copia recursiva.
        function recurse_copy($src,$dst,$sistema,$sistema_extenso,$apresentacao_tutorial,$nome_responsavel,$versao_atual_xml) {
        //echo $nome_responsavel."<br>";
        
        $dir = opendir($src);
        if(mkdir($dst)){
            chmod($dst, 0777);
            while(false !== ( $file = readdir($dir)) ) {
               if (( $file != '.' ) && ( $file != '..' )) {
                   if ( is_dir($src . '/' . $file) ) {
                       recurse_copy($src . '/' . $file, $dst . '/' . $file, $sistema,$sistema_extenso,$apresentacao_tutorial,$nome_responsavel,$versao_atual_xml);
                   }
                   else {              
             
                            //trata nome do file2                   
                            $file2 = str_ireplace('matricial', $sistema, $file);
                            
                            //copia                     
                            copy($src . '/' . $file,$dst . '/' . $file2);
                            //muda o chmod do arquivo
                            chmod($dst . '/' . $file2, 0777);
                            $tipo_arquivo = end(explode(".",$file));
                            //echo "<br>****************".$file;
                            if($tipo_arquivo!="jpg"&&$tipo_arquivo!="png"&&$tipo_arquivo!="gif"&&$tipo_arquivo!="ico"){
                                $conteudo_arquivo = le($dst . '/' . $file2);
                                //echo "tt".$apresentacao_tutorial;
                                $conteudo_arquivo = str_ireplace('apresentatut',$apresentacao_tutorial,$conteudo_arquivo);
                                $conteudo_arquivo = str_ireplace('matricial',$sistema,$conteudo_arquivo);
                                $conteudo_arquivo = str_ireplace('nometutcompleto',$sistema_extenso,$conteudo_arquivo);
                                $conteudo_arquivo = str_ireplace('responsaveltut',$nome_responsavel,$conteudo_arquivo);
                                $conteudo_arquivo = str_ireplace('versaoxmltut',$versao_atual_xml,$conteudo_arquivo);
                                
                                
                                
                                //echo "<textarea>".$nome_responsavel."#######".$conteudo_arquivo."</textarea>";
                                
                                escreve($dst . '/' . $file2,$conteudo_arquivo);
                                $aa++;
                            }
            
                        }
                    }
                }
                closedir($dir);
            } else {echo "nao criou";} 
            }
            $src = $dir_raiz."nucleo/matricial";
            $dst = $dir_raiz."DEMOS/".$sistema; 
            recurse_copy($src,$dst,$sistema,$sistema_extenso, $apresentacao_tutorial,$nome_responsavel, $versao_atual_xml);
            //echo "numero arquivos:".$aa;
            
            
            ///GERA ARQUIVOS A PARTIR DO XML PADRÃO  aqui
            
            //localiza xml mais recente
            $filename = busca_xml_mais_recente($sistema, $dir_raiz);
            $conteudo_arquivo = le($filename);
            //extrai do xml nós <interacao> e os distribui em um array
            $pattern = '/<interacao>(.|\r\n)*?<\/interacao>/i';
            //echo "<textarea>".$conteudo_arquivo."</textarea>";
            preg_match_all($pattern,$conteudo_arquivo,$matches);
            $nohs_interacoes = implode(",", $matches[0]);
            //echo "-->".$sistema."-->".$dir_raiz;
            //echo "cabecalho vazio".$cabecalho;            
            if(!empty($matches[0])){
             //echo "<textarea>origem->".$nohs_interacoes."</textarea>";
                remonta_xml($nohs_interacoes, $sistema, $dir_raiz);           
            }            
            //echo "dfg";
     //redireciona para cadastrar novas demos.
           redireciona($dir_raiz.'index.php?nome_tutorial='.$sistema);
             
        }    
}

include("../footer.php");
?>

