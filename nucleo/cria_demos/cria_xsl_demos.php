<?php

//$demonstra = "mantis";
$array_tipos_slide = array("informacao", "demonstracao","exercicio");
$xsl_completo = "";

// armazena início do conteúdo do arquivo xsl
$xsl_completo =  $xsl_completo."<?xml version='1.0'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:output method='html' omit-xml-declaration='yes' doctype-public='-//W3C//DTD XHTML 1.0 Transitional//EN' doctype-system='http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd' indent='yes'/>
<xsl:template match='/'>

		<html>
			<head>
				<meta name='description' content='".$apresentacao."' /> 
				<meta name='name' content='".$nome_identificador."' />
				<title>Demonstração</title>
				<link rel='stylesheet' href='../css/padrao.css' type='text/css' media='screen' />
				<link rel='stylesheet' href='../css/".$nome_identificador.".css' type='text/css' media='screen' />
				<link rel='stylesheet' href='estilos/tipTip.css' type='text/css' media='screen' />
				<script src='../js/jquery.js' type='text/javascript'></script>
				<script src='../js/jquery-ui-1.8.16.custom.min.js' type='text/javascript'></script>
				<script src='../js/padrao.js' type='text/javascript'></script>
				<script src='../js/".$nome_identificador.".js' type='text/javascript'></script>
				<script src='../js/jquery.tipTip.minified.js' type='text/javascript'></script>
				<script src='../js/jquery.editable-1.3.3.min.js' type='text/javascript'></script>
				<script src='../js/jquery.ajaxupload.3.5.js' type='text/javascript'></script>
			</head>
			<body>
				<div class='navegacao'></div>
				<xsl:for-each select='demonstracao/slide'>
					<div id='{id}' class='slide {tipo_layout}'>


						<div class='metainfo'>
							<h1 class='xml_topico'>Tópico: <xsl:value-of select='topico' /></h1>
							<h2 class='xml_subtopico'>Lição: <xsl:value-of select='subtopico' />
							</h2>
						</div>



						<xsl:choose>";								

// lê e armazena os tipos de slide selecionados na variável $xsl_completo				
for ($i = 0; $i < count($array_tipos_slide); $i++) {						
	
	//abre e lê o arquivo do tipo de slide solicitado
	$xsl_completo_layout_slide = "../partes_layouts/layout_".$array_tipos_slide[$i].".xsl";	
	$handle = fopen($xsl_completo_layout_slide, "r");
	$conteudo = fread ($handle, filesize ($xsl_completo_layout_slide));

	//salva código em $xsl_completo
	$xsl_completo =  $xsl_completo.$conteudo;
	
	//fecha o arquivo
	fclose($handle);	
}

// concatena o final do xsl gerado						
$xsl_completo =  $xsl_completo."

						</xsl:choose>

<!-- Fecha slide -->

					</div>
				</xsl:for-each>

			</body>
		</html>

	</xsl:template>

</xsl:stylesheet>
";

// Abre ou cria o arquivo txt
$fp = fopen($pasta_demo."/xsl/".$nome_identificador.".xsl", "w");
$escreve = fwrite($fp, $xsl_completo);
//fecha o arquivo
fclose($fp);

echo "<br/>Arquivo gerado: <a href='".$pasta_demo."/xsl/".$nome_identificador.".xsl'>".$nome_identificador.".xsl</a>";

?>

