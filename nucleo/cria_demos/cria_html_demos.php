<?php 

//função que faz a transformação do xml pelo xsl
function transform($xml, $xsl) {
   $xslt = new XSLTProcessor();
   $xslt->importStylesheet(new  SimpleXMLElement($xsl));
   return $xslt->transformToXml(new SimpleXMLElement($xml));
} 

//variável para armazenar o resultado da transformação 
$html_completo = transform($xml_completo, $xsl_completo);

//escreve arquivo html
$fp = fopen($pasta_demo."/html/".$nome_identificador.".html", "w");
$escreve = fwrite($fp, $html_completo);
//fecha o arquivo
fclose($fp);

echo "<br/>Arquivo gerado: <a href='".$pasta_demo."/html/".$nome_identificador.".html'>".$nome_identificador.".html</a>";

?>