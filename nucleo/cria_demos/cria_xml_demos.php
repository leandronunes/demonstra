<?php 

//CRIA NÓS XML
	//para cada imagem, cria um nó de slide no xml --> cria o conteúdo miolo do xml

	//cria variável que armazena o caminho da pasta imagens do curso
	chdir($pasta_demo."/imagens");
	$pasta_imagens = getcwd();
	
	if (is_dir($pasta_imagens)) {
		if ($diretorio_aberto = opendir($pasta_imagens)) {
			$index_id = 1;

			while (($arquivo_lido = readdir($diretorio_aberto)) !== false) {
				if (exif_imagetype($arquivo_lido) == IMAGETYPE_JPEG ||
					exif_imagetype($arquivo_lido) == IMAGETYPE_PNG ||
					exif_imagetype($arquivo_lido) == IMAGETYPE_GIF) {					
					echo "<br/>".$arquivo_lido;
						$miolo_xml = $miolo_xml."
							<slide>
								<id>".$index_id."</id>
								<topico><![CDATA[Clique aqui para editar o tópico.]]></topico>
								<subtopico><![CDATA[Clique aqui para editar o subtópico.]]></subtopico>
								<tipo_layout>demonstracao</tipo_layout>
								<print_tela>".$arquivo_lido."</print_tela>
								<texto_principal><![CDATA[Clique para editar o texto.]]></texto_principal>
								<imperativo><![CDATA[Clique para editar o imperativo.]]></imperativo>
								<label><![CDATA[Clique para editar o label.]]></label>
							</slide>";
							$index_id++;
								
					}
				
				}
			if ($index_id === 1) { echo "<br/>Mensagem: O arquivo ".$filename." não contém nenhuma imagem.";}
			closedir($diretorio_aberto);
			echo "".$index_id;
		}
	}

	// sai da pasta imagens e aponta para o diretório inicial
	chdir("../../../criar"); 

	//GERA XML COMPLETO 
	//coloca os metadados mais o miolo dentro da estrutura do xml 
	$xml_completo = "<?xml version='1.0' encoding='UTF-8'?>
	<?xml-stylesheet type='text/xsl' href='../xsl/".$nome_identificador.".xsl'?>
	<demonstracao><meta><nome>".$nome_demo."</nome><data_criacao>".$data_criacao."</data_criacao><apresentacao>".$apresentacao."</apresentacao> <objetivo_educacional>".$objetivo_educacional."</objetivo_educacional></meta>".$miolo_xml."</demonstracao>";

	// Abre ou cria o arquivo xml
	$fp = fopen($pasta_demo."/xml/".$nome_identificador.".xml", "w");
	$escreve = fwrite($fp, utf8_encode($xml_completo));
	//fecha o arquivo
	fclose($fp);
	
	echo "<br/>Arquivo gerado: <a href='".$pasta_demo."/xml/".$nome_identificador.".xml'>".$nome_identificador.".xml</a>";

?>