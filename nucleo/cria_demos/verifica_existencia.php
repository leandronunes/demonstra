<?php 
session_start();

include("header.php");

//classe que protege os dados vindos do formulário
require_once "../bibliotecas/Input.class.php";

//TRATA INPUTS
//Trata os dados vindos de um formulário usando o método POST
$inputObj = new Input("post");
// recebe as entradas do formulário
$nome_demo = $_POST["nome_demo"];
$apresentacao = $_POST["apresentacao"];
$objetivo_educacional = $_POST["objetivo_educacional"];
$imagens_base = $_FILES["imagens_base"];

//VARIAVEIS INICIAIS
//cria variavel para o nome que vai identificar a demo (sem espaços para nome de arquivo, pastas, etc)
$nome_identificador = str_replace(" ", "", $nome_demo);

//cria variavel para data de criação
$data_criacao = date("Ymd-H:i:s");
//cria variavel para armazenar o diretório inicial (pasta onde fica o cria_demos.php)
$diretorio_inicial = getcwd();
//cria variavel para armazenar o caminho da pasta do curso
$pasta_demo = "../demos/".$nome_identificador;

//VARIAVEIS DE SESSAO
//cria variavel de sessão para a demo que está sendo criada/editada com o valor do nome_identificador
if (!$_SESSION["criademos_recarregada"]) {
	$_SESSION["demo_foco"] = $nome_identificador;
	$_SESSION["nome_demo"] = $nome_demo;
	$_SESSION["apresentacao"] = $apresentacao;
	$_SESSION["objetivo_educacional"] = $objetivo_educacional;
	$_SESSION["imagens_base"] = $imagens_base;
}
	
echo "<br/>".$_SESSION["demo_foco"]."<br/>".$_SESSION["nome_demo"]."<br/>".$_SESSION["apresentacao"]."<br/>".$_SESSION["objetivo_educacional"]."<br/>".$_SESSION["imagens_base"];

//verifica se a demo já existe
if (file_exists($pasta_demo)) {
    echo "A demonstração <strong>".$nome_demo."</strong> já existe.
    <br/>
	<form action='cria_demos.php' method='post'>
		<input type='hidden' name='substituir'>
		<input type='submit' value='Substituir'>
	</form>";
	$_SESSION["criademos_recarregada"] = true;
}

//condição para não criar a demo: ela já existir e input 'substituir' não setado e nome da demo indefinido
if (!(file_exists($pasta_demo) && !isset($_POST['substituir'])) && !empty($nome_identificador)) {

	// CRIA PASTA PARA O CURSO
	mkdir ($pasta_demo, 0777);
	chmod($pasta_demo, 0777);

	//CRIA PASTAS INTERNAS
	$pastas = array("imagens","css","xml","js","xsl");
	foreach ($pastas as $cria_pasta){
		mkdir ($pasta_demo."/".$cria_pasta, 0777);
		chmod($pasta_demo."/".$cria_pasta, 0777);
	}


	//CRIA HTML


	//GRAVAR IMAGENS
	$message = "";
	echo "Arquivo enviado: ".$_FILES["imagens_base"]["name"];
	if($_FILES["imagens_base"]["name"]) {
		$filename = $_FILES["imagens_base"]["name"];
		$source = $_FILES["imagens_base"]["tmp_name"];
		$type = $_FILES["imagens_base"]["type"];

		$name = explode(".", $filename);
		$accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
		foreach($accepted_types as $mime_type) {
			if($mime_type == $type) {
				$okay = true;
				break;
			} 
		}
	 
		$continue = strtolower($name[1]) == 'zip' ? true : false;
		if(!$continue) {
			$message = "O arquivo ".$filename." enviado não é um zip válido. Por favor, tente novamente.";
		}
		else {
			$target_path = $pasta_demo."/imagens/".$filename;  // change this to the correct site path
			echo "<br/>Destino: ".$target_path;
			if(move_uploaded_file($source, $target_path)) {
				// trecho comentado para uso com bibliotecas ZZIPlib e Zip PELC
				/*$zip = new ZipArchive();
				$x = $zip->open($target_path);
				if ($x === true) {
				echo "$$$$";
					$zip->extractTo($target_path); // change this to the correct site path
					$zip->close();
					unlink($target_path);
				}*/
				chdir($pasta_demo."/imagens"); 
				exec("unzip '$filename'"); 
				$message = "O arquivo ".$filename." foi enviado e descompactado com sucesso.";			
			} else {	
				$message = "Houve um problema com o envio. Por favor, tente novamente.";
			}
		}	
	}
	echo "<br/>Mensagem: ".$message;

	//CRIA NÓS XML
	//para cada imagem, cria um nó de slide no xml --> cria o conteúdo miolo do xml

	//cria variável que armazena o diretório atual - pasta imagens do curso
	$pasta_imagens = getcwd();

	if (is_dir($pasta_imagens)) {
		if ($diretorio_aberto = opendir($pasta_imagens)) {
			$index_id = 1;
			while (($arquivo_lido = readdir($diretorio_aberto)) !== false) {
				if (exif_imagetype($arquivo_lido) == IMAGETYPE_JPEG ||
					exif_imagetype($arquivo_lido) == IMAGETYPE_PNG ||
					exif_imagetype($arquivo_lido) == IMAGETYPE_GIF) 
					{
						echo "<br/>Imagem: $arquivo_lido";
						$miolo_xml = $miolo_xml."
						<slide>
							<id>".$index_id."</id>
							<topico><![CDATA[Clique aqui para editar o tópico.]]></topico>
							<subtopico><![CDATA[Clique aqui para editar o subtópico.]]></subtopico>
							<tipo_layout>demonstracao</tipo_layout>
							<print_tela>".$arquivo_lido."</print_tela>
							<texto_principal><![CDATA[Clique para editar o texto.]]></texto_principal>
							<imperativo><![CDATA[Clique para editar o imperativo.]]></imperativo>
							<label><![CDATA[Clique para editar o label.]]></label>
						</slide>";
						$index_id++;
				}			
			}
			if ($index_id === 1) { echo "<br/>Mensagem: O arquivo ".$filename." não contém nenhuma imagem.";}
			closedir($diretorio_aberto);
		}
	}
	//apaga arquivo zip enviado
	unlink($_FILES["imagens_base"]["name"]);

	// sai da pasta imagens e aponta para o diretório inicial
	chdir($diretorio_inicial); 

	//GERA XML COMPLETO 
	//coloca os metadados mais o miolo dentro da estrutura do xml 
	$xml_completo = "<?xml version='1.0' encoding='UTF-8'?>
	<?xml-stylesheet type='text/xsl' href='../xsl/".$nome_identificador.".xsl'?>
	<demonstracao><meta><nome>".$nome_demo."</nome><data_criacao>".$data_criacao."</data_criacao><apresentacao>".$apresentacao."</apresentacao> <objetivo_educacional>".$objetivo_educacional."</objetivo_educacional></meta>".$miolo_xml."</demonstracao>";

	// Abre ou cria o arquivo xml
	$fp = fopen($pasta_demo."/xml/".$nome_identificador.".xml", "w");
	$escreve = fwrite($fp, utf8_encode($xml_completo));
	//fecha o arquivo
	fclose($fp);



	// GERA XSL completo
	include("cria_xsl_demos.php");

	//vai para edita_demos.php passando a variavel do nome da demo na sessão
	echo "<br/><br/>Demonstração <strong>".$nome_demo."</strong> gerada com sucesso. <a href='../criar/edita_demos.php'>Clique aqui</a> para editá-la.";

	//COPIA ARQUIVOS css e js padrões e de bibliotecas
	$array_arquivos_copiar = array("../padrao/padrao.css",
								   "../bibliotecas/tipTip.css",
								   "../padrao/padrao.js",
								   "../bibliotecas/jquery.js",
								   "../bibliotecas/jquery-ui-1.8.16.custom.min.js",
								   "../bibliotecas/jquery.tipTip.minified.js",
								   "../bibliotecas/jquery.editable-1.3.3.min.js");
	foreach ($array_arquivos_copiar as $copia_arquivo){
		$nome_arquivo = end(explode("/", $copia_arquivo));
		$extensao_arquivo = end(explode(".", $copia_arquivo));
		copy($array_arquivos_copiar[$i], $pasta_demo."/".$extensao_arquivo."/".$nome_arquivo);
	}
	
}
include("footer.php");
?>

