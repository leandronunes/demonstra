<!-- LAYOUT DEMONSTRAÇÃO -->
							<xsl:when test="tipo_layout='demonstracao'"> 

<!-- Modelo -->
								<div class="modelo">
<!-- Orientação -->
									<div class="orientacao">

										<div class="xml_texto_principal">
											<xsl:value-of select="texto_principal"/>
										</div>

										<div class="imperativo">
											<span class="xml_imperativo">
												<xsl:value-of select="imperativo" />
											</span>
											<span class="xml_label">
												<xsl:value-of select="label" />
											</span>
										</div>
									</div>
<!-- Área Interação -->
									<div class="area_interacao">
										<img class="print_acao_inicio" src="../imagens/{print_tela}" alt="" />
									</div>
								</div>
								
<!-- Hit -->
								<xsl:for-each select="hit_auxilio/hit">								
									<div class="coordenada {estilo}" name="{coordenada}"><xsl:value-of select="consequencia"/></div>
<!-- Consequência hit 						
									<div class="consequencia_hit">
										<div class="xml_texto_hit">
											<xsl:value-of select="consequencia"/>
										</div>
										<input type="button" class="botao_avanca_slide" value="Avançar"/>
									</div>
-->									
								</xsl:for-each>
								
<!-- Consequência slide -->								
								<div class="consequencia">
									<div class="xml_texto_adicional">
										<xsl:value-of select="texto_adicional"/>
									</div>
									<input type="button" class="botao_avanca_slide" value="Avançar"/>
								</div>

							</xsl:when>
