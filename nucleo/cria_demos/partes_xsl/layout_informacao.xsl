
<!-- LAYOUT INFORMACAO -->
							<xsl:when test="tipo_layout='informacao'">

<!-- Modelo -->

								<div class="modelo">
<!-- Orientação -->
									<div class="orientacao">
										<p class="xml_texto_principal">
											<xsl:value-of select="texto_principal"/>
										</p>
										<input type="button" class="botao_avanca_slide" value="Avançar"/>
									</div>

									<xsl:variable name="variavel" select="print_tela"/>
									<img class="print_acao_inicio" src="../imagens/{$variavel}" alt="carregando" />

								</div>

							</xsl:when>
