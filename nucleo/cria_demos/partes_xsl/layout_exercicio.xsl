<!-- LAYOUT EXERCÍCIO -->
							<xsl:when test="tipo_layout='exercicio'">
							
<!-- Modelo -->
								<div class="modelo">
<!-- Orientação -->
									<div class="orientacao">

										<p class="xml_texto_principal">
											<xsl:value-of select="texto_principal"/>
										</p>

										<p class="imperativo">
											<span class="xml_imperativo">
												<xsl:value-of select="imperativo" />
											</span>
											<span class="xml_label">
												<xsl:value-of select="label" />
											</span>
										</p>
									</div>
<!-- Área Interação -->
									<div class="area_interacao">
<!-- Auxilio visual -->
										<div class="hit_auxilio"></div>


										<img class="print_acao_inicio" src="../imagens/{print_tela}" alt="" />
									</div>

								</div>

<!-- Consequencia -->
								<div class="consequencia">
									<p class="xml_texto_adicional">
										<xsl:value-of select="texto_adicional"/>
									</p>
									<input type="button" class="botao_avanca_slide" value="Avançar"/>
								</div>

							</xsl:when>

