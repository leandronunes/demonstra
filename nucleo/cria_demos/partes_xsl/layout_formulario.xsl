							<xsl:when test="tipo_layout='formulario'">

<!-- Modelo -->
								<div class="modelo">
<!-- Orientação -->
									<div class="orientacao">

										<div class="xml_texto_principal">
											<xsl:value-of select="texto_principal"/>
										</div>

										<div class="imperativo">
											<span class="xml_imperativo">
												<xsl:value-of select="imperativo" />
											</span>
											
										</div>
									</div>
<!-- Área Interação -->
									<div class="area_interacao">
										<img class="print_acao_inicio" src="../imagens/{print_tela}" alt="" />
									<textarea class="dados"><xsl:value-of select="dados" /></textarea>
							
									
									
									</div>
								</div>
								
						
<!-- Consequência -->								
								<div class="consequencia">
									<div class="xml_texto_adicional">
										<xsl:value-of select="texto_adicional"/>
									</div>
								</div>

							

							</xsl:when>
							

							

						</xsl:choose>

<!-- Fecha slide -->

					</div>
				</xsl:for-each>
