$(document).ready(function(){
		
	// desabilita links com href=# para não irem ao topo da página
	$(document).on("click", "a[href='#']", function(event) {
	   event.preventDefault();
	});
	
	//{{{ INICIO MENSAGEM INICIAL
		// efeito máscara para mensagem de início do tutorial
	function inicioTutorial() {
		$("body").append("<div class='efeito_mascara_indice'></div>");
		$(".efeito_mascara_indice").css({opacity:0.7})
								   .fadeIn("fast");
		$(".texto_intro_tutorial").css("margin-top", ($(".texto_intro_tutorial").innerHeight() / 2) * -1)
								  .fadeIn("fast");
	}

	// função de clique no botão #inicia_tutorial da mensagem de início do tutorial (remove a mensagem) 
	// vale também para cliques na div .efeito_mascara_indice e tecla ESC
	$(document).on("click", "#inicia_tutorial, .efeito_mascara_indice", function(event) {
		$(".texto_intro_tutorial").fadeOut("fast", function(){ $(this).remove(); });
		$(".efeito_mascara_indice").fadeOut("fast", function(){ $(this).remove(); });	   
	});
	$(document).keypress(function(e) {
		if(e.keyCode == 27) {	
			$(".texto_intro_tutorial").fadeOut("fast", function(){ $(this).remove(); });
			$(".efeito_mascara_indice").fadeOut("fast", function(){ $(this).remove(); });
		}
	});
			
	// função de clique no botão #inicia_comonavegar da mensagem de início do tutorial (remove a mensagem e dispara o 'Como navegar') 	
	$(document).on("click", "#inicia_comonavegar", function(event) {
		$(this).parent().parent().fadeOut("fast", function(){ $(this).remove(); });
		$(".efeito_mascara_indice").fadeOut("fast", function(){ 
			$(this).remove(); 
			var e = jQuery.Event("click");
			$(".EHhelpBox a").trigger(e);
		});
	});
	
	// FIM MENSAGEM INICIAL}}}
	
	// clique no menu de categorias
	$(document).on("click", "#menu_categoria ul li a", function() {
		$("#menu_categoria a").removeClass("selecionado");
		$(this).addClass("selecionado");
		
		$(".seta_grande_categorias").remove();
		$(this).after("<div class='seta_grande_categorias'><span>Item selecionado</span></div>");
		
		$(".tabela_wrapper").hide();
		$("#"+$(this).attr("name")).fadeIn("fast");
	});
	
	
	//{{{ INICIO HISTORICO MÓDULO  - guarda histórico de navegação para voltar no último módulo aberto
	// lê parâmetro passado via querystring - url da barra de endereços
	var querystring = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&').toString();
	//se foi passado parâmetro, exibe categoria correspondente
	if(querystring.indexOf("mod=") >= 0){ 
		var index_modulo = parseInt(querystring.split("mod=")[1],10);
		$("#menu_categoria a[name='mod"+index_modulo+"']").addClass("selecionado")
														  .after("<div class='seta_grande_categorias'><span>Item selecionado</span></div>");
		$(".tabela_wrapper[id='mod"+index_modulo+"']").fadeIn("fast");
	}	
	// se não, seleciona primeiro item do menu de categorias e mostra texto de início do tutorial
	else {
		$("#menu_categoria a:first").addClass("selecionado")
									 .after("<div class='seta_grande_categorias'><span>Item selecionado</span></div>");
		$(".tabela_wrapper:first").fadeIn("fast");
		//inicioTutorial();
	}	
	//FIM HISTORICO MÓDULO }}}
	
	
	//{{{ INICIO PLUGIN DATATABLE
	// comportamento de tabelas	
	$("table").dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": false,
        "bInfo": false,
        "bAutoWidth": false
    } );
    
    	$(".dataTables_filter").prepend("<div class='filtrar_categoria'>Filtrar nesta categoria</div>");
	
	// esconde texto "filtrar nesta categoria" ao clicar no campo de busca da tabela
	$(document).on("click", ".filtrar_categoria", function(){
		$(this).hide();
		$(".dataTables_filter input").focus();
	});
	$(document).on("focus",".dataTables_filter input", function(){
		$(this).parent().siblings(".filtrar_categoria").hide();
	});
	$(document).on("blur",".dataTables_filter input", function(){
		if($(this).val() == "") {
			$(this).parent().siblings(".filtrar_categoria").show();
		}	
	});   
    
    
	// FIM PLUGIN DATATABLE}}}
	
	
	//{{{ INICIO SOLUÇÃO PARA MESCLAR TÓPICOS
	// mescla as linhas dos tópicos quando elas se repetem
	function mesclaTopicosInicial() {
		var classe_linha_topico = "topico_odd";
		$(".tabela_wrapper table").each(function(i){ 
			$(this).children('tbody').children('tr').children("td:nth-child(1)").each(function(j){
				if(j==0){nome_topico_default = "";}
				if ($(this).text() != nome_topico_default) {
					nome_topico_default = $(this).text();
					$(this).css("color","");
					$(".tabela_wrapper:eq("+i+") td:nth-child(1):eq("+(j-1)+")").css("border-bottom", "");
					//$("body").prepend($(this).parent().index());
					if($(this).parent().index()==0) {
						classe_linha_topico = "";
					}
					else {
						if(classe_linha_topico=="") { classe_linha_topico = "topico_odd"; }
						else if(classe_linha_topico=="topico_odd") { classe_linha_topico = ""; }
					}
					$(this).parent().addClass(classe_linha_topico);
				}
				else {
					$(this).parent().addClass(classe_linha_topico);
					$(this).css("color",$(this).css('backgroundColor'));
					$(".tabela_wrapper:eq("+i+") td:nth-child(1):eq("+(j-1)+")").css("border-bottom", "1px solid transparent");
				}
			});	
		});
	}
	
	// mescla as linhas dos tópicos quando elas se repetem (função chamada quando há alteração no campo de busca)
	function mesclaTopicos(index_tabela) {
		$(".tabela_wrapper:eq("+index_tabela+") td:nth-child(1)").each(function(i){
			if(i==0){nome_topico_default = "";}
			//$("body").prepend(nome_topico_default+" ");
			if ($(this).text() != nome_topico_default) {
				nome_topico_default = $(this).text();
				$(this).css("color","");
				$(".tabela_wrapper:eq("+index_tabela+") td:nth-child(1)").eq(i-1).css("border-bottom", "");
			}
			else {
				$(this).css("color",$(this).css('backgroundColor'));
				if($(this).text() == $(".tabela_wrapper:eq("+index_tabela+") td:nth-child(1)").eq(i).text()){
					$(".tabela_wrapper:eq("+index_tabela+") td:nth-child(1)").eq(i-1).css("border-bottom", "1px solid transparent");
				}
			}
		});
		//$("body").prepend("<br/>");
	}
	
	// chama a função no carregamento da página
	mesclaTopicosInicial();
	
	// chama a função mesclaTopicos a cada alteração no campo de busca
	$(document).on("keyup change",".dataTables_filter input", function(){
		var index_tabela = $(this).parents().filter(".tabela_wrapper").index() - 1;
		mesclaTopicos(index_tabela);
		//$("body").prepend(index_tabela+" ");
	});
	
	//FIM SOLUÇÃO PARA MESCLAR TÓPICOS }}}
	
	
});