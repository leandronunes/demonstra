$(document).ready(function(){

//{{{ INICIALIZA VISÃO PROFESSOR
	
	//oculta botão salvar, para mostrar apenas quando xml terminar de carregar
	//(evita o bug de mandar gerar novo xml, sem que ele esteja completo)
	$('.botao_salvar').hide();
	
	//torna campos editáveis na própria página
	camposEditaveis();
	
	//navegação - desabilita funções específicas da visão aluno e inicializa navegação das interações
	desabilitaNavegacaoTeclado();
	$('.proxima_acao').remove(); 	
	atualizaNavegacaoInteracoes();

	//monta o xml inicial
	criaXmlInicial();

	//desabilita arraste das imagens ao clicar e arrastar
	desabilitaDrag();

	//fixaNavegacaoInteracoes();
	
	//marca hit
	//marcaHit();
	
	//botão que mostra/esconde caixa de ferramentas lateral
	mostra_esconde_ferramentas();
	
	//deixa caixas de texto (informação+instrução) e suas setas arrastáveis
	hitTextoDrag();
	hitSetaDrag();
	
	//desliga o botão "Seguir"
	$(document).off("click", ".botao_seguir");
	$('.botao_seguir').css('background','none repeat scroll 0 0 #999')
					  .css('color','#444')
					  .css('cursor','default');
					  
	//desliga o acertaHit()
	$('.hit').off("click")
			 .css('cursor','default');
			 
	//desliga tela de "iniciar demonstração" (remove botão e máscara e interrompe rolagem automática)	 
	$(".botao_iniciar_demo").remove();
	$(".efeito_mascara_inicio").remove();
	$('html,body').stop(true, true).scrollTop(0);
	
	//torna o hit redimensionável e arrastável (substitui marcahit)
	$('.hit').resizable({containment: '.print_tela:visible', handles: 'n, e, s, w, ne, se, sw, nw', resize:function(event, ui) {
				trataEfeitoMascara($(this)); 
			 }, stop:function(event, ui) {
				gravaPosicoesHit(); 
			 } });
	$('.hit').draggable({ containment: '.print_tela:visible', drag:function(event, ui) { 
				trataEfeitoMascara($(this));
			 }, stop:function(event, ui) {
				gravaPosicoesHit(); 
			 } })
			 .css('cursor','move');
			 
//{{{ // TWITTER BOOTSTRAP
	//tooltip do botão buscar do "localizar/substituir"
	$('.botao_localizar').popover({placement:"right", trigger:'hover'});
//}}}

			 
	//permite arrastar o hit com as setas do teclado
	/*$(".hit").click(function() {
		var hit_editado = $(this);
		if(!hit_editado.hasClass('elemento_movel')){
			$('.elemento_movel').removeClass('elemento_movel');
			hit_editado.addClass('elemento_movel');
		}
		else {
			$(this).removeClass('elemento_movel');
		}
	});
	$(document).keydown(function(e) {
		var elemento_movel = $('.elemento_movel');
		var posicao = elemento_movel.position();
		var distancia = 1;
		var container = $('.print_tela:visible');
		
		switch (e.keyCode) {
			case 37: elemento_movel.css('left', posicao.left - distancia); break; // esquerda
			case 38: elemento_movel.css('top', posicao.top - distancia); break; // para cima
			case 39: elemento_movel.css('left', posicao.left + distancia); break; // direita
			case 40: elemento_movel.css('top', posicao.left + distancia); break; // para baixo
			default: return true;
		}
		
		// manter elemento_movel dentro do container
		if (posicao.left < 0 ||
		    posicao.top < 0 ||
			posicao.left + elemento_movel.innerWidth() > container.width() ||
			posicao.top + elemento_movel.innerHeight() > container.height()) {
			elemento_movel.css(posicao);
		}

		// evita rolagem da página
		e.preventDefault();
	});*/
	
	//permite arrastar a caixa de texto com as setas do teclado
	/*$(".texto_hit").click(function() {
		if(!$(this).hasClass('elemento_movel')){
			$('.elemento_movel').removeClass('elemento_movel');
			$(this).addClass('elemento_movel');
			$(document).on('keydown', handleKeys);
		}
		else {
			$(this).removeClass('elemento_movel');
		}
	});*/
	

	function handleKeys(e) {
		var position, 
			draggable = $('#draggable'),
			container = $('#container'),
			distance = 1; // Distance in pixels the draggable should be moved

		position = draggable.position();

		// Reposition if one of the directional keys is pressed
		switch (e.keyCode) {
			case 37: position.left -= distance; break; // Left
			case 38: position.top  -= distance; break; // Up
			case 39: position.left += distance; break; // Right
			case 40: position.top  += distance; break; // Down
			default: return true; // Exit and bubble
		}

		// Keep draggable within container
		if (position.left >= 0 && position.top >= 0 &&
			position.left + draggable.width() <= container.width() &&
			position.top + draggable.height() <= container.height()) {
			draggable.css(position);
		}

		// Don't scroll page
		e.preventDefault();
	}
			 
	//permite alternar entre os tipos de hit (borda / máscara)
	alteraTipoHit();
	
	//função de clique no botão SALVAR - atualiza todo o XML
	$(document).on('click', '.botao_salvar', function(event) { 
		postXml('formulario_professor', 'salvar', encontraUrlRetorno());
	});
	
	//função de clique no botão TROCAR IMAGEM - envia arquivo de imagem(print tela) substitui a nova imagem na interação atual
	$(document).on('click', '.botao_troca_imagem', function(event) { 
		postXml('formulario_professor', 'troca_imagem', encontraUrlRetorno());
	});
	
	//função de clique no botão TROCAR Sequencia de imagens - envia arquivo zip de imagens(print tela) e substitui pelas novas em sequencia
	$(document).on('click', '.botao_troca_todas', function(event) { 
		postXml('formulario_professor', 'troca_todas', encontraUrlRetorno());
	});
	
	//função de clique no botão DOWNLOAD - baixa o pacote final com todas as demonstrações da visão aluno (pastas html, imagens, js e css)
	//$(document).on('click', '.botao_download', function(event) { 
	//	postXml('formulario_professor', 'download_pacote', encontraUrlRetorno());
	//});
	
	//customiza campo para enviar arquivo
	$('input[id=imagem_substituta]').change(function() {
		$('#input_trocaimagem').val($(this).val().replace("C:\\fakepath\\", ""));
	});	
	
	//função de clique no botão LOCALIZAR - localiza ocorrencia do termo digitado via ajax
	$(document).on('click', '.botao_localizar', function(event) {
		$("#resultado_localizar").html('<div class="loading"></div>');
		postXml('formulario_localizar_substituir', 'localizar', encontraUrlRetorno());
	});
	
	//função de clique no botão PRÉ-VISUALIZAR - mostra como ficará o texto após a substituição
	$(document).on('click', '.botao_pre_visualizar', function(event) {
		//ativa preview
		if(!$(this).hasClass('ativo')){
			$('#resultado_localizar .termo_localizar').text($('.mensagem_resultado_localizar .termo_substituir').text());
			$('#resultado_localizar .termo_localizar').addClass('preview');
			$(this).addClass('ativo');
		}
		//desativa preview
		else {
			$('#resultado_localizar .termo_localizar').text($('.mensagem_resultado_localizar .termo_localizar').text());
			$('#resultado_localizar .termo_localizar').removeClass('preview');
			$(this).removeClass('ativo');
		}
	});
	
	//função de clique nos botões "LOCALIZAR E SUBSTITUIR TEXTO" e "NOVA BUSCA" - reseta busca
	$(document).on('click', '.botao_localizar_substituir, .botao_nova_busca', function(event) {
		$('.campos_localizar').show();
		$('.mensagem_resultado_localizar').hide();
		$('#resultado_localizar').html('');
		$('.confirma_substituir').hide();
		$('#formlocalizar_inputLocalizar').focus();
	});
	
	//função de clique no botão SUBSTITUIR - substitui termo
	$(document).on('click', '.botao_substituir', function(event) {
		postXml('formulario_localizar_substituir', 'substituir', encontraUrlRetorno());
	});
	
	//função de clique no botão VER MODO ALUNO - exibe o modo aluno
	$(document).on('click', '.botao_modoaluno', function(event) { 
		postXml('formulario_professor', 'simular_modoaluno', encontraUrlRetorno());
	});
	
	
//}}}


	//{{{ INICIO FUNÇÕES PROFESSOR
			
		//{{{ EDIÇÃO DE TEXTO
		
		//torna campos editáveis na própria página (plugin Editable)
		function camposEditaveis(){
			//$('.nome_topico').editable({onSubmit:atualizaInteracaoXml});
			//$('.descricao_acao .acao').editable({onSubmit:atualizaDemoXml, submit:'OK', cancel:'Cancelar'});
			$('.apresentacao').editable({type:'textarea', onEdit:function(){this.children('button').addClass('btn btn-small');ajusta_textarea(this.children('textarea'));}, onSubmit:function(){atualizaDemoXml();verificaCampoEditado();}, submit:'OK', cancel:'Cancelar'});
			$('.importante').editable({type:'textarea', onEdit:function(){this.children('button').addClass('btn btn-small');ajusta_textarea(this.children('textarea'));}, onSubmit:function(){atualizaDemoXml();verificaCampoEditado();}, submit:'OK', cancel:'Cancelar'});
			$('.texto_informacao').editable({type:'textarea', onEdit:function(){this.children('button').addClass('btn btn-small');corrigePosicaoSeta();ajusta_textarea(this.children('textarea'));}, onSubmit:function(){atualizaInteracaoXml();verificaCampoEditado();corrigePosicaoSeta();}, onCancel:function(){corrigePosicaoSeta();}, submit:'OK', cancel:'Cancelar'}).css('cursor','text');
			$('.texto_instrucao').editable({type:'textarea', onEdit:function(){this.children('button').addClass('btn btn-small');corrigePosicaoSeta();ajusta_textarea(this.children('textarea'));}, onSubmit:function(){atualizaInteracaoXml();verificaCampoEditado();corrigePosicaoSeta();}, onCancel:function(){corrigePosicaoSeta();}, submit:'OK', cancel:'Cancelar'}).css('cursor','text');
		}
		
		//redimensiona textarea para que exiba todo o conteúdo sem barra de rolagem
		function ajusta_textarea(textarea) {
			textarea.height(1);
			textarea.height(20+textarea.get(0).scrollHeight);
		}
		
		//clique no botão para adicionar texto "APRESENTAÇÃO": adiciona o texto e oculta o botão de adicionar novo texto
		$(document).on('click','.adicionar_apresentacao',function(){
			$(this).siblings('.apresentacao').text('Texto de apresentação da demonstração - clique para editar.');
			$(this).hide();
		});
		//clique no botão para adicionar texto "IMPORTANTE": adiciona o texto e oculta o botão de adicionar novo texto
		$(document).on('click','.adicionar_importante',function(){
			$(this).siblings('.importante').html('<h5>Importante</h5>Texto importante - clique para editar.');
			$(this).hide();
		});
		//clique no botão para adicionar texto "INFORMAÇÃO": adiciona o texto e desabilita o botão de adicionar novo texto
		$(document).on('click','.adicionar_informacao:not(.disabled)',function(){
			$(this).parents().filter('.texto_hit').find('.texto_informacao').text('Informação - clique para editar o texto.');
			$(this).addClass('disabled');
			corrigePosicaoSeta();
		});
		//clique no botão para adicionar texto "INSTRUÇÃO": adiciona o texto e desabilita o botão de adicionar novo texto
		$(document).on('click','.adicionar_instrucao:not(.disabled)',function(){
			$(this).parents().filter('.texto_hit').find('.texto_instrucao').text('Instrução - clique para editar o texto.');
			$(this).addClass('disabled');
			corrigePosicaoSeta();
		});
		
		//função que verifica se após a edição ainda existe texto no campo editado. 
		//Se não houver, adiciona um botão que permite que o usuário adicione novo texto posteriormente
		function verificaCampoEditado(){
			//texto de apresentação
			if($('#sub'+index_subtopico+' .descricao_acao .apresentacao').text().trim().length == 0){
				$('#sub'+index_subtopico+' .descricao_acao .adicionar_apresentacao').show();
			}

			//texto importante
			if($('#sub'+index_subtopico+' .descricao_acao .importante').text().trim().length == 0){
				$('#sub'+index_subtopico+' .descricao_acao .adicionar_importante').show();
			}
			
			//texto informação
			if($('#interacao'+index_interacao+' .texto_informacao').text().trim().length == 0){
				$('#interacao'+index_interacao+' .adicionar_informacao').removeClass('disabled');
			}
			
			//texto instrução
			if($('#interacao'+index_interacao+' .texto_instrucao').text().trim().length == 0){
				$('#interacao'+index_interacao+' .adicionar_instrucao').removeClass('disabled');
			}
		}
		//}}}	
		
		//{{{ Desabilita navegação por teclado
		// Para evitar que avance ou retorne a interação durante a digitação de texto
		function desabilitaNavegacaoTeclado(){
			//$(document).unbind('keydown');
			
			//-->Desabilitado. O controle está no js do modo aluno, função navegacaoTeclado()
			//Se houver textarea aberta, navegação por teclado será desabilitada.
		}	
		//}}}
		
		//{{{ Fixa a navegação por interações no topo da página
		/*function fixaNavegacaoInteracoes(){
			$(window).bind('scroll', function(){
				//Desloca barra da escola para o rodapé ao rolar a página
				if($(window).scrollTop() == $('.navegacao_interacoes').offset().top){ 
					$('.navegacao_interacoes').css("top","0")
											  .css("position","fixed");				 
				}
			});
		}	*/
		
		//exibe caixa de ferramentas de configuração da interação ao passar o mouse no botão "configurar"
		$(document).on('click','.edita_interacao .config_interacao',function(){
			$(this).parents().filter('.edita_interacao').children('.painelconfig_interacao').show();
		});
		//oculta caixa de ferramentas de configuração da interação ao tirar o mouse da caixa ou clicar em algum dos botões (input)
		$(document).on('mouseleave','.edita_interacao>.painelconfig_interacao',function(){
			var painel = $(this);			
			timeout_editainteracao = setTimeout(function(){painel.fadeOut('fast');}, 500);
		});
		$(document).on('mouseenter','.edita_interacao>.painelconfig_interacao',function(){
			clearTimeout(timeout_editainteracao);
		});	
		//
		$(document).on('mouseup','.edita_interacao>.painelconfig_interacao input:not(.troca_tipohit)',function(){
			$(this).parents().filter('.edita_interacao').children('.painelconfig_interacao').hide();
		});
		
		//}}}		
		
		//{{{GERAR XML
		
			//{{{CRIA BASE XML
			function criaXmlInicial(){
				//desabilita cache
				$.ajaxSetup({
					cache: false
				});
				//carrega todos os links de demonstrações presentes no index.html
				$('.retorna_xml').load('xml_completo.txt', function(){
					//distribui nós do XML (<interacao>) pelo array registros_xml
					registros_xml = $('.retorna_xml').text().split('</interacao>');
					for (i = (registros_xml.length-2); i >= 0; i--){
						registros_xml[i+1] = registros_xml[i]+'</interacao>';
					}
					//limpa índice zero do array já que os IDs das interações começam do 1
					registros_xml[0] = '';
					//exibe botão salvar após carregar o xml
					$('.botao_salvar').show();
				});
			}
			//}}}
			
			//{{{ MONTA REGISTRO
			function montaRegistro(i){	
			
				//obtem url da imagem
				if($('#interacao'+i+' img').length){
					//se imagem já foi carregada /demo/imagens/
					var url_img = $('#interacao'+i+' img').attr('src').split('/demo/imagens/').pop();
				}
				else {
					//se imagem ainda não foi carregada (url da imagem armazenada na div "url_img")
					var url_img = $('#interacao'+i+' .url_img').text().split('/demo/imagens/').pop();
				}
				 
			
				/*Id_modulo   	  */ campoA = $('#interacao'+i).parents().filter('.modulo').attr('id').split('mod')[1];
				/*Id_topico   	  */ campoB = $('#interacao'+i).parents().filter('.topico').attr('id').split('top')[1];
				/*Id_subtopico	  */ campoC = $('#interacao'+i).parents().filter('.subtopico').attr('id').split('sub')[1];
				/*Id_passo	  	  */ campoD = $('#interacao'+i).parents().filter('.passo').attr('id').split('passo')[1];
				/*Id_interacao	  */ campoE = $('#interacao'+i).attr('id').split('interacao')[1];
				/*ordem_topico	  */ campoF = $('#top'+campoB+'>.meta>.ordem').text();
				/*ordem_subtopico */ campoG = $('#sub'+campoC+'>.meta>.ordem').text();
				/*ordem_passo	  */ campoH = $('#passo'+campoD+'>.meta>.ordem').text();
				/*ordem_interacao */ campoI = parseInt($('#passo'+campoD+' .interacao').index($('#interacao'+i)), 10) + 1;
				/*modulo		  */ campoJ = $('#interacao'+i).parents().filter('.modulo').attr('name');
				/*topico		  */ campoK = $('#interacao'+i).parents().filter('.topico').attr('name');
				/*subtopico		  */ campoL = $('#sub'+campoC+' h4.acao').html();
				/*transicao		  */ campoM = $('#interacao'+i+'>.meta>.transicao').text();
				/*rotulo_topico	  */ campoN = $('#top'+campoB+'>.meta>.rotulo').text();
				/*rotulo_interacao*/ campoO = $('#interacao'+i+'>.meta>.rotulo').text();
				/*apres_funcao	  */ //se campo está sendo editado (contém <textarea>)
									 if($('#sub'+campoC+'>.main>.descricao_acao>.apresentacao').html().indexOf("textarea") >= 0)
									 {campoP = $('#sub'+campoC+'>.main>.descricao_acao>.apresentacao').val();} 
									 //se campo NÃO está sendo editado
									 else {campoP = $('#sub'+campoC+'>.main>.descricao_acao>.apresentacao').html();}
				/*apres_import	  */ campoQ = $('#sub'+campoC+'>.main>.descricao_acao>.importante').text().substring(11); //remove palavra "Importante"
				/*instrucao		  */ //se campo está sendo editado (contém <textarea>)
									 if($('#interacao'+i+' .texto_instrucao').html().indexOf("textarea") >= 0)
									 {campoR = $('#interacao'+i+' .texto_instrucao textarea').val();} 
									 //se campo NÃO está sendo editado
									 else {campoR = $('#interacao'+i+' .texto_instrucao').html();}
				/*informacao	  */ //se campo está sendo editado (contém <textarea>)
									 if($('#interacao'+i+' .texto_informacao').html().indexOf("textarea") >= 0)
									 {campoS = $('#interacao'+i+' .texto_informacao textarea').val();} 
									 //se campo NÃO está sendo editado
									 else {campoS = $('#interacao'+i+' .texto_informacao').html();}
				/*print_tela	  */ campoT = url_img;
				/*tipo_passo	  */ campoU = $('#passo'+campoD+'>.meta>.tipo_passo').text();
				/*tipo_destaque	  */ if($('#interacao'+i+' .hit').hasClass('borda')){campoV="borda";} //se borda
									 else if($('#interacao'+i+' .hit').hasClass('mascara')){campoV="mascara";} //se máscara
									 else if($('#interacao'+i+' .hit').hasClass('informacao')){campoV="informacao";} //se informação
				/*coordenada	  */ campoW = $('#interacao'+i+'>.meta>.coordenada').text();

				//trata campos <instrucao> e <informacao>, que podem não conter nenhum valor
				//(para que xml não seja gravado com o valor "null")
				if(campoR == undefined) {campoR = "";}
				if(campoS == undefined) {campoS = "";}				
				
				// concatena valores das variáveis no nó <interacao> do xml
				registros_xml[i]= "<interacao>   <Id_modulo><![CDATA["+campoA+"]]></Id_modulo> <Id_topico><![CDATA["+campoB+"]]></Id_topico> <Id_subtopico><![CDATA["+campoC+"]]></Id_subtopico> <Id_passo><![CDATA["+campoD+"]]></Id_passo> <Id_interacao><![CDATA["+campoE+"]]></Id_interacao> <ordem_topico><![CDATA["+campoF+"]]></ordem_topico> <ordem_subtopico><![CDATA["+campoG+"]]></ordem_subtopico> <ordem_passo><![CDATA["+campoH+"]]></ordem_passo> <ordem_interacao><![CDATA["+campoI+"]]></ordem_interacao> <modulo><![CDATA["+campoJ+"]]></modulo> <topico><![CDATA["+campoK+"]]></topico> <subtopico><![CDATA["+campoL+"]]></subtopico> <transicao><![CDATA["+campoM+"]]></transicao> <rotulo_topico><![CDATA["+campoN+"]]></rotulo_topico>  <rotulo_interacao><![CDATA["+campoO+"]]></rotulo_interacao> <apres_funcao><![CDATA["+campoP+"]]></apres_funcao> <apres_import><![CDATA["+campoQ+"]]></apres_import> <instrucao><![CDATA["+campoR+"]]></instrucao> <informacao><![CDATA["+campoS+"]]></informacao> <print_tela><![CDATA["+campoT+"]]></print_tela> <tipo_passo><![CDATA["+campoU+"]]></tipo_passo> <tipo_destaque><![CDATA["+campoV+"]]></tipo_destaque> <coordenada><![CDATA["+campoW+"]]></coordenada>   </interacao>";
			}
			//}}}
			
			//{{{ MONTA XML
			function montaXml(){	
				texto_xml = registros_xml.join("");
				$('.retorna_xml').text(texto_xml); 
			}
			//}}}
			
			//{{{ ATUALIZA XML
			// função de 'callback' da edição dos campos
			// atualiza nó da interação equivalente ao campo editado no XML
			function atualizaInteracaoXml(){
				//$('body').append('<div class="loading_professor"/>');
				montaRegistro(index_interacao);
				montaXml();
				//$('.loading_professor').fadeOut("fast", function(){ $(this).remove(); });
			}
			
			// função de 'callback' da edição do nome da demo, apresentação ou texto 'importante' da demo
			// atualiza todos os nós <interacao> daquela demonstração no XML
			function atualizaDemoXml(){
				var quantidade_interacoes = $('#sub'+index_subtopico+' .interacao').length;
				var index_primeira_interacao = parseInt($('#sub'+index_subtopico+' .interacao:first').attr('id').split('interacao')[1], 10);
				var index_ultima_interacao = quantidade_interacoes + index_primeira_interacao - 1;
				//atualiza nome da demo em todos os nós <interacao> daquela demonstração no XML
				for(i=index_primeira_interacao; i<=index_ultima_interacao; i++){					
					montaRegistro(i);
				}
				//monta o XML atualizado
				montaXml();
			}
			
			//}}}
		
		//}}}
		
			
			// função que deixa a caixa do texto principal / adicional arrastável e atualiza coordenadas ao final do 'arraste'
			function hitTextoDrag(){
				$('.texto_hit')/*.css('cursor','move')*/
							   .draggable({ containment: '.print_tela:visible', stop:function(event, ui) {
					gravaPosicoesHit(); 
				} });
			}

			// função que posiciona a seta em relação à caixa do texto principal / adicional
			function hitSetaDrag() {
				$(".hit_seta").css('cursor','move')
							  .draggable({ containment:'.print_tela:visible', snap:true, snapMode:'outer',
								drag: function(event, ui) {
									corrigeSentidoSeta();
									//estende o objeto 'ui' para permitir que elemento arrastado "grude" em um elemento específico
									//visto em: http://jsfiddle.net/fhamidi/PjmJe/1/
									var draggable = $(this).data("draggable");
									$.each(draggable.snapElements, function(index, element) {
										ui = $.extend({}, ui, {
											snapElement: $(element.item),
											snapping: element.snapping
										});
										if (element.snapping) {
											if (!element.snappingKnown) {
												element.snappingKnown = true;
												draggable._trigger("snapped", event, ui);
											}
										} else if (element.snappingKnown) {
											element.snappingKnown = false;
											draggable._trigger("snapped", event, ui);
										}
									});
								},
								stop:function(event, ui) {
									corrigePosicaoSeta();
									gravaPosicoesHit();
								},
								snap: ".texto_hit",
								snapped: function(event, ui) {
									// Do something with 'ui.snapElement' and 'ui.snapping'...
								}
							  });
			}
			//corrige o sentido da seta da caixa de texto durante o arraste 
			function corrigeSentidoSeta() {
				var seta = $('#interacao'+index_interacao+' .hit_seta');
				//top: seta acima da caixa de texto
				if(seta.position().top < 0) {
					seta.css("background-position", "left -32px");
				}
				//right: seta à direita da caixa de texto
				if(seta.position().left > seta.parent().width()) {
					seta.css("background-position", "left -16px");
				}
				//bottom: seta abaixo da caixa de texto
				if(seta.position().top > seta.parent().height()){
					seta.css("background-position", "left -48px");
				}
				//left: seta à esquerda da caixa de texto
				if(seta.position().left <= 0) {
					seta.css("background-position", "left top");
				}
			}
			//corrige a posição da seta da caixa de texto (diferença de 1px da borda da caixa) ao final do arraste 
			function corrigePosicaoSeta() {
				var seta = $('#interacao'+index_interacao+' .hit_seta');
				//top: seta acima da caixa de texto

				if(seta.position().top < 0) {
					seta.css("top", "-16px");
				}
				//right: seta à direita da caixa de texto
				else if(seta.position().left > seta.parent().width()) {
					seta.css("left", seta.parent().innerWidth());
				}
				//bottom: seta abaixo da caixa de texto
				else if(seta.position().top > seta.parent().height()){
					seta.css("top", seta.parent().innerHeight());
				}
				//left: seta à esquerda da caixa de texto
				else if(seta.position().left <= 0) {
					seta.css("left", "-16px");
				}
				//nenhum dos anterior: seta dentro da caixa de texto - posiciona seta abaixo
				else {
					seta.css("top", seta.parent().innerHeight());
					corrigeSentidoSeta();
				}
			}				
			
			//atualiza o xml com a nova coordenada do hit e caixa de texto
			function gravaPosicoesHit() { 
				var seta = $('#interacao'+index_interacao+' .hit_seta');
				var hit = $('#interacao'+index_interacao+' .hit');
				if(hit.length){
					var x_hit = parseInt(hit.position().left,10);
					var y_hit = parseInt(hit.position().top,10);
					var width_hit = hit.width();
					var height_hit = hit.height();
				}
				else{
					var x_hit = 10;
					var y_hit = 10;
					var width_hit = 100;
					var height_hit = 100;
				}
				var x_caixa_txt = parseInt(seta.parent().position().left,10);
				var y_caixa_txt = parseInt(seta.parent().position().top,10);
				var x_seta = parseInt(seta.position().left,10);
				var y_seta = parseInt(seta.position().top,10);
				$('#interacao'+index_interacao+'>.meta>.coordenada').text(x_hit+'_'+y_hit+'_'+width_hit+'_'+height_hit+'_'+x_caixa_txt+'_'+y_caixa_txt+'_'+x_seta+'_'+y_seta);
				atualizaInteracaoXml();				
			}
			
			//redimensiona o efeito máscara ao mudar o tamanho ou posição do hit
			function trataEfeitoMascara(hit) {
				if(hit.hasClass('mascara')){
					$('#interacao'+index_interacao+' .efeito_mascara').remove();					
					efeitoMascara(hit);
				}
			}
			
			//alterna entre os tipos de hit
				//alterna entre os tipos de hit
				function alteraTipoHit() {	
				    
				    if($('#interacao'+index_interacao+" .tipo_hit_selecionado").text()=="informacao"){
                            //posicao padrao
                            left_padrao = "1px";
                            top_padrao = "1px";
                            height_padrao = "100px";
                            width_padrao = "100px";
                            //caixa de texto									
                            left_padrao_th=  "123px";
                            top_padrao_th=  "3px";
                            left_padrao_seta = "-16px";
                            top_padrao_seta = "10px"; 				        
				    }
				    
				    
				
                    $(document).on('click','.troca_tipohit',function(){
                        //localizao objeto do hit			   
                        var hit = $(this).parents().filter('.interacao').children('.hit');
						
                        //DEFINE POSIÇÕES PADRÃO, DEPENDENDO DO TIPO DE HIT ATUAL //guarda variaveis de posição do hit caso o usuario mude de ideia	
                        if($(this).parents('.altera_tipo_hit').find('.tipo_hit_selecionado').text()!='informacao'){
                            //pega ultima posicao do hit    
                            height_padrao = $('#interacao'+index_interacao+" .hit").height();
                            width_padrao= $('#interacao'+index_interacao+" .hit").width();
                            position = $('#interacao'+index_interacao+" .hit").position();					
                            left_padrao= $('#interacao'+index_interacao+" .hit").position().left;
                            top_padrao= $('#interacao'+index_interacao+" .hit").position().top;
                            //caixa de texto									
                            left_padrao_th = $('#interacao'+index_interacao+" .texto_hit").position().left;
                            top_padrao_th = $('#interacao'+index_interacao+" .texto_hit").position().top;
                            left_padrao_seta = $('#interacao'+index_interacao+" .hit_seta").position().left;
                            top_padrao_seta = $('#interacao'+index_interacao+" .hit_seta").position().top;
                        }
                                    
                        
                        //SE BOTÃO CLICADO É DO TIPO BORDA
                        if($(this).attr('title') == 'borda'){

                            //troca classes do hit
                            hit.removeClass('mascara informacao'); 
							hit.addClass('borda');
							//troca classes da caixa de texto
                            hit.next('.texto_hit').removeClass('mascara informacao'); 
							hit.next('.texto_hit').addClass('borda');                  
                            //coloca valores do ultimo hit marcado.
                            hit.css('height',height_padrao).css('width', width_padrao).css('left', left_padrao).css('top', top_padrao); 
                            $('#interacao'+index_interacao+" .texto_hit").css('left', left_padrao_th).css('top', top_padrao_th);
                            $('#interacao'+index_interacao+" .hit_seta").css('left', left_padrao_seta).css('top', top_padrao_seta);
                                    
                            //reseta estilo da borda
                            hit.css('border',''); 						
                            //remove o botão "Seguir"
                            hit.next('.texto_hit').children('.botao_seguir').remove();
                            //remove o efeito_mascara
                            $('#interacao'+index_interacao+' .efeito_mascara').remove();
                            //mostra a seta
                            $('#passo'+index_passo+'_texto'+index_interacao+' .hit_seta').show(); 
                            //padraoiza o tipo que será usado para salvar os dados

                            $(this).parents('.altera_tipo_hit').find('.tipo_hit_selecionado').text('borda');
							//corrige posição da seta
							corrigePosicaoSeta();
                        }
                        //SE BOTÃO CLICADO É DO TIPO INFORMAÇÃO
                        else if($(this).attr('title') == 'informacao'){
                    
                            
                            //troca classes do hit
                            hit.removeClass('mascara borda'); 
							hit.addClass('informacao');
							//troca classes da caixa de texto
                            hit.next('.texto_hit').removeClass('mascara borda'); 
							hit.next('.texto_hit').addClass('informacao'); 
                            //deixa borda transparente
                            hit.css('border','transparent');
                            //remove o botão "Seguir", se houver
                            hit.next('.texto_hit').children('.botao_seguir').remove();
                            //adiciona o botão "Seguir"
                            hit.next('.texto_hit').append('<input type="button" class="botao_seguir" value="Seguir"/>'); 						
                            //esconde hit
                            hit.css('height','0').css('width','0').css('border','none').css('left','350px ').css('top','150px');
                            //posiciona caixa de texto
                            $("#passo"+index_passo+"_texto"+index_interacao).css('left','320px').css('top','125px'); 
                            //esconde a seta
                            $('#passo'+index_passo+'_texto'+index_interacao+' .hit_seta').hide(); 
                            //adiciona o efeito_mascara
                            efeitoMascara(hit); 
                            //padraoiza o tipo que será usado para salvar os dados
                            $(this).parents('.altera_tipo_hit').find('.tipo_hit_selecionado').text('informacao'); 
    
                        }
                        //SE BOTÃO CLICADO É DO TIPO MÁSCARA
                        else if($(this).attr('title') == 'mascara'){
                                  // alert($(this).parents('.altera_tipo_hit').find('.tipo_hit_selecionado').text());
                                 //atualiza o tipo que será usado para salvar os dados
                            $(this).parents('.altera_tipo_hit').find('.tipo_hit_selecionado').text('mascara'); 
                           //alert("sdf");
                           //troca classes do hit
                            hit.removeClass('informacao borda'); 
							hit.addClass('mascara');
							//troca classes da caixa de texto
                            hit.next('.texto_hit').removeClass('informacao borda'); 
							hit.next('.texto_hit').addClass('mascara'); 
                           //mostra e posiciona o hit no ultimo valor valido
                           hit.css('height',height_padrao).css('width', width_padrao).css('left', left_padrao).css('top', top_padrao); 
                           $('#interacao'+index_interacao+" .texto_hit").css('left', left_padrao_th).css('top', top_padrao_th);
                             //remove o botão "Seguir", se houver
                            hit.next('.texto_hit').children('.botao_seguir').remove();
                            //adiciona o botão "Seguir"
                            hit.next('.texto_hit').append('<input type="button" class="botao_seguir" value="Seguir"/>'); 
                             //atualiza o tipo que será usado para salvar os dados
                            $(this).parents('.altera_tipo_hit').find('.tipo_hit_selecionado').text('mascara');	
                            //deixa borda transparente
                           hit.css('border','transparent'); 
                            //mostra a seta
                            $('#passo'+index_passo+'_texto'+index_interacao+' .hit_seta').show(); 
                            //posiciona seta
                            $('#interacao'+index_interacao+" .hit_seta").css('left', left_padrao_seta).css('top', top_padrao_seta);
							//corrige posição da seta
							corrigePosicaoSeta();
                           //adiciona o efeito_mascara
                           efeitoMascara(hit);
                            
                        }
                     gravaPosicoesHit();
				});
			}
			
			
			//função - impede que a imagem seja arrastada no marca hit
			function desabilitaDrag(){
				$(document).on('dragstart', 'img', function(event) { event.preventDefault(); });	
				$(document).on('draggesture', 'img', function(event) { event.preventDefault(); }); //Firefox versões abaixo de 3.5
			}
			
	
			//função submit - salva alterações feitas no modo professor: salva novo XML, faz as transformações e gera HTMLs
			function postXml(formName, actionName, url_retorno, posicao){

				var hiddenControl = $('#'+formName+' .action');
				//var hiddenControl = document.getElementById('action');
				var theForm = document.getElementById(formName);       
				hiddenControl.val(actionName);
				
/////OBS: transformar appends em uma função	
				//insere input oculto coma  url de retorno
				$("#"+formName).append('<input type="hidden" id="html_form" name="html_form" value="'+url_retorno+'" />');
					
				//adiciona input oculto no formulário com o index_interacao, para ser usado nas funções de adicionar ou excluir interação
				$("#"+formName).append('<input type="hidden" id="id_interacao" name="id_interacao" value="'+index_interacao+'" />');	
				
				//adiciona input oculto no formulário com antes ou depois (para ser usado nas funções de adicionar ou excluir interação)
				$("#"+formName).append('<input type="hidden" id="posicao" name="posicao" value="'+posicao+'" />');
					
				//específico para botão "salvar"
				if(actionName == "salvar"){
					$('#'+formName+' .botao_salvar').after('<p class="salvando text-warning">Salvando...</p>');
					$('#'+formName+' .botao_salvar').attr('disabled','disabled');
					$.post($("#"+formName).attr('action'), $("#"+formName).serialize())
										  .done(function(data) { 	
											//alert(data);
											//se função retornar "sucesso"
											if (data.toLowerCase().indexOf("###sucesso###") >= 0){
												//alert('sucesso!');
												$('#'+formName+' .salvando').toggleClass('text-warning text-success')
																				.toggleClass('salvando sucesso')
																				.text('Sucesso!');
												setTimeout(function(){
													$('#'+formName+' .sucesso').fadeOut('normal', function(){$(this).remove();});
													$('#'+formName+' .botao_salvar').removeAttr('disabled');
												}, 1000);
											}
											//se função retornar "falha"
											else if (data.toLowerCase().indexOf("###falha###") >= 0){
												//alert('falhou.');
												$('#'+formName+' .salvando').toggleClass('text-warning text-error')
																				.toggleClass('salvando falha')
																				.text('A gravação dos dados falhou.');	
											}								
										  })/*
										  .fail(function() { alert("error"); })
										  .always(function() { alert("finished"); })*/;
				}
				//específico para botão "localizar"
				else if (actionName == "localizar") {
					//verifica se campo "localizar" foi preenchido corretamente e executa
					if($("#formlocalizar_inputLocalizar").val() != ""){
						
						//oculta inputs "localizar/substituir"
						$(".campos_localizar").hide();
						
						//atribui valores aos inputs localizar e substituir no formulario de substituir
						$("#formsubstituir_inputLocalizar").val($("#formlocalizar_inputLocalizar").val());
						$("#formsubstituir_inputSubstituir").val($("#formlocalizar_inputSubstituir").val());
						
						//exibe mensagem informando os termos pesquisados
						$('.mensagem_resultado_localizar').show();
						
						//atribui valores à mensagem de termo pesquisado
						$(".termo_localizar").text($("#formlocalizar_inputLocalizar").val());
						$(".termo_substituir").text($("#formlocalizar_inputSubstituir").val());
						
						//atribui valor ao input do xml completo
						$("#formsubstituir_xml_form").val($(".retorna_xml").val());
						
						$.post($("#"+formName).attr('action'), $("#"+formName).serialize())
											  .done(function(data) { 	
											  
												//atribui string retornada pelo php com o resultado da busca à div #resultado_localizar
												$("#resultado_localizar").html(data);
												
												//exibe botões para fazer nova busca e substituir
												$(".confirma_substituir").show();
												
												//se função localizar não retornar resultados
												if($("#ids_substituir").text() == "") {
													//desabilita botão "substituir"
													$(".botao_substituir").attr('disabled','disabled');
													//oculta mensagem de termo pesquisado
													$('.mensagem_resultado_localizar').hide();
												}
												//se retornar, atribui valor dos IDs das interações encontradas ao input correspondente e habilita botão "substituir"
												else {
													$("#formsubstituir_array_ids_para_substituir").val($("#ids_substituir").text());
													$(".botao_substituir").removeAttr('disabled');
													//ao rolar a div com os resultados, "gruda" o título da demonstração corrente no topo do div, para facilitar navegação
													/*$('.corpo_modal_localizar').scroll(function(){
														var titulo_demo = $('.resultado_titulo_demo');
														titulo_demo.each(function() {
															if($(this).position().top < 0) {
																titulo_demo.removeClass('topo');
																$(this).addClass('topo');
															}
														});
													});*/
												}
											  });
					}
					//se campo "localizar" não estiver preenchido, exibe mensagem de erro.
					else {
						$("#formulario_localizar_substituir .alert-info").hide();
						$("#resultado_localizar").html('<div class="alert alert-error"><p>O campo <strong>"Localizar"</strong> é obrigatório.</p></div>');
					}
				}
				//geral
				else {
					theForm.submit();
				} 
			}			
			
			//define a página e interação para a qual vai voltar depois de uma ação de post de formulário
			function encontraUrlRetorno(){
				//adiciona input oculto no formulário com a url da interação que está sendo editada, para que após salvar o XML, retorne para a mesma interação
					// lê parâmetro passado via querystring - url da barra de endereços
					var querystring = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&').toString();
					// se há o parâmetro 'interacao' na url, inclui parâmetro na url de retorno
					if(querystring.indexOf("interacao") >= 0){ 
						var url_retorno = window.location.href.split('/')[window.location.href.split('/').length - 1].toString().split('=')[0]+'='+$('#interacao'+index_interacao+'>.meta>.rotulo').text();
					}	
					// se NÃO há o parâmetro 'interacao' na url e não é a primeira interação, inclui parâmetro 'interacao' na url de retorno
					else if(index_interacao != $('#sub'+index_subtopico+' .interacao:first').attr('id').split('interacao')[1]){
						var url_retorno = window.location.href.split('/')[window.location.href.split('/').length - 1].toString()+'?interacao='+$('#interacao'+index_interacao+'>.meta>.rotulo').text();
					}	
					// se NÃO há o parâmetro 'interacao' na url e é a primeira interação, url de retorno será apenas o nome do arquivo html
					else {
						var url_retorno = window.location.href.split('/')[window.location.href.split('/').length - 1].toString();
					}
					
				return url_retorno;
			}
			
			//{{{CRIAR E EXCLUIR INTERAÇÕES
			
			//excluir interações
			$(document).on("click", ".interacao_excluir", function(e) {	
				//verifica se demonstração tem apenas uma interação. Em caso afirmativo, interação não pode ser excluída
				if($('#sub'+index_subtopico+' .interacao').length == 1){
					alert("Esta demonstração possui apenas uma etapa, por isso não é possível exclui-la.");
				}
				else {
					var confirma = confirm("Tem certeza que deseja EXCLUIR a etapa "+($('.interacao').index($('#interacao'+index_interacao)) + 1)+" desta demonstração?"); 
					if(confirma){
						postXml('formulario_professor', 'excluir_interacao',encontraUrlRetorno());			
					}	
				}
			});
			//adicionar interação antes		
			$(document).on("click", ".interacao_adicionar_antes", function(e) {	
				var confirma = confirm("Tem certeza que deseja adicionar uma etapa antes dessa?"); 
				if(confirma){				
					postXml('formulario_professor', 'adicionar_interacao',encontraUrlRetorno(),'antes');
				}	
			});
			//adicionar interação depois
			$(document).on("click", ".interacao_adicionar_depois", function(e) {			
				var confirma = confirm("Tem certeza que deseja adicionar uma etapa depois dessa?"); 
				if(confirma){				
					postXml('formulario_professor', 'adicionar_interacao',encontraUrlRetorno(),'depois');	
				}	
			});			
			//}}}
			
			
			//{{{FUNÇÃO QUE MOSTRA/ESCONDE a caixa de ferramentas lateral
			function mostra_esconde_ferramentas(){
				//variáveis com o valor do tempo da animação da caixa de ferramentas e da seta 'mostra/esconde'
				var tempo_ferramentas = "normal";
				var tempo_seta = 1;
				
				//evento de clique na seta 'mostra/esconde'
				$(document).on('click', '#mostra_esconde', function(event) { 
				if(!$(this).hasClass('oculto')) {
					//esconde
					$(this).hide();
					$('#formulario_professor').animate({right:-297}, tempo_ferramentas, function(){ 
																				$('#mostra_esconde').stop(true, true);
																				$('#mostra_esconde').css('right',0); 
																				$('#mostra_esconde').show(); 
																				$('#mostra_esconde').blur(); 
																			   });
					$(this).addClass('oculto');
				}
				else {
					//mostra
					$(this).hide();
					$('#formulario_professor').animate({right:0}, tempo_ferramentas, function(){ 
																				$('#mostra_esconde').css('right','295px'); 
																				$('#mostra_esconde').show();
																				$('#mostra_esconde').blur();
																			});
					$(this).removeClass('oculto');
				}		
			});
			//mostra o botão mostra/esconde apenas quando o mouse estiver sobre a caixa de ferramentas
			$('#formulario_professor').hover(function(){
										$('#mostra_esconde').animate({right:295}, tempo_seta);
									  }, function(){
										if(!$('#mostra_esconde').is(":hover")) {
											$('#mostra_esconde').animate({right:274}, tempo_seta);
										}	
									  });
			$('#mostra_esconde').hover(function(){
									$('#mostra_esconde').stop(true, true);
									if($(this).hasClass('oculto')){ $('#mostra_esconde').css('right',0); }
									else{ $('#mostra_esconde').css('right','295px'); }
								}, function(){
									if(!$(this).hasClass('oculto')){ $('#mostra_esconde').animate({right:274}, tempo_seta); }						
								});	
			}
			//}}}
		
			
			// Initialise the table
			$("#tabela_indice_professor").tableDnD();

			
			
			
			
			
	//FIM FUNÇÕES PROFESSOR    AQUI  }}}
});