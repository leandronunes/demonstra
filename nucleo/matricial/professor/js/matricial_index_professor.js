$(document).ready(function(){
	
	// desabilita links com href=# para não irem ao topo da página
	$(document).on("click", "a[href='#']", function(event) {
	   event.preventDefault();
	});

	//INICIALIZAR PLUGINS
		$('.fancybox').fancybox(); //efeito lightbox
		
	//customiza campo para enviar arquivo
	$('input[id=xml_planilha]').change(function() {
		$('#input_xmlplanilha').val($(this).val().replace("C:\\fakepath\\", ""));
	});
	$('input[id=imagens_xml]').change(function() {
		$('#input_imagensxml').val($(this).val().replace("C:\\fakepath\\", ""));
	});
	$('input[id=imagens_base]').change(function() {
		$('#input_imagensbase').val($(this).val().replace("C:\\fakepath\\", ""));
	});
		
	//função de clique no botão DOWNLOAD - baixa o pacote final com todas as demonstrações da visão aluno (pastas html, imagens, js e css)
	$(document).on('click', '.botao_download', function(event) {
		$('#form_exporta input[name=action]').val('download_pacote');
		$('#form_exporta').submit();
	});
	
			
	//função de clique no botão EXPORTAR CSV - baixa arquivo para importação nas planilhas
	$(document).on('click', '.botao_download_csv', function(event) {
		$('#form_exporta input[name=action]').val('download_csv');
		$('#form_exporta').submit();
	});
	
	//função de clique no botão EXPORTAR BACKUP - baixa o pacote final com todas as demonstrações da visão professor
	$(document).on('click', '.botao_download_backup', function(event) {
		$('#form_exporta input[name=action]').val('download_backup');
		$('#form_exporta').submit();
	});
	//função de clique no botão RESTAURAR BACKUP - analisa pacote de backup e atualiza para versão atual
	$(document).on('click', '.botao_restaura_bakup', function(event) {
		$('#form_exporta input[name=action]').val('restaura_backup');
		$('#form_exporta').submit();
	});
	
	
	// clique no menu de categorias
	$(document).on("click", "#menu_categoria ul li a", function() {
		$("#menu_categoria a").removeClass("selecionado");
		$(this).addClass("selecionado");
		
		$(".seta_grande_categorias").remove();
		$(this).after("<div class='seta_grande_categorias'><span>Item selecionado</span></div>");
		
		$(".tabela_wrapper").hide();
		$("#"+$(this).attr("name")).fadeIn("fast");
	});
	
	
	//{{{FUNÇÃO QUE MOSTRA/ESCONDE a caixa de ferramentas lateral
		mostra_esconde_ferramentas();		
		function mostra_esconde_ferramentas(){
			//variáveis com o valor do tempo da animação da caixa de ferramentas e da seta 'mostra/esconde'
			var tempo_ferramentas = "normal";
			var tempo_seta = 1;
				
			//evento de clique na seta 'mostra/esconde'
			$('#mostra_esconde').css('right','261px');
			$(document).on('click', '#mostra_esconde', function(event) { 
				if(!$(this).hasClass('oculto')) {
					//esconde
					$(this).hide();
					$('.ferramentas_indice').animate({right:-285}, tempo_ferramentas, function(){ 
																				$('#mostra_esconde').stop(true, true);
																				$('#mostra_esconde').css('right',0); 
																				$('#mostra_esconde').show(); 
																				$('#mostra_esconde').blur(); 
																			   });
					$(this).addClass('oculto');
				}
				else {
					//mostra
					$(this).hide();
					$('.ferramentas_indice').animate({right:0}, tempo_ferramentas, function(){ 
																				$('#mostra_esconde').css('right','282px'); 
																				$('#mostra_esconde').show();
																				$('#mostra_esconde').blur();
																			});
					$(this).removeClass('oculto');
				}		
			});
			
			//mostra o botão mostra/esconde apenas quando o mouse estiver sobre a caixa de ferramentas
			$('.ferramentas_indice').hover(function(){
										$('#mostra_esconde').animate({right:282}, tempo_seta);
									  }, function(){
										if(!$('#mostra_esconde').is(":hover")) {
											$('#mostra_esconde').animate({right:261}, tempo_seta);
										}	
									  });
			$('#mostra_esconde').hover(function(){
									$('#mostra_esconde').stop(true, true);
									if($(this).hasClass('oculto')){ $('#mostra_esconde').css('right',0); }
									else{ $('#mostra_esconde').css('right','282px'); }
								}, function(){
									if(!$(this).hasClass('oculto')){ $('#mostra_esconde').animate({right:274}, tempo_seta); }						
								});	
		}
	//}}}
	
	//{{{ INICIO HISTORICO MÓDULO  - guarda histórico de navegação para voltar no último módulo aberto
	// lê parâmetro passado via querystring - url da barra de endereços
	var querystring = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&').toString();
	//se foi passado parâmetro, exibe categoria correspondente
	if(querystring.indexOf("mod=") >= 0){ 
		var index_modulo = parseInt(querystring.split("mod=")[1],10);
		$("#menu_categoria a[name='mod"+index_modulo+"']").addClass("selecionado")
														  .after("<div class='seta_grande_categorias'><span>Item selecionado</span></div>");
		$(".tabela_wrapper[id='mod"+index_modulo+"']").fadeIn("fast");
	}	
	// se não, seleciona primeiro item do menu de categorias e mostra texto de início do tutorial
	else {
		$("#menu_categoria a:first").addClass("selecionado")
									 .after("<div class='seta_grande_categorias'><span>Item selecionado</span></div>");
		$(".tabela_wrapper:first").fadeIn("fast");
		//inicioTutorial();
	}	
	//FIM HISTORICO MÓDULO }}}
	
	//{{{ INICIO SOLUÇÃO PARA MESCLAR TÓPICOS
	// mescla as linhas dos tópicos quando elas se repetem
	function mesclaTopicosInicial() {
		var classe_linha_topico = "topico_odd";
		$(".tabela_wrapper table").each(function(i){ 
			$(this).children('tbody').children('tr').children("td:nth-child(1)").each(function(j){
				if(j==0){nome_topico_default = "";}
				if ($(this).text() != nome_topico_default) {
					nome_topico_default = $(this).text();
					$(this).css("color","");
					$(".tabela_wrapper:eq("+i+") td:nth-child(1):eq("+(j-1)+")").css("border-bottom", "");
					//$("body").prepend($(this).parent().index());
					if($(this).parent().index()==0) {
						classe_linha_topico = "";
					}
					else {
						if(classe_linha_topico=="") { classe_linha_topico = "topico_odd"; }
						else if(classe_linha_topico=="topico_odd") { classe_linha_topico = ""; }
					}
					$(this).parent().addClass(classe_linha_topico);
				}
				else {
					$(this).parent().addClass(classe_linha_topico);
					$(this).css("color",$(this).css('backgroundColor'));
					$(".tabela_wrapper:eq("+i+") td:nth-child(1):eq("+(j-1)+")").css("border-bottom", "1px solid transparent");
				}
			});	
		});
	}
	
	// mescla as linhas dos tópicos quando elas se repetem (função chamada quando há alteração no campo de busca)
	function mesclaTopicos(index_tabela) {
		$(".tabela_wrapper:eq("+index_tabela+") td:nth-child(1)").each(function(i){
			if(i==0){nome_topico_default = "";}
			//$("body").prepend(nome_topico_default+" ");
			if ($(this).text() != nome_topico_default) {
				nome_topico_default = $(this).text();
				$(this).css("color","");
				$(".tabela_wrapper:eq("+index_tabela+") td:nth-child(1)").eq(i-1).css("border-bottom", "");
			}
			else {
				$(this).css("color",$(this).css('backgroundColor'));
				if($(this).text() == $(".tabela_wrapper:eq("+index_tabela+") td:nth-child(1)").eq(i).text()){
					$(".tabela_wrapper:eq("+index_tabela+") td:nth-child(1)").eq(i-1).css("border-bottom", "1px solid transparent");
				}
			}
		});
		//$("body").prepend("<br/>");
	}
	
	// chama a função no carregamento da página
	mesclaTopicosInicial();
	
	// chama a função mesclaTopicos a cada alteração no campo de busca
	$(document).on("keyup change",".dataTables_filter input", function(){
		var index_tabela = $(this).parents().filter(".tabela_wrapper").index() - 1;
		mesclaTopicos(index_tabela);
		//$("body").prepend(index_tabela+" ");
	});
	
	//FIM SOLUÇÃO PARA MESCLAR TÓPICOS }}}
	
	

	//função de clique no botão alterar ordem - desabilita links e ativa o plugin arrastar. ao clicar novamente, refaz o xml e salva e reativa links
	$(document).on('click', '.edita_ordem', function(event) { 
			
		//desabilita links das tabelas
		 $('a').contents().unwrap().wrap('');
		
		//liga plugin
		$(".tabela_indice_professor").tableDnD(); //torna linhas de tabelas arrastáveis
		
		//altera xml pelo ajax? ou só ao salvar?
		
	});
	
	
		
});