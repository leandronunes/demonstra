<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" encoding="UTF-8" cdata-section-elements="nome_modulo nome_topico nome_subtopico transicao rotulo_interacao apresentacao importante instrucao informacao print_tela tipo_passo tipo_destaque" />
			
	<xsl:key name="interacao-by-Id_modulo" match="interacao" use="Id_modulo" />
	<xsl:key name="interacao-by-Id_topico" match="interacao" use="concat(Id_modulo, '|', Id_topico)"/>
	<xsl:key name="interacao-by-Id_subtopico" match="interacao" use="concat(Id_topico, '|', Id_subtopico)" />
	<xsl:key name="interacao-by-Id_passo" match="interacao" use="concat(Id_subtopico, '|', Id_passo)" />
	
	<xsl:template match="/">
			<demonstracao sistema="{demonstracao/@sistema}" rotulo_sistema="{demonstracao/@rotulo_sistema}" apresentacao_tutorial="{demonstracao/@apresentacao_tutorial}" responsavel="{demonstracao/@responsavel}"
			versaoxml="{demonstracao/@versaoxml}">
				<xsl:apply-templates />
			</demonstracao>	
	</xsl:template>
	
	<xsl:template match="demonstracao">
		
		<xsl:for-each select="interacao[count(. | key('interacao-by-Id_modulo', Id_modulo)[1]) = 1]">
			<xsl:sort data-type="number" select="Id_modulo" />
			
			<modulo id="{Id_modulo}" nome="{modulo}">
			
				 <xsl:for-each select="key('interacao-by-Id_modulo', Id_modulo)[generate-id() = generate-id(key('interacao-by-Id_topico', concat(Id_modulo, '|', Id_topico))[1])]">

					<xsl:sort data-type="number" select="Id_topico" />
					<topico id="{Id_topico}" nome="{topico}" ordem="{ordem_topico}" rotulo="{rotulo_topico}" >
						
						<rotulo_topico>
							<xsl:value-of select="rotulo_topico" />
						</rotulo_topico>
						
						<xsl:for-each select="key('interacao-by-Id_topico', concat(Id_modulo, '|', Id_topico))[generate-id() = generate-id(key('interacao-by-Id_subtopico', concat(Id_topico, '|', Id_subtopico))[1])]">
							<xsl:sort data-type="number" select="Id_subtopico" />
							<subtopico id="{Id_subtopico}" nome="{subtopico}" ordem="{ordem_subtopico}">
								
								<apresentacao>
									<xsl:value-of select="apres_funcao" />
								</apresentacao>
								<importante>
									<xsl:value-of select="apres_import" />
								</importante>
								<xsl:for-each select="key('interacao-by-Id_subtopico', concat(Id_topico, '|', Id_subtopico))[generate-id() = generate-id(key('interacao-by-Id_passo', concat(Id_subtopico, '|', Id_passo))[1])]">
									<xsl:sort data-type="number" select="Id_passo" />
									<passo id="{Id_passo}" ordem="{ordem_passo}" tipo="{tipo_passo}">
																				
										<xsl:for-each select="key('interacao-by-Id_passo', concat(Id_subtopico, '|', Id_passo))">
											<interacao id="{Id_interacao}" rotulo="{rotulo_interacao}" ordem="{ordem_interacao}" transicao="{transicao}">
												<instrucao>
													<xsl:value-of select="instrucao" />
												</instrucao>
												<informacao>
													<xsl:value-of select="informacao" />
												</informacao>
												<coordenada>
													<xsl:value-of select="coordenada" />
												</coordenada>
												<!--<coordenada>
													<destaque_x><xsl:value-of select="destaque_x" /></destaque_x>
													<destaque_y><xsl:value-of select="destaque_y" /></destaque_y>
													<destaque_largura><xsl:value-of select="destaque_largura" /></destaque_largura>
													<destaque_altura><xsl:value-of select="destaque_altura" /></destaque_altura>
													<texto_x><xsl:value-of select="texto_x" /></texto_x>
													<texto_y><xsl:value-of select="texto_y" /></texto_y>
													<seta_x><xsl:value-of select="seta_x" /></seta_x>
													<seta_y><xsl:value-of select="seta_y" /></seta_y>
												</coordenada>-->
												<tipo_destaque>
													<xsl:value-of select="tipo_destaque" />
												</tipo_destaque>
												<print_tela>
													<xsl:value-of select="print_tela" />
												</print_tela>
												<area_destaque>
													<xsl:value-of select="area_destaque" />
												</area_destaque>
											</interacao>	
										</xsl:for-each>											
									</passo>
								</xsl:for-each>
							</subtopico>
						</xsl:for-each>
					</topico>
				</xsl:for-each>
			</modulo>
		
		</xsl:for-each>
	
	</xsl:template>

</xsl:stylesheet>