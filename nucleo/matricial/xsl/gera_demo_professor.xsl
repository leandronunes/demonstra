<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" indent="yes" />
	
	<xsl:template match="/">
		
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<meta name="description" content="{demonstracao/@sistema}"/>
				<meta name="name" content="Demonstração"/>
				<title><xsl:value-of select="demonstracao/@rotulo_sistema" /></title>
			
				<!-- insere folha de estilo especial para o IE6 -->				
				<xsl:comment>
					<![CDATA[[if IE 6]><link rel="stylesheet" type="text/css" href="../demo/css/matricial_ie6.css" /><![endif]]]>
				</xsl:comment>
				
				<!-- controles para o IE para inputs customizados do tipo "file" -->
				<xsl:comment>
					<![CDATA[[if IE 9]><style>.ie_show { display:block } .ie_hide { display:none }</style><![endif]]]>
				</xsl:comment>

					
				<!--BIBLIOTECAS -->
					
					<!-- js -->
					<script src="bibliotecas/jquery.js" type="text/javascript"></script>
					<script src="bibliotecas/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
					<script src="bibliotecas/jquery.tipTip.minified.js" type="text/javascript"></script>
					<script src="bibliotecas/jquery.dataTables.min.js" type="text/javascript"></script>
					<script src="bibliotecas/jquery.tablednd.0.7.min.js" type="text/javascript"></script>
					<script src="bibliotecas/jquery.fancybox.js?v=2.1.3" type="text/javascript" ></script>
					<script src="bibliotecas/jquery.editable-1.3.3.min.js" type="text/javascript"></script>		
					<script src="bibliotecas/bootstrap/js/bootstrap.js" type="text/javascript"></script>		
					<!-- css -->
					<link  href="bibliotecas/tipTip.css" type="text/css" rel="stylesheet" media="screen"/>
					<link  href="bibliotecas/jquery.fancybox.css?v=2.1.2"  rel="stylesheet" type="text/css"  media="screen" />
					<link  href="bibliotecas/bootstrap/css/bootstrap.css"  rel="stylesheet" type="text/css"  media="screen" />
					
				<!-- demonstra -->
				
				<link href="../demo/imagens/tema/favicon.ico" rel="shortcut icon"  />
				<script src="../demo/js/matricial_demo_professor_aluno.js" type="text/javascript"></script>
		
				
				<script src="js/matricial_demo_professor.js" type="text/javascript"></script>
				<link href="css/matricial_professor.css" rel="stylesheet" type="text/css" media="screen"/>	
	
			</head>
									
			<body id="pagina_demonstracao_professor">

<!-- XML para edição/modo professor -->
			<a href="#" id="mostra_esconde" title="Mostrar/esconder"></a>
			
			<form id="formulario_professor" class="arquivo_xml" method="post" action="../../../nucleo/padroes_demonstra/padrao_sistema.php" name="formulario_professor" enctype="multipart/form-data">
			
			

			<p>
					<!-- botão voltar para Demonstra -->
						<a href="../../../index.php" class="btn btn-block btn-inverse">Voltar para o catálogo de tutoriais</a>
					</p>				
				
					<!-- botão para ver modo aluno -->					
					<p>
					    <a  class="btn btn-block btn-inverse" href="index.html?mod={demonstracao/modulo/@id}" title="Voltar para o índice">Modo professor - Índice</a>
					</p>
					
					<legend>Modo professor - Edição</legend>	
					<a href="../demo/html/demo_{demonstracao/modulo/topico/subtopico/@id}.html" class="botao_modoaluno btn btn-mini btn-warning">Ver modo aluno</a>

				<fieldset>
					
					<input type="hidden" id="action" name="action" class="action" />
					<input type="hidden" id="sistema_form" name="sistema_form" value="{demonstracao/@sistema}" />
					<input type="hidden" id="id_demonstracao" name="id_demonstracao" value="{demonstracao/modulo/topico/subtopico/@id}" />
					<!--<p><xsl:value-of select="demonstracao/modulo/topico/subtopico/@id" /></p>-->
					<textarea class="retorna_xml" id="xml_form" name="xml_form"></textarea>
					<input type="button" class="botao_salvar btn btn-large btn-block btn-info" value="Salvar todas as alterações"/>
					<hr/>
					<p>Trocar imagem da etapa atual:</p>
					<input type="file" name="imagem_substituta" id="imagem_substituta" value="..." placeholder="Arquivo…" class="hide ie_show"/>
					<div class="input-append ie_hide">
					  <input id="input_trocaimagem" class="input_file" type="text" onclick="$('input[id=imagem_substituta]').click();"/>
					  <a class="btn btn-mini" onclick="$('input[id=imagem_substituta]').click();">Escolher arquivo…</a>
					</div>
					<br/>
					<input type="button" class="botao_troca_imagem btn" value="Enviar e trocar imagem"/> 
					<!--<br/>
					<input type="button" class="botao_troca_todas btn" value="Trocar toda a sequencia"/> -->
					
					<hr/>	
					
					<!-- inicio localizar -->
					<p>
						<a href="#modal_localizar_substituir" role="button" data-toggle="modal" data-target="#modal_localizar_substituir" class="botao_localizar_substituir btn">
							<i class="icon-search"></i>&#160;&#160;Localizar e substituir texto
						</a>
					</p>
					
				
					<!-- fim localizar -->
				</fieldset>	
			</form>	

			<!-- Início - Janela modal - Localizar e substituir -->
			<div id='modal_localizar_substituir' class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="form_localizar_substituirLabel" aria-hidden="true">
				<form method="post" action="../../../nucleo/padroes_demonstra/padrao_sistema.php" class="form-horizontal" id="formulario_localizar_substituir" name="formulario_localizar_substituir">
				    <div class="modal-header">
					    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					    <h3 id="form_localizar_substituirLabel">Localizar e substituir texto</h3>
					</div>
					<div class="corpo_modal_localizar modal-body">
						<div class="campos_localizar">
							<!-- inputs localizar e substituir -->
							<div class="control-group">
								<label class="control-label" for="formlocalizar_inputLocalizar">Localizar:</label>
								<div class="controls">
									<input type="text" id="formlocalizar_inputLocalizar" name="inputLocalizar" />
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label" for="formlocalizar_inputSubstituir">Substituir por:</label>
								<div class="controls">
									<input type="text" id="formlocalizar_inputSubstituir" name="inputSubstituir" />
								</div>
							  </div>
							  <div class="control-group">
								<div class="controls">
									<label class="checkbox">
										<input type="checkbox" name="expressao_regular"/> Usar expressão regular
									</label>
									<a href="#" class="botao_localizar btn btn-primary" data-toggle="popover" data-content="A substituição poderá ser confirmada na etapa seguinte.">Buscar em todas as demonstrações</a>
								</div>
							</div>
						</div>
						
						<!-- inputs ocultos -->
						<input type="hidden" id="formsubstituir_action" name="action" class="action" />
						<input type="hidden" id="formsubstituir_sistema_form" name="sistema_form" value="{demonstracao/@sistema}" />
						<input type="hidden" id="formsubstituir_id_demonstracao" name="id_demonstracao" value="{demonstracao/modulo/topico/subtopico/@id}" />
						<input type="hidden" id="formsubstituir_inputLocalizar" name="inputLocalizar" />
						<input type="hidden" id="formsubstituir_inputSubstituir" name="inputSubstituir" />
						<input type="hidden" id="formsubstituir_array_ids_para_substituir" name="array_ids_para_substituir" />
						<textarea id="formsubstituir_xml_form" name="xml_form"></textarea>
						
						<!-- Resultados -->
						<div class="mensagem_resultado_localizar alert alert-info">
							<p>Resultado da busca pelo termo <span class="termo_localizar"></span>. Ele será substituído por <span class="termo_substituir"></span> em <strong>todas</strong> as demonstrações deste tutorial.</p>
							<input type="button" class="botao_pre_visualizar btn btn-small btn-info" data-toggle="button" value="Pré-visualizar nos resultados"/>
						</div>
						<div id="resultado_localizar"></div>
					
					</div>	
                    <div class="modal-footer">
						<div class="confirma_substituir">
							<input type="button" class="botao_nova_busca btn" value="Nova busca"/>
							<input type="button" class="botao_substituir btn btn-primary" value="Substituir"/>
						</div>
                    </div>
				</form>
			</div>
			<!-- Fim - Janela modal - Localizar e substituir -->			
			
<!-- inicio modulos -->
	<xsl:for-each select="demonstracao/modulo">
		<div class="modulo" id="mod{@id}" name="{@nome}">
		
<!-- inicio topicos -->	
			<xsl:for-each select="topico">
				<div class="topico" id="top{@id}" name="{@nome}"> 
				  <div class="meta">
					<div class="ordem"><xsl:value-of select="@ordem" /></div>
					<div class="rotulo"><xsl:value-of select="@rotulo" /></div>
				  </div>		
<!-- inicio subtopicos -->
				  <xsl:for-each select="subtopico">	
					<div class="subtopico" id="sub{@id}" name="{@nome}">
						<div class="meta">
							<div class="ordem"><xsl:value-of select="@ordem" /></div>
						</div>
<!-- inicio header -->			
						<div class="header">
						    <div class="container">
								<div class="busca_interna"></div>
								<div class="breadcrumb">
									<ul>
										<li class="first"><a href="index.html?mod={../../@id}" title="Voltar para o índice" /></li>
										<li><xsl:value-of select="../../@nome" /></li>
										<li><xsl:value-of select="../@nome" /></li>
										<li class="last"><xsl:value-of select="@nome" /></li>
									</ul>
								</div>
								<!--<div class="logo">
									<h1><a title="{../../../@nome}" href="index.html?mod={../../@id}"><xsl:value-of select="../../../@nome" /></a></h1>
								</div>-->
							</div>	
						</div>
<!-- fim header -->	
					  <div class="main">
<!-- inicio nav subtopico e descricao -->
						<h3 name="{../@id}" class="nome_topico"><xsl:value-of select="../@nome" /></h3>
						<!--<div class="navegacao_subtopicos">
							
							<div class="menu_subtopico">
								<div class="paginacao">
									<xsl:for-each select="../subtopico">	
										<a class="link_subtopico" name="{@id}" title="{@nome}" href="#"/>
									</xsl:for-each>
								</div>
							</div>
						</div>-->
						<div class="descricao_acao">
							<h4 class="acao"><xsl:value-of select="@nome" /></h4>
							
							<!-- Se não houver texto apresentação, insere botão para adicionar o texto -->	
							<xsl:choose>
								<xsl:when test="apresentacao!='' and apresentacao!='*'">
									<div class="apresentacao"><xsl:value-of disable-output-escaping="yes" select="apresentacao" /></div>
									<a class="adicionar_apresentacao btn" style="display:none"><i class="icon-plus-sign"></i> Adicionar texto de apresentação</a>
								</xsl:when>
								<xsl:otherwise>
									<div class="apresentacao"></div>
									<a class="adicionar_apresentacao btn"><i class="icon-plus-sign"></i> Adicionar texto de apresentação</a>
								</xsl:otherwise>
							</xsl:choose>
							<!-- Se não houver texto importante, insere botão para adicionar o texto 	
							<xsl:choose>
								<xsl:when test="importante!='' and importante!='*'">
									<div class="importante"><h5>Importante</h5><xsl:value-of disable-output-escaping="yes" select="importante" /></div>
									<a class="adicionar_importante btn" style="display:none"><i class="icon-plus-sign"></i> Texto importante</a>
								</xsl:when>
								<xsl:otherwise>
									<div class="importante"></div>
									<a class="adicionar_importante btn"><i class="icon-plus-sign"></i> Texto importante</a>
								</xsl:otherwise>
							</xsl:choose> -->
							
						</div>
<!-- fim nav subtopico e descricao -->							
					  </div>

<!-- Menu de navegação entre interações -->
					  <div class="navegacao_interacoes">
						<div class="paginacao">
							<a href="#" class="anterior" title="Anterior"> « </a>
							<xsl:for-each select="passo/interacao">	
								<a class="link_interacao" name="{@id}" href="#">
									<xsl:text> </xsl:text>
									<xsl:value-of select="position()" />
									<xsl:text> </xsl:text>
								</a>
							</xsl:for-each>
							<a href="#" class="proximo" title="Próximo"> » </a>
						</div>
					  </div>
					  
<!-- INÍCIO DA DEMONSTRAÇÃO -->
					  <div class="demonstracao">
					  
<!-- inicio passos -->	
					  <xsl:for-each select="passo">
						<div class="passo {@tipo}" id="passo{@id}" >
						  <div class="meta">
							<div class="ordem"><xsl:value-of select="@ordem" /></div>
							<div class="tipo_passo"><xsl:value-of select="@tipo" /></div>
						  </div>  						
						  
						  <xsl:choose>
<!-- LAYOUT INFORMACAO -->
							<xsl:when test="@tipo='informacao'">
<!-- Modelo -->
								<div class="modelo">
									<div class="area_interacao">
<!-- Orientação -->					
										<div class="orientacao">
											<p class="xml_texto_principal">
												<xsl:value-of disable-output-escaping="yes" select="hit_auxilio/hit/texto_principal"/>
											</p>
											<input type="button" class="botao_avanca_passo btn" value="Seguir"/>
										</div>
										<span class="url_img">../demo/imagens/<xsl:value-of select="print_tela" /></span>
									</div>	
								</div>

							</xsl:when>
							
<!-- LAYOUT DEMONSTRAÇÃO -->
						    <xsl:when test="@tipo='demonstracao'"> 
<!-- início interação -->
								<xsl:for-each select="interacao">
														
								  <!--<div id="passo{../@id}_interacao{@ordem}" class="interacao" name="{@rotulo}">-->
								  <div id="interacao{@id}" class="interacao">
									<div class="meta">
										<div class="rotulo"><xsl:value-of select="@rotulo" /></div>
										<div class="ordem"><xsl:value-of select="@ordem" /></div>
										<div class="transicao"><xsl:value-of select="@transicao" /></div>
										<div class="coordenada">
											<xsl:value-of select="coordenada/destaque_x" />_<xsl:value-of select="coordenada/destaque_y" />_<xsl:value-of select="coordenada/destaque_largura" />_<xsl:value-of select="coordenada/destaque_altura" />_<xsl:value-of select="coordenada/texto_x" />_<xsl:value-of select="coordenada/texto_y" />_<xsl:value-of select="coordenada/seta_x" />_<xsl:value-of select="coordenada/seta_y" />
										</div>
									</div>  
<!-- print_tela -->
									<img src="../demo/imagens/{print_tela}" class="print_tela" alt="" style=""/>
									<b>Imagem:</b>&#160;<xsl:value-of select="print_tela" />
									<xsl:if test="area_destaque!='' and area_destaque!='*'">
										<br/>
										<b>Destaque:</b>&#160;<xsl:value-of select="area_destaque" />
									</xsl:if>
<!-- destaque -->
									<div id="" class="hit {tipo_destaque}" style="position: absolute; width: {coordenada/destaque_largura}px; height: {coordenada/destaque_altura}px; display: block; top: {coordenada/destaque_y}px; left: {coordenada/destaque_x}px; opacity: 1;"/>
<!-- texto: informação + instrução -->									
									<div id="passo{../@id}_texto{@id}" class="texto_hit {tipo_destaque}" style="top: {coordenada/texto_y}px; left: {coordenada/texto_x}px; cursor: auto; display: block;">
									
<!-- ferramentas de edição da interação / caixa de texto -->									
										<div class="edita_interacao">
											<div class="btn-group">
												<a class="config_interacao btn btn-info btn-small"><i class="icon-cog icon-white"></i>Configurar</a>
												<a class="mover_caixatexto btn btn-info btn-small" title="Clique e arraste para mover a caixa de texto"><i class="icon-move icon-white"></i>Mover</a>
											</div>	
											<div class="painelconfig_interacao">
												<div class="text-left">
													<a class="config_interacao btn btn-info btn-small"><i class="icon-cog icon-white"></i>Configurar</a>
												</div>	
												<br/>
												<div class="altera_tipo_hit">
													Tipo de destaque:<br/>
													<div class="btn-group" data-toggle="buttons-radio">
														<a class="troca_tipohit borda btn btn-mini" title="borda">borda</a>
														<a class="troca_tipohit mascara btn btn-mini" title="mascara">máscara</a>
														<a class="troca_tipohit informacao btn btn-mini" title="informacao">informação</a>
													</div>	
													<p class="muted">
														Selecionado: <span class="tipo_hit_selecionado"><xsl:value-of select="tipo_destaque" /></span>
													</p>	
												</div>
												<hr/>
												Adicionar textos:<br/>
												<div class="btn-group">
													<!-- insere botão para adicionar texto INFORMAÇÃO. Se não houver texto deixa botão desabilitado. -->
													<xsl:choose>
														<xsl:when test="informacao='' or informacao='*'">
															<a class="adicionar_informacao btn btn-mini"><i class="icon-info-sign"></i> informação</a>
														</xsl:when>
														<xsl:otherwise>
															<a class="adicionar_informacao btn btn-mini disabled"><i class="icon-info-sign"></i> informação</a>
														</xsl:otherwise>
													</xsl:choose>
													<!-- insere botão para adicionar texto INSTRUÇÃO. Se não houver texto deixa botão desabilitado. -->
													<xsl:choose>
														<xsl:when test="instrucao='' or instrucao='*'">
															<a class="adicionar_instrucao btn btn-mini"><i class="icon-hand-up"></i> instrução</a>
														</xsl:when>
														<xsl:otherwise>
															<a class="adicionar_instrucao btn btn-mini disabled"><i class="icon-hand-up"></i> instrução</a>
														</xsl:otherwise>
													</xsl:choose>
												</div>	
												<hr/>
												<!-- insere botões para adicionar e excluir interação -->
												Adicionar nova interação:<br/>
												<div class="btn-group">
													<a class="interacao_adicionar_antes btn btn-mini" title="Adicionar interação antes"><i class="icon-circle-arrow-left"></i> Antes</a> <a class="interacao_adicionar_depois btn btn-mini" title="Adicionar interação depois">Depois <i class="icon-circle-arrow-right"></i></a>
												</div>
												<p><br/><a class="interacao_excluir btn btn-mini" title="Excluir interação"><i class="icon-remove"></i> Excluir interação</a></p>												
											</div>
										</div>
										
										<div class="hit_etapa">(Etapa xx de <xsl:value-of select="count(../..//interacao)"/>)</div>
										<div class="caixa_texto_hit"><span class="texto_informacao"><xsl:value-of disable-output-escaping="yes" select="informacao"/></span><xsl:if test="informacao!='' and informacao!='*' and instrucao!='' and instrucao!='*'"><xsl:text> </xsl:text></xsl:if><span class="texto_instrucao"><xsl:value-of disable-output-escaping="yes" select="instrucao"/></span>
										<div id="rotulointeracao{@rotulo}" name="interacao{@id}" class="id_interacao">Referência (copie para o Mantis o texto em vermelho):<div class="referencia">demo_<xsl:value-of select="../../../../@rotulo"/>.html?interacao=<xsl:value-of select="@rotulo"/></div></div></div>
										<xsl:if test="tipo_destaque='mascara' or tipo_destaque='informacao'"><input type="button" value="Seguir" class="botao_seguir"/></xsl:if>
										<div class="hit_seta" style="top: {coordenada/seta_y}px; left: {coordenada/seta_x}px; background-position: left top;"/>
									</div>
										
								  </div>					
								</xsl:for-each>
<!-- fim interação -->
							</xsl:when>	
						  							
						  </xsl:choose>						  
						
						</div>
					  </xsl:for-each>
<!-- fim passos -->
					  </div>
<!-- FIM DA DEMONSTRAÇÃO -->					  

<!-- proxima ação/subtopico -->
					  <xsl:if test="following-sibling::*[1]/@nome != ''">
					    <div class="proxima_acao">
							<p>A próxima ação no índice é <a class="link_proxima_acao" name="{following-sibling::*[1]/passo/interacao/@id}" href="#"><xsl:value-of select="following-sibling::*[1]/@nome"/></a>.</p>
					    </div>
					  </xsl:if>
					  
					  
					</div>
				  </xsl:for-each>	
<!-- fim subtopicos -->	
				
				</div>
			</xsl:for-each>	
<!-- fim topicos -->

		</div>			
	</xsl:for-each>	
<!-- fim modulos -->	


<!-- plugin tooltip -->
				<div style="" id="tiptip_holder">
					<div id="tiptip_arrow">
						<div id="tiptip_arrow_inner"/>
					</div>
					<div id="tiptip_content"/>
				</div>
				
<!-- glossario -->
				<div class="glossario">
					<!--<dl>
						<dt></dt>
						<dd><br/><em>Fonte: </em></dd>
					</dl>-->
				</div>				
									
			</body>
		</html>
	
							
	</xsl:template>
						
</xsl:stylesheet>

