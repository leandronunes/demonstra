<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" indent="yes"/>
	
	<xsl:template match="/">
		
		<html>
		
		<head>
			
				<title><xsl:value-of select="demonstracao/@rotulo_sistema" /></title>
				
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<meta name="description" content="{demonstracao/@sistema}"/>
				<meta name="name" content="Demonstração"/>
				
				
				<!--BIBLIOTECAS -->
					
					<!-- js -->
					<script src="../bibliotecas/jquery.js" type="text/javascript"></script>
					<script src="../bibliotecas/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
					<script src="../bibliotecas/jquery.tipTip.minified.js" type="text/javascript"></script>
					<script src="../bibliotecas/jquery.dataTables.min.js" type="text/javascript"></script>
					<script type="text/javascript" src="https://coletajavascript.serpro.gov.br/estatistica.js"></script>
						
					<!-- css -->
					<link  href="../bibliotecas/tipTip.css" type="text/css" rel="stylesheet" media="screen"/>
					
					
				<!-- demonstra -->
				
				<link href="../imagens/tema/favicon.ico" rel="shortcut icon"  />
				<link href="../css/matricial_aluno.css" rel="stylesheet" type="text/css" media="screen"/>
				<script src="../js/matricial_index_aluno.js" type="text/javascript"></script>
				
			
			</head>
		
		
			<body id="indice_demonstracoes" class="index">
			<div id="wrapper">
				<div class="header">
					<div class='texto_intro_tutorial'>
						<h1><xsl:value-of select="demonstracao/@rotulo_sistema" /></h1>
						<p><xsl:value-of select="demonstracao/@apresentacao_tutorial" /></p>
						<p>Para iniciar o Tutorial, clique em Exibir Tutorial. <!-- Para conhecer como está organizado o Tutorial, siga em Como navegar. --> </p>
						<div class='botoes_intro_tutorial'>
							<input id='inicia_tutorial' type='button' value='Exibir tutorial'/>
						<!--	<input id='inicia_comonavegar' type='button' value='Como navegar'/> -->
						</div>
					</div>
					<div class="busca_interna"></div>
					<div class="logo index">
						<h1><a href="index.html" title="{demonstracao/@rotulo_sistema}"><xsl:value-of select="demonstracao/@rotulo_sistema" /></a></h1>
					</div>
					<!--<div class="EHhelpBox"><a rel='navegacao_indice' href='#'>Como navegar no índice</a></div>-->
				</div>

				<div class="main index">
					<div id="menu_categoria">
						<h2>Selecione uma categoria:</h2>
						<ul>
							<xsl:for-each select="demonstracao/modulo">
								<li><a href="#" name="mod{@id}"><xsl:value-of select="@nome" /></a></li>
							</xsl:for-each>
						</ul>
					</div>
					
					<xsl:for-each select="demonstracao/modulo">
						<div class="tabela_wrapper" id="mod{@id}">
							<!--<h3><span>Resultados para <xsl:value-of select="@nome" />:</span></h3>-->
				
						  <div class="tabela_topicos">
						
							<table caption="{demonstracao/@nome}" summary="Índice - {demonstracao/@nome}">
								<thead>
									<tr>
										<th class="titulo_coluna">Tópico</th>
										<th class="titulo_coluna">O que você deseja fazer?</th>
									</tr>
								</thead>
<!-- inicio topicos -->			
								<tbody>
									<xsl:for-each select="topico">
									
<!-- inicio subtopicos -->
										<xsl:for-each select="subtopico">										
											<tr>
												<td class="titulo_linha1">
												<!--<td class="titulo_linha1" name="{../assunto}">-->
												<!--<td>-->
													<xsl:value-of disable-output-escaping="yes" select="../@nome" />
												</td>
												<td>
													<a href="demo_{@id}.html" name="{@id}"><xsl:value-of disable-output-escaping="yes" select="@nome" /></a>
												</td>
											</tr>
										</xsl:for-each>	
<!-- fim subtopicos -->										
										
									</xsl:for-each>	
								</tbody>
<!-- fim topicos -->										
								
							</table>
							
						  </div>
						</div>
					</xsl:for-each>	
<!-- fim modulos -->						  
						  
					
				</div>
			</div>

			</body>
		</html>
	
							
	</xsl:template>
						
</xsl:stylesheet>

