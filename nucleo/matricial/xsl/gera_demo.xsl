<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" indent="yes" />
	
	<xsl:template match="/">
		
		<html>
			<head>
			
			
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<meta name="description" content="{demonstracao/@sistema}"/>
				<meta name="name" content="Demonstração"/>
				
				<title><xsl:value-of select="demonstracao/@rotulo_sistema" /></title>
				<link rel="shortcut icon" href="../imagens/tema/favicon.ico" />
				
				
				<!--BIBLIOTECAS -->
				
				<!-- js -->
				<script src="../bibliotecas/jquery.js" type="text/javascript"></script>
				<script src="../bibliotecas/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
				<script src="../bibliotecas/jquery.tipTip.minified.js" type="text/javascript"></script>
				<script src="../bibliotecas/jquery.dataTables.min.js" type="text/javascript"></script>	
				<script type="text/javascript" src="https://coletajavascript.serpro.gov.br/estatistica.js"></script>
				
				<!-- css -->
				<link  href="../bibliotecas/tipTip.css" type="text/css" rel="stylesheet" media="screen"/>
				
				<!-- demonstra -->
				
				<link href="../imagens/tema/favicon.ico" rel="shortcut icon"  />
				<link href="../css/matricial_aluno.css" rel="stylesheet" type="text/css" media="screen"/>				
				<script src="../js/matricial_demo_professor_aluno.js" type="text/javascript"></script>
				
				
				
				<!-- insere folha de estilo especial para o IE6 -->				
				<xsl:comment>
					<![CDATA[[if IE 6]><link rel="stylesheet" type="text/css" href="../css/matricial_ie6.css" /><![endif]]]>
				</xsl:comment>
				
			</head>
									
			<body id="pagina_demonstracao">


<!-- inicio modulos -->
	<xsl:for-each select="demonstracao/modulo">
		<div class="modulo" id="mod{@id}" name="{@nome}">
		
<!-- inicio topicos -->	
			<xsl:for-each select="topico">
				<div class="topico" id="top{@id}" name="{@nome}"> 
				  <div class="meta">
					<div class="ordem"><xsl:value-of select="@ordem" /></div>
					<div class="rotulo"><xsl:value-of select="@rotulo" /></div>
				  </div>	
<!-- inicio subtopicos -->
				  <xsl:for-each select="subtopico">	
					<div class="subtopico" id="sub{@id}" name="{@nome}">
						<div class="meta">
							<div class="ordem"><xsl:value-of select="@ordem" /></div>
						</div>
<!-- inicio header -->			
						<div class="header">
						    <div class="container">
								<div class="busca_interna"></div>
								<div class="breadcrumb">
									<ul>
										<li class="first"><a href="index.html?mod={../../@id}" title="Voltar para o índice" /></li>
										<li><xsl:value-of select="../../@nome" /></li>
										<li><xsl:value-of select="../@nome" /></li>
										<li class="last"><xsl:value-of select="@nome" /></li>
									</ul>
								</div>
								<div class="logo">
									<h1><a title="{../../../@nome}" href="index.html?mod={../../@id}"><xsl:value-of select="../../../@nome" /></a></h1>
								</div>
							</div>	
						</div>
<!-- fim header -->	
					  <div class="main">
						<!-- excluido do padrão <div class="EHhelpBox"><a rel='navegacao_demonstracao1' href='#'>Como navegar na demonstração</a></div>-->	
<!-- inicio nav subtopico e descricao -->
						<h3 name="{../@id}" class="nome_topico"><xsl:value-of select="../@nome" /></h3>
						<div class="navegacao_subtopicos">
							
						<!-- excluido do padrão
							<div class="menu_subtopico">
								<div class="paginacao">
									<xsl:for-each select="../subtopico">	
										<a class="link_subtopico" name="{@id}" title="{@nome}" href="#"/>
									</xsl:for-each>
								</div>
							</div>
							-->	
							
						</div>
						<div class="descricao_acao">
							<h4 class="acao"><xsl:value-of select="@nome" /></h4>
			
						<xsl:if test="apresentacao!=''">
								<div class="apresentacao"><xsl:value-of disable-output-escaping="yes" select="apresentacao" /></div>
							</xsl:if>	
						<!-- trecho a seguir comentado para testes. Excluir.				
						<xsl:if test="importante!='' and importante!='*'">
								<div class="importante"><h5>Importante</h5><xsl:value-of disable-output-escaping="yes" select="importante" /></div>
							</xsl:if>  -->
						</div>
<!-- fim nav subtopico e descricao -->							
					  </div>

<!-- Menu de navegação entre interações -->
					  <div class="navegacao_interacoes">
						<div class="paginacao">
							<a href="#" class="anterior" title="Anterior"> « </a>
							<xsl:for-each select="passo/interacao">	
								<a class="link_interacao" name="{@id}" href="#">
									<xsl:text> </xsl:text>
									<xsl:value-of select="position()" />
									<xsl:text> </xsl:text>
								</a>
							</xsl:for-each>
							<a href="#" class="proximo" title="Próximo"> » </a>
						</div>
					  </div>
					  
<!-- INÍCIO DA DEMONSTRAÇÃO -->
					  <div class="demonstracao">
					  
<!-- inicio passos -->	
					  <xsl:for-each select="passo">
						<div class="passo {@tipo}" id="passo{@id}" >
						  <div class="meta">
							<div class="ordem"><xsl:value-of select="@ordem" /></div>
							<div class="tipo_passo"><xsl:value-of select="@tipo" /></div>
						  </div>  
						  <xsl:choose>
<!-- LAYOUT INFORMACAO -->
							<xsl:when test="@tipo='informacao'">
<!-- Modelo -->
								<div class="modelo">
									<div class="area_interacao">
<!-- Orientação -->					
										<div class="orientacao">
											<p class="xml_texto_principal">
												<xsl:value-of disable-output-escaping="yes" select="hit_auxilio/hit/texto_principal"/>
											</p>
											<input type="button" class="botao_avanca_passo" value="Seguir"/>
										</div>
										<span class="url_img">../imagens/<xsl:value-of select="print_tela" /></span>
									</div>	
								</div>

							</xsl:when>
							
<!-- LAYOUT DEMONSTRAÇÃO -->
						    <xsl:when test="@tipo='demonstracao'"> 
<!-- início interação -->
								<xsl:for-each select="interacao">
														
								  <!--<div id="passo{../@id}_interacao{@ordem}" class="interacao" name="{@rotulo}">-->
								  <div id="interacao{@id}" class="interacao">
									<div class="meta">
										<div class="rotulo"><xsl:value-of select="@rotulo" /></div>
										<div class="ordem"><xsl:value-of select="@ordem" /></div>
										<div class="transicao"><xsl:value-of select="@transicao" /></div>
										<div class="coordenada">
											<xsl:value-of select="coordenada/destaque_x" />_<xsl:value-of select="coordenada/destaque_y" />_<xsl:value-of select="coordenada/destaque_largura" />_<xsl:value-of select="coordenada/destaque_altura" />_<xsl:value-of select="coordenada/texto_x" />_<xsl:value-of select="coordenada/texto_y" />_<xsl:value-of select="coordenada/seta_x" />_<xsl:value-of select="coordenada/seta_y" />											
										</div>
									</div>  
<!-- print_tela -->
									<img src="../imagens/{print_tela}" class="print_tela" alt="" style=""/>
<!-- destaque -->
									<div id="" class="hit {tipo_destaque}" style="position: absolute; width: {coordenada/destaque_largura}px; height: {coordenada/destaque_altura}px; display: block; top: {coordenada/destaque_y}px; left: {coordenada/destaque_x}px; opacity: 1;"/>
<!-- texto: informação + instrução -->									
									<div id="passo{../@id}_texto{@id}" class="texto_hit {tipo_destaque}" style="top: {coordenada/texto_y}px; left: {coordenada/texto_x}px; cursor: auto; display: block;">
										<div class="navegacao_hits">
											<input type="button" title="Anterior" value="" class="hit_anterior"/>
											<input type="button" title="Próximo" value="" class="hit_proximo"/>
										</div>
										<div class="hit_etapa">
											(Etapa xx de <xsl:value-of select="count(../..//interacao)"/>)
										</div>
										<div class="caixa_texto_hit"><xsl:if test="informacao!='' and informacao!='*'"><span class="texto_informacao"><xsl:value-of disable-output-escaping="yes" select="informacao"/></span><xsl:text> </xsl:text></xsl:if><xsl:if test="instrucao!='' and instrucao!='*' and tipo_destaque!='mascara'"><span class="texto_instrucao"><xsl:value-of disable-output-escaping="yes" select="instrucao"/></span></xsl:if><div id="rotulointeracao{@rotulo}" name="interacao{@id}" class="id_interacao">Referência (copie para o Mantis o texto em vermelho):<div class="referencia">demo_<xsl:value-of select="../../../../@rotulo"/>.html?interacao=<xsl:value-of select="@rotulo"/></div></div></div>
										<xsl:if test="tipo_destaque='mascara' or tipo_destaque='informacao'"><input type="button" value="Seguir" class="botao_seguir"/></xsl:if>
										<div class="hit_seta" style="top: {coordenada/seta_y}px; left: {coordenada/seta_x}px; background-position: left top;"/>
									</div>
										
								  </div>					
								</xsl:for-each>
<!-- fim interação -->
							</xsl:when>	
						  							
						  </xsl:choose>						  
						
						</div>
					  </xsl:for-each>
<!-- fim passos -->
					  </div>
<!-- FIM DA DEMONSTRAÇÃO -->					  

<!-- proxima ação/subtopico -->
					  <xsl:if test="following-sibling::*[1]/@nome != ''">
					    <div class="proxima_acao">
							<p>A próxima ação no índice é <a class="link_proxima_acao" name="{following-sibling::*[1]/passo/interacao/@id}" href="#"><xsl:value-of select="following-sibling::*[1]/@nome"/></a>.</p>
					    </div>
					  </xsl:if>
					  
					  
					</div>
				  </xsl:for-each>	
<!-- fim subtopicos -->	
				
				</div>
			</xsl:for-each>	
<!-- fim topicos -->

		</div>			
	</xsl:for-each>	
<!-- fim modulos -->	


<!-- plugin tooltip -->
				<div style="" id="tiptip_holder">
					<div id="tiptip_arrow">
						<div id="tiptip_arrow_inner"/>
					</div>
					<div id="tiptip_content"/>
				</div>
				
									
			</body>
		</html>
	
							
	</xsl:template>
						
</xsl:stylesheet>

