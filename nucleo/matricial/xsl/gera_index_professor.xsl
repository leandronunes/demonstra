<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" indent="yes"/>
	
	<xsl:template match="/">
		
		<html>
			<head>
			
				<title><xsl:value-of select="demonstracao/@rotulo_sistema" /></title>
				
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<meta name="description" content="{demonstracao/@sistema}"/>
				<meta name="name" content="Demonstração"/>
				
				
				<!--BIBLIOTECAS -->
					
					<!-- js -->
					<script src="bibliotecas/jquery.js" type="text/javascript"></script>
					<script src="bibliotecas/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
					<script src="bibliotecas/jquery.tipTip.minified.js" type="text/javascript"></script>
					<script src="bibliotecas/jquery.dataTables.min.js" type="text/javascript"></script>
					<script src="bibliotecas/jquery.tablednd.0.7.min.js" type="text/javascript"></script>
					<script src="bibliotecas/jquery.fancybox.js?v=2.1.3" type="text/javascript" ></script>
					<script src="bibliotecas/jquery.editable-1.3.3.min.js" type="text/javascript"></script>		
					<script src="bibliotecas/bootstrap/js/bootstrap.js" type="text/javascript"></script>		
					<!-- css -->
					<link  href="bibliotecas/tipTip.css" type="text/css" rel="stylesheet" media="screen"/>
					<link  href="bibliotecas/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet" media="screen"/>
					<link  href="bibliotecas/jquery.fancybox.css?v=2.1.2"  rel="stylesheet" type="text/css"  media="screen" />
					
				<!-- demonstra -->
				
				<link href="../demo/imagens/tema/favicon.ico" rel="shortcut icon"  />
					
				<script src="js/matricial_index_professor.js" type="text/javascript"></script>
				<link href="css/matricial_professor.css" rel="stylesheet" type="text/css" media="screen"/>
				
			</head>
			
			<body id="indice_demonstracoes" class="index">
				<div id="wrapper">
					<div class="header">	
						<h1>Tutorial: <b><xsl:value-of select="demonstracao/@rotulo_sistema" /></b></h1>
					</div>
					<div class="main index">
						<div id="menu_categoria">
							<h2>Selecione uma categoria:</h2>
							<ul>
								<xsl:for-each select="demonstracao/modulo">
									<li><a href="#" name="mod{@id}"><xsl:value-of select="@nome" /></a></li>
								</xsl:for-each>
							</ul>
						</div>
						
						<a href="#" id="mostra_esconde" title="Mostrar/esconder"></a>
						<div class="ferramentas_indice">
								<p>
								<!-- botão voltar para Demonstra -->
								<a href="../../../index.php" class="btn btn-block btn-inverse">Voltar para o catálogo de tutoriais</a>
							</p>
							
								<legend>Modo professor - Índice</legend>		
							<!-- botão para ver modo aluno -->
							<a href="../demo/html/" class="botao_modoaluno btn btn-mini btn-warning">Ver modo aluno</a>
							
											
							<p>
								Tutorial: <b><xsl:value-of select="demonstracao/@rotulo_sistema" /></b>
								<br/>
								<span class="muted">Responsável: <b><xsl:value-of select="demonstracao/@responsavel" /></b></span>
								<br/>
								<span class="muted">VersãoXML: <b><xsl:value-of select="demonstracao/@versaoxml" /></b></span>
							</p>
							
							<hr/>
							
							<ul class="nav nav-tabs nav-stacked">
								<li>
									<!-- Form atualiza XML -->
									<a href='#form_atualiza_xml' role="button" data-toggle="modal"><i class="icon-refresh"></i>&#160;&#160;Atualizar XML</a>
								</li>
								<li>
									<!-- Form Envia Imagens -->
									<a href='#form_envia_imagens' role="button" data-toggle="modal"><i class="icon-picture"></i>&#160;&#160;Enviar ZIP de imagens</a>
								</li>
								<li>
									<!-- Form Envia Imagens -->
									<a href='#form_redimensiona_coordenadas' role="button" data-toggle="modal"><i class="icon-resize-full"></i>&#160;&#160;Alterar dimensão da imagem</a>
								</li>
							</ul>
							
							<!-- Janela modal - Form atualiza XML -->
							<div id='form_atualiza_xml' class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="form_atualiza_xmlLabel" aria-hidden="true">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									<h3 id="form_atualiza_xmlLabel">Atualizar XML do tutorial</h3>
								</div>
								<form action='../../../nucleo/edita_em_massa.php' method='post' enctype='multipart/form-data'>
									<div class="modal-body">
										<label>Envie um arquivo XML</label>
										<input type="file" name="xml_planilha" id="xml_planilha" class="hide ie_show"/>
										<div class="input-append ie_hide">
										  <input id="input_xmlplanilha" type="text" onclick="$('input[id=xml_planilha]').click();"/>
										  <a class="btn" onclick="$('input[id=xml_planilha]').click();">Escolher arquivo…</a>
										</div>
										<input type='hidden' name='tutorial_existente' value="{demonstracao/@sistema}"/>
										<p class="linha_ou"><span class="traco">&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</span><span class="label label-info ou"> ou </span><span class="traco">&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</span></p>
										<label>Cole o conteúdo do XML diretamente no campo abaixo</label>
										<textarea name="textarea_xml" id="textarea_xml"></textarea>
									</div>
									<div class="modal-footer">
										<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
										<button class="btn btn-primary" name='enviar_conteudo_xml'>Enviar</button>
									</div>	
								</form>
							</div>													
							<!-- Janela modal - Form Envia Imagens -->	
							<div id='form_envia_imagens' class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="form_envia_imagensLabel" aria-hidden="true">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									<h3 id="form_envia_imagensLabel">Enviar ZIP com imagens</h3>
								</div>
								<form action='../../../nucleo/edita_em_massa.php' method='post' enctype='multipart/form-data'>
									<div class="modal-body">
										<div class="alert alert-block">
										  <h4>Importante!</h4>
										  Ao enviar imagens com o mesmo nome de imagens existentes, elas serão substituídas.
										</div>
										<input type="file" name="imagens_xml" id="imagens_xml" class="hide ie_show"/>
										<div class="input-append ie_hide">
										  <input id="input_imagensxml" type="text" onclick="$('input[id=imagens_xml]').click();"/>
										  <a class="btn" onclick="$('input[id=imagens_xml]').click();">Escolher arquivo…</a>
										</div>
										<input type='hidden' name='tutorial_existente' value="{demonstracao/@sistema}"/>
									</div>
									<div class="modal-footer">
										<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
										<button class="btn btn-primary" name='enviar_imagens_xml'>Enviar</button>
									</div>	
								</form>
							</div>
							<!-- Janela modal - Form Altera coordenadas -->	
							<div id='form_redimensiona_coordenadas' class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="form_envia_imagensLabel" aria-hidden="true">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									<h3 id="form_envia_imagensLabel">Alterar Largura da Imagem</h3>
								</div>
								<form action='../../../nucleo/edita_em_massa.php' method='post' enctype='multipart/form-data'>
									<div class="modal-body">
										<div class="alert alert-block">
										  <h4>Importante!</h4>
										  Após mudar as dimensões, faça uma revisão para ajustar a posição das as setas das caixas de texto e destaques.
										</div>
										
										Nova largura: <input type="text" name="largura_nova" id="largura_nova" class="hide ie_show"/>											
										<input type='hidden' name='tutorial_existente' value="{demonstracao/@sistema}"/>
									</div>
									<div class="modal-footer">
										<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
										<button class="btn btn-primary" name='enviar_coordenadas_xml'>Enviar</button>
									</div>	
								</form>
							</div>
							
							<hr/>
							
							<!-- botão cria nova demo -->
							<a data-toggle="modal" class="btn btn-inverse btn-block" role="button" href="#form_cria_demo{@id}">Criar nova demonstração</a>
							<!-- Janela modal - botão cria nova demo -->
							<div id='form_cria_demo{@id}' class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="cria_demo{@id}Label" aria-hidden="true" >
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									<h3 id="cria_demo{@id}Label">Criar nova demonstração no tutorial a partir de imagens capturadas</h3>
								</div>
								<form action='../../../nucleo/cria_demos.php' method='post' enctype='multipart/form-data'>
									<div class="modal-body">
										<input type='hidden' name='tutorial_existente' value="{demonstracao/@sistema}"/>
										<label>Nome da demonstração</label>
										<input type='text' name='demonstracao' id='demonstracao'/>
										<span class="help-block">Escreva a ação que o usuário saberá realizar após usar a demonstração (exemplo: 'Operar o sistema Mantis' ou 'Incluir casos no Mantis').</span>
										<br/>
										<label>Imagens</label>
										<input type="file" name="imagens_base" id="imagens_base" class="hide ie_show"/>
										<div class="input-append ie_hide">
											<input id="input_imagensbase" type="text" onclick="$('input[id=imagens_base]').click();"/>
											<a class="btn" onclick="$('input[id=imagens_base]').click();">Escolher arquivo…</a>
										</div>
										<span class="help-block">Faça o upload de um arquivo ZIP com todas as imagens ordenadas.</span>
									</div>
									<div class="modal-footer">
										<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
										<button class="btn btn-primary" name='cria_demo'>Criar demonstração</button>
									</div>
								</form>	
							</div>
							
							<hr/>
							
							<p>Exportar:</p>
							<form action='../../../nucleo/padroes_demonstra/padrao_sistema.php' id='form_exporta' method='post'>
								<ul class="nav nav-tabs nav-stacked">
									<li>
										<!-- botão baixar pacote -->
										<a name="botao_download" class="botao_download" href='#' role="button"><i class="icon-download"></i>&#160;&#160;Demonstrações no modo aluno</a>
									</li>
									<li>
										<!-- botão baixar backup -->
										<a name="botao_download_backup" class="botao_download_backup" href='#' role="button"><i class="icon-download-alt"></i>&#160;&#160;Demonstrações no modo professor</a>
									</li>
									<li>
										<!-- botão baixar planilha (csv) -->
										<a name="botao_download_csv" class="botao_download_csv" href='#' role="button"><i class="icon-list-alt"></i>&#160;&#160;Formato planilha</a>
									</li>	
									<li>
										<!-- botão restaurar backup -->
										<a name="botao_restaura_backup" class="botao_restaura_backup" href='#' role="button"><i class="icon-share"></i>&#160;&#160;Restaurar Backup <sup>[beta]</sup></a>
									</li>
								</ul>
								
								<input type="hidden" name="action" value="" />
								<input type='hidden' name='tutorial' value="{demonstracao/@sistema}"/>
							</form>	
							
							<hr/>
							
						
						</div>	
						
						
					<xsl:for-each select="demonstracao/modulo">
						<div class="tabela_wrapper" id="mod{@id}">
							<!--<h3><span>Resultados para <xsl:value-of select="@nome" />:</span></h3>-->
				
						  <div class="tabela_topicos">
						
							<table caption="{demonstracao/@nome}" summary="Índice - {demonstracao/@nome}">
								<thead>
									<tr>
										<th class="titulo_coluna">Tópico</th>
										<th class="titulo_coluna">Demonstração</th>
									</tr>
								</thead>
<!-- inicio topicos -->			
								<tbody>
									<xsl:for-each select="topico">
									
<!-- inicio subtopicos -->
										<xsl:for-each select="subtopico">	
											<xsl:variable name="css-class">
												<xsl:choose>
												  <xsl:when test="@id mod 2 = 0">even</xsl:when>
												  <xsl:otherwise>odd</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<tr id="{@id}" class="{$css-class}">
												<td class="titulo_linha1 coluna_top"><xsl:value-of disable-output-escaping="yes" select="../@nome" /></td>
												<td class="coluna_sub"><a href="demo_{@id}.html" name="{@id}"><xsl:value-of disable-output-escaping="yes" select="@nome" /></a></td>
											</tr>
											<!--<tr>
												<td class="titulo_linha1">
													<xsl:value-of disable-output-escaping="yes" select="../@nome" />
												</td>
												<td>
													<a href="demo_{@id}.html" name="{@id}"><xsl:value-of disable-output-escaping="yes" select="@nome" /></a>
												</td>
											</tr>-->
										</xsl:for-each>	
<!-- fim subtopicos -->										
										
									</xsl:for-each>	
								</tbody>
<!-- fim topicos -->										
								
							</table>
							
						  </div>
						</div>
					</xsl:for-each>	
						<!--
						<div class="tabela_wrapper" id="mod{@id}">
						    <div class='apresentacao_tutorial'><xsl:value-of select="demonstracao/@apresentacao_tutorial" /></div>
							<h5>Rol de demonstrações:</h5>
							<div class="tabela_topicos">
								<table class='tabela_indice_professor' caption="{@nome}" summary="Índice - {@nome}">
									<thead>
										<tr>
											<th class="titulo_coluna">Tópico</th>
											<th class="titulo_coluna">Demonstração</th>
										</tr>
									</thead>
									<tbody>
										<xsl:for-each select="demonstracao/modulo">
											<xsl:for-each select="topico">
												<xsl:for-each select="subtopico">
													<xsl:variable name="css-class">
														<xsl:choose>
														  <xsl:when test="@id mod 2 = 0">even</xsl:when>
														  <xsl:otherwise>odd</xsl:otherwise>
														</xsl:choose>
													</xsl:variable>
												    <tr id="{@id}" class="topico_odd {$css-class}">
														<td class="coluna_top"><xsl:value-of disable-output-escaping="yes" select="../@nome" /></td>
														<td class="coluna_sub"><a href="demo_{@id}.html" name="{@id}"><xsl:value-of disable-output-escaping="yes" select="@nome" /></a></td>
													</tr>									
												</xsl:for-each>
											</xsl:for-each>				 				
										</xsl:for-each>									
									</tbody>									
								</table>
							</div>
						</div>	-->
					</div>
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>

