<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" indent="yes" />
	
	<xsl:template match="/">
		
		<html>
			<head>
			
			
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<meta name="description" content="{demonstracao/@sistema}"/>
				<meta name="name" content="Demonstração"/>
				
				<title><xsl:value-of select="demonstracao/@rotulo_sistema" /></title>
				<link rel="shortcut icon" href="../imagens/tema/favicon.ico" />
				
				
				
				<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'/>
				
				<style>
					body { font-family: 'Open Sans', sans-serif; }
					.texto_hit.informacao .texto_instrucao { display:none; }
					.texto_hit .texto_instrucao { color:#DF8200; }
					.hit_etapa { color:#999; }
				</style>

				
			</head>
									
			<body id="pagina_demonstracao">

				<h1>Tutorial: <xsl:value-of select="demonstracao/@rotulo_sistema" /></h1>

<!-- inicio modulos -->
	<xsl:for-each select="demonstracao/modulo">
		
<!-- inicio topicos -->	
			<xsl:for-each select="topico">
				
<!-- inicio subtopicos -->
				  <xsl:for-each select="subtopico">
					<h2>Módulo: <xsl:value-of select="../../@nome" /></h2>
					<h3>Tópico: <xsl:value-of select="../@ordem" /> - <xsl:value-of select="../@nome" /></h3>
					<h4>Demonstração: <xsl:value-of select="@ordem" /> - <xsl:value-of select="@nome" /></h4>

<!-- inicio nav subtopico e descricao -->
						<div class="descricao_acao">			
							<xsl:if test="apresentacao!=''">
								<div class="apresentacao"><xsl:value-of disable-output-escaping="yes" select="apresentacao" /></div>
							</xsl:if>	
						</div>
<!-- fim nav subtopico e descricao -->							

					  
<!-- INÍCIO DA DEMONSTRAÇÃO -->
					  <div class="demonstracao">
					  
<!-- inicio passos -->	
					  <xsl:for-each select="passo">
						<div class="passo {@tipo}" id="passo{@id}" >
	
						  <xsl:choose>
<!-- LAYOUT INFORMACAO -->
							<xsl:when test="@tipo='informacao'">
<!-- Modelo -->
								<div class="modelo">
									<div class="area_interacao">
<!-- Orientação -->					
										<div class="orientacao">
											<p class="xml_texto_principal">
												<xsl:value-of disable-output-escaping="yes" select="hit_auxilio/hit/texto_principal"/>
											</p>
											<input type="button" class="botao_avanca_passo" value="Seguir"/>
										</div>
										<strong>Imagem:</strong> <span class="url_img">../imagens/<xsl:value-of select="print_tela" /></span>
									</div>	
								</div>

							</xsl:when>
							
<!-- LAYOUT DEMONSTRAÇÃO -->
						    <xsl:when test="@tipo='demonstracao'"> 
<!-- início interação -->
								<xsl:for-each select="interacao">
														
								  <!--<div id="passo{../@id}_interacao{@ordem}" class="interacao" name="{@rotulo}">-->
								  <div id="interacao{@id}" class="interacao">
<!-- print_tela -->
									<img src="../imagens/{print_tela}" class="print_tela" alt="" style=""/>
<!-- destaque -->
									<div id="" class="hit {tipo_destaque}" style="position: absolute; width: {coordenada/destaque_largura}px; height: {coordenada/destaque_altura}px; display: block; top: {coordenada/destaque_y}px; left: {coordenada/destaque_x}px; opacity: 1;"/>
<!-- texto: informação + instrução -->									
									<div id="passo{../@id}_texto{@id}" class="texto_hit {tipo_destaque}" style="top: {coordenada/texto_y}px; left: {coordenada/texto_x}px; cursor: auto; display: block;">
										<div class="hit_etapa">
											(Etapa <xsl:value-of select="position()" /> de <xsl:value-of select="count(../..//interacao)"/>)
										</div>
										<div class="caixa_texto_hit"><xsl:if test="informacao!='' and informacao!='*'"><span class="texto_informacao"><xsl:value-of disable-output-escaping="yes" select="informacao"/></span><xsl:text> </xsl:text></xsl:if><xsl:if test="instrucao!='' and instrucao!='*' and tipo_destaque!='mascara'"><span class="texto_instrucao"><xsl:value-of disable-output-escaping="yes" select="instrucao"/></span></xsl:if></div>
									</div>
									<p></p>	<p></p>
								  </div>					
								</xsl:for-each>
<!-- fim interação -->
							</xsl:when>	
						  							
						  </xsl:choose>						  
						
						</div>
					  </xsl:for-each>
					  <hr/>
<!-- fim passos -->
					  </div>
<!-- FIM DA DEMONSTRAÇÃO -->					  
					  
					  
				  </xsl:for-each>	
<!-- fim subtopicos -->	
				
				
			</xsl:for-each>	
<!-- fim topicos -->
		
	</xsl:for-each>	
<!-- fim modulos -->	


<!-- plugin tooltip -->
				<div style="" id="tiptip_holder">
					<div id="tiptip_arrow">
						<div id="tiptip_arrow_inner"/>
					</div>
					<div id="tiptip_content"/>
				</div>
				
									
			</body>
		</html>
	
							
	</xsl:template>
						
</xsl:stylesheet>

