$(document).ready(function(){
	$("#tabela_catalogo").dataTable({
		"iDisplayLength": 20
	});
	
	
	//inicializa fancybox
	$('.fancybox').fancybox();
	
	
//{{{ // CONTROLES PARA EDIÇÃO DO TUTORIAL

	//clique no botão criar novo tutorial
	/*$("#novo_tutorial").click(function() {
		$.fancybox.open($('#container_criar_novo_tutorial'),{
				padding : 20
		});
		$('#nome_sistema').focus();
	});*/
	//clique no link "Atualizar XML" - exibe o formulário correspondente	
	$(document).on('click','.link_atualizar_xml',function(){
		$(this).parent().parent().hide("drop", { direction: "left" }, 200, function(){
			$(this).siblings('.container_atualizar_xml').show("drop", { direction: "right" }, 500);
		});
	});
	
	
//}}}

//{{{ // BOOTSTRAP
	
	//define foco no campo de nome do novo tutorial ao exibir o formulário para criar novo tutorial
	$('#container_criar_novo_tutorial').on('shown', function () {
		$('#nome_sistema').focus();
	});
	
	//tootip que mostra as infos do tutorial ao passar o mouse no link para o modo professor (no índice de tutoriais)
	if($(window).width() > 1220) {
		//se couber na página, exibe infos à esquerda do link para o modo professor
		$('.link_tutorial').popover({placement:'left', trigger:'hover', html:'true'});
	}
	else {
		//se não, exibe acima 
		$('.link_tutorial').popover({placement:'top', trigger:'hover', html:'true'});
	}
//}}}
	
	
//{{{ // TRATA CARREGAMENTO DA PÁGINA SE UM NOVO TUTORIAL FOI CRIADO
	
	// lê parâmetro passado via querystring - url da barra de endereços
	var querystring = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&').toString();

	// se novo tutorial foi criado, exibe as opções para edição deste tutorial
	if(querystring.indexOf("nome_tutorial") >= 0){ 
		//simula clique no link do tutorial que acabou de ser criado para exibir suas opções
		/*var tutorial_criado = $('.fancybox[href=#'+querystring.split("=")[1].replace('#', '')+']');
		tutorial_criado.click();*/
		$('#'+querystring.split("=")[1].replace('#', '')).modal('show');
	}
//}}}	




});		


function retirar_acentos($string){
    $string = ereg_replace("[^a-zA-Z0-9_.]", "", strtr($string, "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ", "aaaaeeiooouucAAAAEEIOOOUUC_"));
    return $string;
}	