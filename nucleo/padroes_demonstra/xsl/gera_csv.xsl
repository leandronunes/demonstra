<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>	
    <xsl:template match="/">
      
      
        <xsl:text>rotulo_interacao</xsl:text>
            <xsl:text>&#x9;</xsl:text>
        <xsl:text>transicao</xsl:text>
            <xsl:text>&#x9;</xsl:text>
        <xsl:text>tipo_passo</xsl:text>
            <xsl:text>&#x9;</xsl:text>
        <xsl:text>tipo_destaque</xsl:text>
            <xsl:text>&#x9;</xsl:text>
        <xsl:text>coordenada</xsl:text>
            <xsl:text>&#x9;</xsl:text>
       <xsl:text>modulo</xsl:text>
            <xsl:text>&#x9;</xsl:text>
       <xsl:text>topico</xsl:text>
            <xsl:text>&#x9;</xsl:text>
      <xsl:text>subtopico</xsl:text>
            <xsl:text>&#x9;</xsl:text>
       <xsl:text>print_tela</xsl:text>
            <xsl:text>&#x9;</xsl:text>
       <xsl:text>informacao</xsl:text>
            <xsl:text>&#x9;</xsl:text>
       <xsl:text>instrucao</xsl:text>
            <xsl:text>&#x9;</xsl:text>
       <xsl:text>area_destaque</xsl:text>
            <xsl:text>&#x9;</xsl:text>
       <xsl:text>apres_funcao</xsl:text>
            <xsl:text>&#x9;</xsl:text>
       <xsl:text>apres_import</xsl:text>
    
    
   
        <xsl:for-each select="demonstracao/interacao">
           
              <xsl:text>&#x9;</xsl:text>
            <xsl:text>&#10;</xsl:text>
            <xsl:value-of select="rotulo_interacao"/>
            <xsl:text>&#x9;</xsl:text>
             <xsl:value-of select="transicao"/>
            <xsl:text>&#x9;</xsl:text>
             <xsl:value-of select="tipo_passo"/>
            <xsl:text>&#x9;</xsl:text>
            <xsl:value-of select="tipo_destaque"/>
            <xsl:text>&#x9;</xsl:text>
            <xsl:value-of select="coordenada"/>
            <xsl:text>&#x9;</xsl:text>
            <xsl:value-of disable-output-escaping="yes" select="modulo"/>
            <xsl:text>&#x9;</xsl:text>
            <xsl:value-of disable-output-escaping="yes" select="topico"/>
            <xsl:text>&#x9;</xsl:text>
            <xsl:value-of disable-output-escaping="yes" select="subtopico"/>
            <xsl:text>&#x9;</xsl:text>
            <xsl:value-of disable-output-escaping="yes" select="print_tela"/>
            <xsl:text>&#x9;</xsl:text>
            <xsl:value-of disable-output-escaping="yes" select="informacao"/>
            <xsl:text>&#x9;</xsl:text>
            <xsl:value-of disable-output-escaping="yes" select="instrucao"/>
            <xsl:text>&#x9;</xsl:text>
            <xsl:value-of select="area_destaque"/>               
            <xsl:text>&#x9;</xsl:text>
            <xsl:value-of disable-output-escaping="yes" select="apres_funcao"/>
             <xsl:text>&#x9;</xsl:text>
            <xsl:value-of disable-output-escaping="yes" select="apres_import"/>
          
           
            
	
        </xsl:for-each>
							
	
    </xsl:template>

</xsl:stylesheet>

