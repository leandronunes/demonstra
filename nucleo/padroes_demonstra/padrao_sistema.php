<?php

	//obtem o nome da função que foi passada para o campo hidden //DISTRIBUIDOR DE FUNÇÕES
	#$funcao = $_REQUEST["action"];
	//foi usado REQUEST porque dependendo do que você for fazer você pode querer enviar via get o nome da função dai ela sera pega do mesmo jeito, porque REQUEST recebe dados via GET, POST, e COOKIE

	//verifica se a função existe	
	#if (function_exists($funcao)) {
	#	//chama uma função dada pelo primeiro parâmetro		
	#	call_user_func($funcao);
	#}
?>

<?php
//Insere CDATA pelo simplexml - extraído de: http://stackoverflow.com/questions/6260224/how-to-write-cdata-using-simplexmlelement

class SimpleXMLExtended extends SimpleXMLElement {
  public function addCData($cdata_text) {
    $node = dom_import_simplexml($this); 
    $no   = $node->ownerDocument; 
    $node->appendChild($no->createCDATASection($cdata_text)); 
    } 
  }


?>

<?php
//remove caracteres especiais, usado para tratar nome dos arquivos de imagem
function retirar_acentos($string){
	
	$a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ «»';
    $b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr___';
    $string = utf8_decode($string);    
    $string = strtr($string, utf8_decode($a), $b);
    $string = strtolower($string);
    
    return $string;
   }	
  ?>   
<?php

///////////////////////////////GERA ARQUIVOS/////////////////////////////

//função que cria/atualiza htmls e livro do moodle
function gera_arquivos($texto_xml, $xml_hierarquizado, $sistema, $dir_raiz) {
 //echo "jj";
    $cabecalho = busca_metainfo($sistema, $dir_raiz);
     
    
    //echo "<textarea>X:".$texto_xml."</textarea>";
    //echo "<textarea>H:".$xml_hierarquizado."</textarea>";
	//echo "rr<textarea>".$sistema."</textarea>";
    ////////// INDEX - trata conteúdo e grava arquivos //////////

    
    //echo "jj";
    
	//faz as transformações para o index (gera HTML de acordo com xsl Index)
	//echo "para transformar";
	$xml_index_transformado = transforma($xml_hierarquizado, $dir_raiz."DEMOS/".$sistema."/xsl/gera_index.xsl");
	//faz as transformações para o index (gera HTML de acordo com xsl Index professor)
	$xml_index_transformado_professor = transforma($xml_hierarquizado, $dir_raiz."DEMOS/".$sistema."/xsl/gera_index_professor.xsl");
	//echo "transformados";
	//escreve arquivos
	$dir = $dir_raiz.'DEMOS/'.$sistema.'/professor/index.html';
	//echo "-->".$dir; 
	//aluno
	escreve($dir_raiz.'DEMOS/'.$sistema.'/demo/html/index.html', $xml_index_transformado);
	//professor
	escreve($dir_raiz.'DEMOS/'.$sistema.'/professor/index.html', $xml_index_transformado_professor);
	
	//escreve txt no formato interações
	$texto_interacoes = seleciona_txt_interacoes($texto_xml);
	//echo "<textarea>escreve txt:".$texto_xml."</textarea>";
	escreve($dir_raiz.'DEMOS/'.$sistema.'/professor/xml_completo.txt', $texto_interacoes);	
	
	//gera html com o texto de todas as demonstrações (somente se o arquivo gera_demos_texto.xsl existir)
	if(file_exists($dir_raiz."DEMOS/".$sistema."/xsl/gera_demos_texto.xsl")){
		$xml_demos_texto_transformado = transforma($xml_hierarquizado, $dir_raiz."DEMOS/".$sistema."/xsl/gera_demos_texto.xsl");
		escreve($dir_raiz.'DEMOS/'.$sistema.'/professor/demos_texto.html', $xml_demos_texto_transformado);
	}

	////////// DEMOS - INÍCIO //////////
	
	//faz uma array com um xml independente para cada subtópico

	libxml_use_internal_errors(true);
    $sxe = simplexml_load_string($texto_xml);
    if (!$sxe) {
        echo "Failed falha1 loading XML\n";
        foreach(libxml_get_errors() as $error) {
           echo "\t", $error->message;
        }
    }
    else 
    {       
      //echo "<textarea>H:".$texto_xml."</textarea>";
      //echo "<textarea>as:".$xml->asXML()."</textarea>";	  
	  //echo "<textarea>H:".$texto_xml."</textarea>";
	  $xml = new SimpleXMLElement($texto_xml); 
	  foreach ($xml->xpath('//interacao') as $character) {
		  
	      $subtopico = intval($character->Id_subtopico);
	      //echo $subtopico; 	
        //echo $conj_interacoes;
          $split_modulos[$subtopico] .= $character->asXML();
      }
	  //print_r($split_modulos);
    }   
	//trata e escreve demos, agrupadas pelo "rótulo_topico"
    $key=1;  
	foreach ($split_modulos as $interacoesxml) {
	    
		if ($interacoesxml != "") {			
		    //echo $interacoes;
		    //monta xml 
		    $conteudo_xml_pagina = "<?xml version='1.0' encoding='UTF-8'?>".$cabecalho.$interacoesxml."</demonstracao>";
		    
			
			$xml_topico_transformado = transforma($conteudo_xml_pagina, $dir_raiz.'DEMOS/'.$sistema.'/xsl/gera_xml_hierarquizado.xsl');
		 //echo "<textarea>X:".$xml_topico_transformado."</textarea>";
		
		//muda estrutura da tag <coordenada> do xml, separando cada valor da coordenada em uma tag diferente
			$pattern = '/<coordenada>(.|\r\n)*?<\/coordenada>/i';
			$xml_novo = preg_match_all($pattern, $xml_topico_transformado, $matches);
			foreach ($matches[0] as $coordenada){
				//remove tag <coordenada> do xml
				$coordenada_antiga = str_replace('<coordenada>', '', $coordenada);
				$coordenada_antiga = str_replace('</coordenada>', '', $coordenada_antiga);
				//separa valores da coordenada em um array e distribui cada valor nas novas tags do xml
				$coordenada_split = explode("_", $coordenada_antiga);
				$coordenada_nova = "<destaque_x>".$coordenada_split[0]."</destaque_x><destaque_y>".$coordenada_split[1]."</destaque_y><destaque_largura>".$coordenada_split[2]."</destaque_largura><destaque_altura>".$coordenada_split[3]."</destaque_altura><texto_x>".$coordenada_split[4]."</texto_x><texto_y>".$coordenada_split[5]."</texto_y><seta_x>".$coordenada_split[6]."</seta_x><seta_y>".$coordenada_split[7]."</seta_y>";		
				$xml_topico_transformado = str_replace($coordenada, '<coordenada>'.$coordenada_nova.'</coordenada>', $xml_topico_transformado);
			}			
			
			
	
			//faz as transformações para as demonstrações (gera o código HTML de acordo com xsl demo)
		
			//aluno
			$xml_demo_transformado = transforma($xml_topico_transformado, $dir_raiz."DEMOS/".$sistema."/xsl/gera_demo.xsl");
				
			//faz as transformações para as demonstrações (gera o código HTML de acordo com xsl demo professor)
			//professor
			//echo "-->".$sistema;
			$xml_demo_professor_transformado = transforma($xml_topico_transformado, $dir_raiz."DEMOS/".$sistema."/xsl/gera_demo_professor.xsl");
			
			//insere XML base para edição/modo professor
			$xml_demo_professor_transformado = str_replace('<div class="arquivo_xml"><textarea class="retorna_xml"></textarea></div>', '<div class="arquivo_xml"><textarea class="retorna_xml">'.$val.'</textarea></div>', $xml_demo_professor_transformado);
				//echo "tt".$key;
			//escreve arquivos
			//aluno    
			escreve($dir_raiz."DEMOS/".$sistema."/demo/html/demo_".$key.".html", $xml_demo_transformado);
			//professor
			escreve($dir_raiz."DEMOS/".$sistema."/professor/demo_".$key.".html", $xml_demo_professor_transformado);
		
			$key++;
		}
		
	}
}
?>


<?php 


/////////////GRAVA IMAGENS/////////////////


function grava_imagens($sistema, $imagens_zip, $dir_raiz){	//retorna array com nome das imagens gravadas
	//echo "aa>".$sistema;
	//endereço do diretório de destino das imagens 
	$diretorio = $dir_raiz."DEMOS/".$sistema."/demo/imagens/";
	//echo "==>".$diretorio;
	//verifica se é zip
	$message = "";
	
	if($imagens_zip["name"]) {
		$filename = $imagens_zip["name"];
		//echo "-->".$filename;
		$source = $imagens_zip["tmp_name"];
		$type = $imagens_zip["type"];
		$name = end(explode(".", $filename));
		$accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
		foreach($accepted_types as $mime_type) {
			if($mime_type == $type) {
				$okay = true;
				break;
			} 
		}
	}
	$continue = strtolower($name) == 'zip' ? true : false;
	if(!$continue) {
		$message = "O arquivo ".$filename." enviado não é um zip válido. Por favor, tente novamente.";
	}
	else {
		
	    
	    //cria diretorio temporario e entra
		mkdir($diretorio.'/mydir_temp', 0777);
	    
		//move zip ou copia zip se estiver substituindo demo
		if(move_uploaded_file($source, $diretorio.'/mydir_temp/'.$filename) || copy($source, $diretorio.'/mydir_temp/'.$filename)) 
		{		
			//guarda o caminho diretorio atual e 
			$diretorio_home = getcwd();
		
			
			
			chdir($diretorio."/mydir_temp/");
			
			//descompacta arquivos. o modificador -l traz uma lista dos arquivos para o output. mas nao consegui fazer funcionar. por isso repeti o comando dentro do if. 	
			
			if ($teste = exec("unzip $filename"))
			{		
			    
				//entrar na pasta, abre o diretório
				$ponteiro  = opendir(getcwd());
				// monta os vetores com os itens encontrados na pasta
				while(false !== ( $file = readdir($ponteiro)) ) {
					//verifica extensão do arquivo
					$tipo_arquivo = end(explode( ".",$file));
					if (($tipo_arquivo == 'png' ) || ($tipo_arquivo == 'jpg' ) || ($tipo_arquivo == 'jpeg' )) {
						$deszipados[]=$file;
						
					}
					sort($deszipados);
				}
				foreach($deszipados as $fila){
					//limpar nome dos arquivos
					$file2 = retirar_acentos($fila); 
						//echo "<br>".$fila."-".$file2;
					//renomear	
					rename($fila,"../".$file2);
					//guardar lista na array				
					$lista_arquivos[]=$file2;
					
					//copia para pasta geral
					//echo "--".$diretorio.$file2;
					//copy($file2, $diretorio.$file2);
					
				}
				closedir($ponteiro);
				$message = "O arquivo ".$filename." foi enviado e descompactado com sucesso.";	
			}
			else{
				$message = "Houve um problema na descompactação. Por favor, tente novamente.";
			}
		}
		else {	
				$message = "Houve um problema com o envio. Por favor, tente novamente.";
			}					
		}
		//echo "<br/>Mensagem: ".$message;
		//apaga arquivo zip enviado
		unlink($imagens_zip["name"]);
		//volta para o dir original
		chdir($diretorio_home);
		//apaga o diretório temporário
		rmdir($diretorio."/mydir_temp/");
		//array com nome de arquivos
			//print_r($lista_arquivos);
		return $lista_arquivos;
}
?>


<?php 


////////////////TRANSFORMA//////////////////////


//função que lê o arquivo
function le($nome_arquivo) {
    //echo "000000000".$nome_arquivo."0000000";
    $conteudo_arquivo = file_get_contents($nome_arquivo);
  return $conteudo_arquivo;
} 

//função que escreve o arquivo
function escreve($nome_arquivo, $conteudo) {
	//echo $nome_arquivo;
	//echo "<textarea cols=50 rows=30>".$conteudo."</textarea>";
  $fp = fopen($nome_arquivo, "w");
  $escreve = fwrite($fp, $conteudo);
  fclose($fp);
  return true;
} 

//função que faz a transformação do xml pelo xsl
function transforma($xml, $caminho_xsl) {
   $xsl = le($caminho_xsl);   
   //echo "<textarea cols=50 rows=30>".$caminho_xsl."--".$xml."</textarea>";
   $xslt = new XSLTProcessor(); 
   $xslt->importStylesheet(new  SimpleXMLElement($xsl));
   //echo "qq";
   libxml_use_internal_errors(true);
   $sxe = simplexml_load_string($xml);
   if (!$sxe) {
       echo "Failed falha2 loading XML\n";
       foreach(libxml_get_errors() as $error) {
          echo "\t", $error->message;
       }
   }
   else{
       //echo "qq";
       $teste = $xslt->transformToXml(new SimpleXMLElement($xml));
       //echo "vv";
   }
   return $teste;
}

// função que trata o conteúdo do XML, removendo asteriscos, erros comuns, etc.
function trata_xml($texto_xml) {
      
	$texto_xml = str_replace("</interacao>,<interacao>", "</interacao> <interacao>", $texto_xml);
	$texto_xml = str_replace("*.", ".", $texto_xml);
	$texto_xml = str_replace(" .", ".", $texto_xml);
	$texto_xml = str_replace("<![CDATA[*]]>", "<![CDATA[]]>", $texto_xml);
	$texto_xml = str_replace("<print_tela><![CDATA[]]></print_tela>", "<print_tela><![CDATA[ ]]></print_tela>", $texto_xml);
	$texto_xml = str_replace("<layout_hit><![CDATA[]]></layout_hit>", "<layout_hit><![CDATA[borda]]></layout_hit>", $texto_xml);
	$texto_xml = str_replace("<coordenada><![CDATA[]]></coordenada>", "<coordenada><![CDATA[1_1_100_100_123_3_-16_10]]></coordenada>", $texto_xml);
	$texto_xml = ereg_replace ( "[\n\r]" , "" , $texto_xml );
	
	//echo "<textarea cols=50 rows=30>trata:".$texto_xml."</textarea>";	  
  	
	//trata nomes de imagens usando bibliotexa simplexml
	libxml_use_internal_errors(true);
    $sxe = simplexml_load_string($texto_xml);
    if (!$sxe) {
        echo "Failed falha3 loading XML\n";
        foreach(libxml_get_errors() as $error) {
           echo "\t", $error->message;
        }
    }
    else 
    {      
	  $xml = new SimpleXMLElement($texto_xml);      
      foreach ($xml->xpath('//interacao') as $character) {
         	$novo = retirar_acentos($character->print_tela);         	
         	$character->print_tela = $novo;         	         	
        }
    }      
    $texto_xml = $xml->asXML();
    //echo "<textarea cols=50 rows=30>".$texto_xml."</textarea>";
	return $texto_xml;	
}

//seleciona xml mais recente

function busca_xml_mais_recente($sistema, $dir_raiz) {
	//echo $sistema." - ".$dir_raiz;
	$files = glob($dir_raiz."DEMOS/".$sistema."/xml/*.xml");
	if(is_array($files) && count($files) > 0)
	{
		foreach($files as $file)
		{
			//echo "<br>".filemtime($file)."<br>";
			$filetime[]=$file;
	  
		}
	}
	$filetime2=max($filetime);
	return $filetime2;
}

//encontra metainfo da demonstração
function busca_metainfo($sistema, $dir_raiz){
       
	//encontra último xml
	$recente = busca_xml_mais_recente($sistema, $dir_raiz);	
	//echo $recente;
	//le arquivo
	$conteudo = le($recente);    
	//echo "<textarea cols=50 rows=30>".$conteudo."</textarea>";
	//encontra tag<demonstração
	$pattern = '/<demonstracao(.|\r\n)*?>/i';
	preg_match_all($pattern, $conteudo, $matchesa);
	//echo "->".$matchesa[0][0]."<br>";
	
	//corrige fechamento de tag para XMLs vazios (sem nó <interacao>)
	$matchesa[0][0] = str_replace("/>", ">", $matchesa[0][0]);
	
	return $matchesa[0][0];
}
?>



<?php

//////////////////////////ACTION PROFESSOR/////////////////////////////////////



//faz todas as transformações do xml, salva xml backup e gera arquivos HTML das demonstrações
function remonta_xml($xml, $sistema, $dir_raiz) {
    
   //echo "sist".$sistema."dir raiz".$dir_raiz;
   //echo "<textarea>-->para remontar:".$xml."</textarea>";	
    
    // monta o xml corretamente, para poder manipular com o simplexml
    
	//verifica se o xml contém cabeçalho
	if (strpos($xml,'<?xml') === false) {
		//busca cabeçalho recente
		$cabecalho = busca_metainfo($sistema, $dir_raiz);
		if(empty($cabecalho)){
			$cabecalho = "<demonstracao>";
		}		
		//concatena com o cabeçalho
		$xml = '<?xml version="1.0" encoding="UTF-8"?>'.$cabecalho.$xml."</demonstracao>";
	}
	
	//trata conteúdo do xml que veio do formulário- remove caracteres especiais, define defaults, etc.	
	//echo "<textarea>-->concatenado para tratar:".$xml."</textarea>";	
	$xml= trata_xml($xml);	
	//echo "<textarea>tratado:".$xml."</textarea>";
	
	//backup do xml formato interação//salva "xml no formato interação"
	$data_criacao = date("Ymd-H-i-s");
	escreve($dir_raiz.'DEMOS/'.$sistema.'/xml/'.$data_criacao.'.xml', $xml);	
	
	//echo "<textarea>para transformar:".$xml."</textarea>";	
	
	//passa no transforma para deixar xml no formato hierarquizado
	$xml_hierarquizado = transforma($xml, $dir_raiz.'DEMOS/'.$sistema.'/xsl/gera_xml_hierarquizado.xsl');

	
	//cria arquivos das demonstrações
	//echo "<textarea>para gerar arquivo. XML:".$xml."***********Hirarq: ".$xml_hierarquizado."***********SIST:".$sistema."***********DIR raiz:".$dir_raiz."</textarea>";	
	
	gera_arquivos($xml, $xml_hierarquizado, $sistema, $dir_raiz);
	//echo "gerados";
}

//função que atualiza os <Id_interacao> após exclusão ou inclusão de uma interação
function atualiza_id_interacoes(&$item, $key) {
	$pattern = '/<Id_interacao>(.|\r\n)*?<\/Id_interacao>/i';
	preg_match_all($pattern,$item,$matches);
	$id_antigo = implode("", $matches[0]);
	$id_novo = "<Id_interacao><![CDATA[".($key+1)."]]></Id_interacao>";
	
	$item = str_replace($id_antigo, $id_novo, $item);
}

//função que substitui o conteúdo de uma tag de um item do xml
function item_xml_replace($tag, $valor, &$item_xml){
	$item_xml = preg_replace('/<'.$tag.'>(.|\r\n)*?<\/'.$tag.'>/i', '<'.$tag.'><![CDATA['.$valor.']]></'.$tag.'>', $item_xml);
}

//função que remove letras de uma string 
//(usado para encontrar maior valor numérico de <rotulo_interacao>, pois no Sisgad foram usadas letras no rótulo)
function remove_letras(&$item, $key){
	$item = preg_replace('/[a-zA-Z]/','',$item);
}

//função chamada no clique no botão salvar no modo professor. 
//Gera xml, faz as transformações e salva HTMLs das demonstrações.
function salvar() {
	//recebe valores do formulário em variáveis
	$sistema  = $_POST["sistema_form"];
    $texto_interacoes  = $_POST["xml_form"];
    $html  = $_POST["html_form"];
    $dir_raiz = "../../";	
    //echo "<textarea style='width:1000px;height:500px'>".$texto_interacoes."</textarea>";
    //echo "<script>alert('Salvando [".$sistema."] e [".$dir_raiz."] e [".$html."]');</script>";
	//salva arquivo txt com o xml
	$texto_interacoes = str_replace("</interacao>,<interacao>", "</interacao> <interacao>", $texto_interacoes);
	escreve($dir_raiz.'DEMOS/'.$sistema.'/professor/xml_completo.txt', $texto_interacoes);
	//echo "<textarea>a:".$texto_interacoes."</textarea>";
	
	
	//remonta xml e gera arquivos
	remonta_xml($texto_interacoes, $sistema, $dir_raiz);
	//redireciona($dir_raiz."DEMOS/".$sistema."/professor/".$html);
	$success = true;
	
	if ($success){
		echo "###sucesso###";
    }
    else{
        echo "###falha###";
    }  	
}

//função chamada no clique no botão 'troca_imagem' no modo professor. 
//envia nova imagem que irá substituir o print de tela da interação que o usuário estiver editando
function troca_imagem() {
	//recebe valores do formulário em variáveis
	$sistema  = $_POST["sistema_form"];
    $texto_xml  = $_POST["xml_form"];
    $html  = $_POST["html_form"];
    $dir_raiz = "../../";
	$imagem_substituta = $_FILES["imagem_substituta"];
	$id_interacao = $_POST["id_interacao"];

	
	//echo "<textarea style='width:1000px;height:500px'>->inicio".$texto_xml."</textarea>";
	
	//verifica se é imagem (png ou jpg ou gif)
	$message = "";
	if($imagem_substituta["name"]) {
		$filename = retirar_acentos($imagem_substituta["name"]); //trata o nome da imagem
		$source = $imagem_substituta["tmp_name"];
		$type = $imagem_substituta["type"];
		$name = explode(".", $filename);
		$accepted_types = array('image/jpeg', 'image/pjpeg', 'image/png', 'image/gif');
		foreach($accepted_types as $mime_type) {
			if($mime_type == $type) {
				$okay = true;
				break;
			}
		}
		if(!$okay) {
			//Se arquivo não for válido, exibe alert e retorna para página da demonstração
			$message = "O arquivo ".$filename." enviado não é uma imagem válida. Por favor, tente novamente.";
			//echo "<script>alert('".$message."');</script>";
			redireciona($dir_raiz."DEMOS/".$sistema."/professor/".$html);
		}
		else {
			//echo "<script>alert('É imagem!');</script>";
			///// Verifica se imagem já existe /////
			// pega o endereço do diretório 
			$diretorio = $dir_raiz."DEMOS/".$sistema."/demo/imagens/";
			// abre o diretório
			$ponteiro  = opendir($diretorio);
			// percorre a pasta de imagens checando se já existe
			while ($nome_imagens_existentes = readdir($ponteiro)) 
			{
				if($nome_imagens_existentes==$filename)
				{
					$repetido = true;
					break;
				}
			}
			//se já imagem existir, renomeia inserindo o timestamp (data e hora do upload) no final do nome
			if($repetido) {
				//echo "<script>alert('Já existe uma imagem com esse nome.');</script>";
				$data_upload = date("Ymd-H-i-s");
				$filename = $name[0]."_".$data_upload.".".end($name);
			}
			else {
				//echo "<script>alert('Imagem inédita.');</script>";
			}			
			
			//faz o upload (salva na pasta)
			move_uploaded_file($source, $diretorio.$filename);
			
/// OBS: transformar em função #picar		
			//extrai do xml nós <interacao> e os distribui em um array
			$pattern = '/<interacao>(.|\r\n)*?<\/interacao>/i';
			preg_match_all($pattern,$texto_xml,$matches);
			$array_xml_interacoes = $matches[0];
            /// OBS: transformar em função #picar
            //print_r($array_xml_interacoes);
			
			//identifica nó da interação que contém a imagem que será trocada
			$item_alterar_imagem = $id_interacao-1;
				
			//atualiza xml
			item_xml_replace('print_tela', $filename, $array_xml_interacoes[$item_alterar_imagem]);

			//transforma array em string
			$texto_xml = implode("", $array_xml_interacoes);
			
			//salva arquivo txt com o xml
			escreve($dir_raiz.'DEMOS/'.$sistema.'/professor/xml_completo.txt', $texto_xml);
	
			//remonta xml e gera arquivos

			//echo "<textarea style='width:1000px;height:500px'>->antes".$texto_xml."</textarea>";
			
			remonta_xml($texto_xml, $sistema, $dir_raiz);

			//redireciona para página da demonstração, no ponto que o usuário estava editando
			redireciona($dir_raiz."DEMOS/".$sistema."/professor/".$html);
	
		}
	}
	else
	{
	    echo "campo de imagem vazio";
	}
}

//função chamada no clique no botão 'download' no modo professor. 
//zipa o pacote final com todas as demonstrações da visão aluno (pastas html, imagens, js e css) e fornece o arquivo zip para o usuário
function download_pacote() {
	

	
	//recebe valores do formulário em variáveis
	$sistema  = $_POST["tutorial"];
    //pendente: deixar dinamico
	$dir_raiz = "../../";
	
	//armazena data atual em variável para inserir no nome do arquivo zip
	$data_atual = date("Ymd-H-i-s");
	
	//nome do arquivo zip (exemplo: demo_sistema.zip)
 	$nome_arquivo = "demo_".$sistema."_".$data_atual;
	
	//define caminho da pasta que será zipada e onde será criado o arquivo zip
	$path = $dir_raiz."DEMOS/".$sistema."/demo";
	$output = $dir_raiz."DEMOS/".$sistema."/demo";
	
	//escreve o zip
	file_put_contents($path, $output); 
	
	//guarda o caminho diretorio atual 
	$diretorio_home = getcwd();

	//muda o diretorio atual
	chdir($path); 

	//zipa todo o conteúdo da pasta "demo"
	exec("zip -r '$nome_arquivo' ./"); 
	exec("zip -d '$nome_arquivo' '*.zip' ./$sistema");
	$filename = "$nome_arquivo.zip";
	
	//volta para o diretório original
	chdir($diretorio_home);	
	
	//disponibiliza o zip criado para o usuário
	header('Content-Type: application/octet-stream'); 
	header('Content-Disposition: attachment; filename=' .urlencode($filename)); 
	header('Content-Transfer-Encoding: binary'); 
	readfile($dir_raiz."DEMOS/".$sistema."/demo/".$filename);
	
	//apaga o arquivo zip
	unlink($dir_raiz."DEMOS/".$sistema."/demo/".$filename);
	
	//redireciona para página da demonstração, no ponto que o usuário estava editando
	redireciona($dir_raiz."DEMOS/".$sistema."/professor/".$html);
}


//função chamada no clique no botão 'download_backup' no modo professor. 
//zipa o pacote final com todos os arquivos para backup, incluindo os xml, xsl, etc.
function download_backup() {
		
  
    
	//recebe valores do formulário em variáveis
	$sistema  = $_POST["tutorial"];
    //pendente: deixar dinamico
	$dir_raiz = "../../";
	
	 otimiza_backup($sistema,$dir_raiz);
	
	
	//armazena data atual em variável para inserir no nome do arquivo zip
	$data_atual = date("Ymd-H-i-s");
	
	//nome do arquivo zip (exemplo: demo_sistema.zip)
 	$nome_arquivo = "backup_".$sistema."_".$data_atual;
	
	//define caminho da pasta que será zipada e onde será criado o arquivo zip
	$path = $dir_raiz."DEMOS/".$sistema;
	$output = $dir_raiz."DEMOS/".$sistema;
	
	//escreve o zip
	file_put_contents($path, $output); 
	
	//guarda o caminho diretorio atual 
	$diretorio_home = getcwd();

	//muda o diretorio atual
	chdir($path); 

	//zipa todo o conteúdo da pasta "demo"
	exec("zip -r '$nome_arquivo' ./"); 
	exec("zip -d '$nome_arquivo' '*.zip' ./$sistema");
	$filename = "$nome_arquivo.zip";
	
	//volta para o diretório original
	chdir($diretorio_home);	
	
	//disponibiliza o zip criado para o usuário
	header('Content-Type: application/octet-stream'); 
	header('Content-Disposition: attachment; filename=' .urlencode($filename)); 
	header('Content-Transfer-Encoding: binary'); 
	readfile($output."/".$filename);
	
	//apaga o arquivo zip
	unlink($output."/".$filename);
	
	
}


//função chamada no clique no botão 'exportar para planilha' no modo professor. 
function download_csv() {
    
    //recebe valores do formulário em variáveis
	$sistema  = $_POST["tutorial"];
    //pendente: deixar dinamico
	$dir_raiz = "../../";
	//armazena data atual em variável para inserir no nome do arquivo
	$data_atual = date("Ymd-H-i-s");
	//nome do arquivo csv
 	$nome_arquivo = "demo_csv_".$sistema."_".$data_atual.".txt";
	//define caminho da pasta onde será criado o arquivo
	$output = $dir_raiz."DEMOS/".$sistema."/demo/";    
    
   //echo $sistema;
    
    //acha último xml
    $recente = busca_xml_mais_recente($sistema, $dir_raiz);	
	//echo $recente;
	//le arquivo
	$conteudo = le($recente);    
	


    $arquivo_csv = transforma($conteudo, $dir_raiz."nucleo/padroes_demonstra/xsl/gera_csv.xsl");

   //echo "<textarea style='width:1000px;height:500px'>".$arquivo_csv."</textarea>";

    //escreve novo arquivo
    escreve($output."/".$nome_arquivo, $arquivo_csv);   
    
    //disponibiliza para download
    //disponibiliza o zip criado para o usuário
	header('Content-Type: application/octet-stream'); 
	header('Content-Disposition: attachment; filename=' .urlencode($nome_arquivo)); 
	header('Content-Transfer-Encoding: binary'); 
	readfile($output."/".$nome_arquivo);	
	//apaga o arquivo
	unlink($dir_raiz."DEMOS/".$sistema."/demo/".$nome_arquivo);
	
}







//função chamada no clique no botão excluir interação no modo professor. 
//exclui a interação, e atualiza xml e HTMLs
function excluir_interacao() {
	//recebe valores do formulário em variáveis
	$sistema  = $_POST["sistema_form"];
    $interacoes_xml  = $_POST["xml_form"];
    $html  = $_POST["html_form"];
    $dir_raiz = "../../";
	$id_interacao = $_POST["id_interacao"];

	
    //echo "<textarea style='width:1000px;height:500px'>".$interacoes_xml."</textarea>";
    //echo "<script>alert('Salvando [".$sistema."] e [".$dir_raiz."] e [".$id_interacao."]');</script>";
	


/// OBS: transformar em função #picar		
	//extrai do xml nós <interacao> e os distribui em um array
	$pattern = '/<interacao>(.|\r\n)*?<\/interacao>/i';
	preg_match_all($pattern,$interacoes_xml,$matches);
	$array_xml_interacoes = $matches[0];
/// OBS: transformar em função #picar
	
	//identifica nó da interação que será excluída
	$item_excluir = $id_interacao-1;
	
	//exclui item(nó do xml) do array
	unset($array_xml_interacoes[$item_excluir]);
	
	//atualiza índice dos elementos do array que vinham depois do item excluído
	$array_xml_interacoes = array_values($array_xml_interacoes);

	//percorre array renumerando ids de cada interação
	array_walk($array_xml_interacoes, 'atualiza_id_interacoes');

	//transforma array em string
	$interacoes_xml = implode("", $array_xml_interacoes);

	//corrige url de retorno, retirando a interação que foi excluída
	$nome_arquivo_html = explode("?",$html);
	$html = $nome_arquivo_html[0];
	
	//salva arquivo txt com o xml
	escreve($dir_raiz.'DEMOS/'.$sistema.'/professor/xml_completo.txt', $interacoes_xml);
	
	//remonta xml e gera arquivos
	remonta_xml($interacoes_xml, $sistema, $dir_raiz);
	
	//redireciona para página da demonstração, no ponto que o usuário estava editando
    redireciona($dir_raiz."DEMOS/".$sistema."/professor/".$html);
		
}

//função chamada no clique no botão adicionar interação (antes ou depois), no modo professor. 
//adiciona a interação, e atualiza xml e HTMLs
function adicionar_interacao() {
	//recebe valores do formulário em variáveis
	$sistema  = $_POST["sistema_form"];
    $xml_form  = $_POST["xml_form"];
    $html  = $_POST["html_form"];
    $dir_raiz = "../../";
	$id_interacao = $_POST["id_interacao"];
	$posicao = $_POST["posicao"];

	
	$indice_interacao_atual = $id_interacao - 1; //porque array começa do índice zero

	//identifica nó da interação que será adicionada
	if($posicao == 'antes') {
		$indice_item_incluir = $indice_interacao_atual;
		//echo "<script>alert('antes [".$indice_interacao_atual."]');</script>";
	}
	else if($posicao == 'depois') {
		$indice_item_incluir = $indice_interacao_atual + 1;
	}
	
    //echo "<textarea style='width:1000px;height:500px'>".$xml_form."</textarea>";
    //echo "<script>alert('Salvando [".$sistema."] e [".$dir_raiz."] e [".$id_interacao."]');</script>";
	


/// OBS: transformar em função #picar		
	//extrai do xml nós <interacao> e os distribui em um array
	$pattern = '/<interacao>(.|\r\n)*?<\/interacao>/i';
	preg_match_all($pattern,$xml_form,$matches);
	$array_xml_interacoes = $matches[0];
/// OBS: transformar em função #picar
	
	//encontra valor do maior <rotulo_interacao>, para definir o valor do rótulo da interação que será adicionada
	$pattern = '/<rotulo_interacao>(.|\r\n)*?<\/rotulo_interacao>/i';
	$matches = array();
	preg_match_all($pattern, $xml_form, $matches);
	$matches[0] = str_replace("<rotulo_interacao><![CDATA[", "", $matches[0]);
	$matches[0] = str_replace("]]></rotulo_interacao>", "", $matches[0]);
	//remove todas as letras dos <rotulo_interacao>
	array_walk($matches[0], 'remove_letras');
	//encontra o valor numérico máximo
	$maior_rotulo_interacao = max($matches[0]);
	$rotulo_nova_interacao = $maior_rotulo_interacao + 1;
	
	//copia nó da interação atual (a partir da qual foi solicitada a inclusão)
	$xml_nova_interacao = $array_xml_interacoes[$indice_interacao_atual];
	
	//substitui valores de tags no xml que mudarão na nova interação
	item_xml_replace('rotulo_interacao', $rotulo_nova_interacao, $xml_nova_interacao);
	item_xml_replace('instrucao', 'Instrução - clique para editar o texto.', $xml_nova_interacao);
	item_xml_replace('informacao', 'Informação - clique para editar o texto.', $xml_nova_interacao);
	item_xml_replace('tipo_destaque', 'borda', $xml_nova_interacao);
	item_xml_replace('coordenada', '1_1_100_100_123_3_-16_10', $xml_nova_interacao);	

	//insere item(nó do xml) no array, na posição especificada pelo o usuário: antes ou depois da interação atual(a partir da qual foi solicitada a inclusão)
	array_splice($array_xml_interacoes, $indice_item_incluir, 0, $xml_nova_interacao);
		
    //percorre array renumerando ids de cada interação
	array_walk($array_xml_interacoes, 'atualiza_id_interacoes');

	//transforma array em string
	$xml_interacoes = implode("", $array_xml_interacoes);

	//corrige url de retorno, retirando a interação que foi excluída
	$nome_arquivo_html = explode("?",$html);
	$html = $nome_arquivo_html[0].'?interacao='.$rotulo_nova_interacao;
	
	//echo "<textarea style='width:1000px;height:500px'>xmlcompleto:".$xml_interacoes."</textarea>";
	
	//salva arquivo txt com o xml
	//escreve($dir_raiz.'DEMOS/'.$sistema.'/professor/xml_completo.txt', $xml_interacoes);
	
	//remonta xml e gera arquivos
	remonta_xml($xml_interacoes, $sistema, $dir_raiz);
	
	//redireciona para página da demonstração, no ponto que o usuário estava editando
   redireciona ($dir_raiz."DEMOS/".$sistema."/professor/".$html);	
}

// limpa imagens não utilizadas e seleciona os xmls chave
function otimiza_backup($sistema, $dir_raiz ){

    //IMAGENS
        
    //encontra último xml
	$recente = busca_xml_mais_recente($sistema, $dir_raiz);	
	$xml=simplexml_load_file($recente);
	//print_r($xml);
	//encontra  array com lista imagens do xml
    foreach($xml->interacao as $registro) {
        //echo "<strong>print tela:</strong> ".$registro->print_tela."<br>";
        $array_prints[] = $registro->print_tela;
    }
    //endereço do diretório das imagens 
	$diretorio = $dir_raiz."DEMOS/".$sistema."/demo/imagens/";
    //guarda o caminho diretorio atual e 
	$diretorio_home = getcwd();
	//vai para diretório das imagens do projeto	
	chdir($diretorio);
	$ponteiro  = opendir(getcwd());
	while(false !== ( $file = readdir($ponteiro)) ) {
	    //verifica extensão do arquivo
		$tipo_arquivo = end(explode( ".",$file));
		if (($tipo_arquivo == 'png' ) || ($tipo_arquivo == 'jpg' )) {
		    //compara arquivos do diretório com a lista
		    if (in_array($file, $array_prints)) {
		        $result[] = "mantido:".$file."<br>";
		    }
		    else{
                //se não está na lista, remove
                $result[] = "excluído:".$file."<br>";
				unlink($file);
            }		
	    }
	}		
	closedir($ponteiro);		
	//volta para o dir original
	chdir($diretorio_home);
	//array com nome de arquivos
	//print_r($result);
      
    //XMLS CHAVE: 
    //- último dia de edição - primeiro e último dos demais dias anteriores em que houve edição
    
     //endereço do diretório dos xml
	$diretorio = $dir_raiz."DEMOS/".$sistema."/xml/";    
    //guarda o caminho diretorio atual e 
	$diretorio_home = getcwd();
		
	chdir($diretorio);
	$ponteiro  = opendir(getcwd());
	while(false !== ( $file = readdir($ponteiro)) ) {
	    //verifica extensão do arquivo
		$tipo_arquivo = end(explode( ".",$file));
		if ($tipo_arquivo == 'xml' ) {
            //busca data de modificação dos arquivos e faz array com data e nome do arquivo		
            $array_files[date("Ymd",filemtime($file))][] = $file;	    
		}	   
	}		
	//echo "aa";
	//mantém arquivos do último dia editado
	$ultimo_dia_editado[] = max($array_files);	
	$mantem = $ultimo_dia_editado;
	//print_r($mantem);
	
	$array_temp = array_diff($array_files,$ultimo_dia_editado);
	//verifica as datas anteriores editadas
	foreach($array_temp as $dia)	{
	    //echo "aa";
       $mante[] = max($dia);
       $mante[] = min($dia);
    
    }
	print_r($mante);

     
    //varre novamente apagando os arquivos
    rewinddir($ponteiro);
    while(false !== ( $file = readdir($ponteiro)) ) {
	    //verifica extensão do arquivo
		$tipo_arquivo = end(explode( ".",$file));
		if ($tipo_arquivo == 'xml' ) {
		    $file_array = array($file);
		    if(array_diff($file_array,$mante))
		    {
		      // echo "apaga".$file."<br>";
		       unlink($file);
		       
		    }
		    else{
		        //echo "mante".$file."<br>";
		    }
	    
		}
		$file_array = "";
	}
    closedir($ponteiro);
    //volta para o dir original
	chdir($diretorio_home);

}

//substitui toda a sequencia de imagens de uma demonstração pronta, desde que com o mesmo numero de interações e imagens enviadas.

function troca_todas() {
    //recebe valores do formulário em variáveis
	$sistema  = $_POST["sistema_form"];
	$id_demonstracao = $_POST["id_demonstracao"];
    //pendente: deixar dinamico
	$dir_raiz = "../../";

	
	//abre xml
    //guarda número de interações
    $recente = busca_xml_mais_recente($sistema, $dir_raiz);	
	$xml=simplexml_load_file($recente);	
	
	//echo count($xml->xpath('//print_tela'));
	
	foreach($xml->interacao as $registro) {
        if($registro->Id_subtopico==$id_demonstracao){            
            //echo "<strong>print tela:</strong> ".$registro->print_tela."<br>";
            $array_prints[] = $registro->print_tela;            
        }
    }
	$numero_prints = count($array_prints);
	//echo $numero_prints;
	 
	//unzip em pasta temporária e conta as imagens    
    
        //certifique-se que o número de imagens enviadas é o mesmo de interações

            //se é
            
                //cria numero unico de timestamp        
                //renomeia os arquivos e copia para pasta de imagens 
                //altera no xml            
           // se não é, aviso: "na demonstração há N interações e foram enviadas Z imagens. Não há como relacioná-las. Por favor refaça o arquivo copiando ou excluindo as imagens de forma a corresponder ao número de interações."
    
     //apaga pasta temporária
}


function substituir(){
    //recebe valores do formulário em variáveis
	$sistema  = $_POST["sistema_form"];
    $texto_interacoes = $_POST["xml_form"];
    $html  = $_POST["html_form"];
    $dir_raiz = "../../";
	$termo_procurado = $_POST["inputLocalizar"];
	$termo_novo = $_POST["inputSubstituir"];
	$array_ids_para_substituir = explode(",", $_POST["array_ids_para_substituir"]);
	
	//echo $sistema."<br/>".$termo_procurado."<br/>".$termo_novo."<br/>".$_POST["array_ids_para_substituir"]."<br/><textarea>".$texto_interacoes."<textarea>";	
	//print_r($array_ids_para_substituir);
	
//obs, o $texto_xml futuramente estará mantido em um arquivo no servidor, manipulado pelo ajax, sem passar por form como é feito hj.

	//concatena cabeçalho para tratar pelo simplexml
	$cabecalho = busca_metainfo($sistema, $dir_raiz);
	$texto_xml = '<?xml version="1.0" encoding="UTF-8"?>'.$cabecalho.$texto_interacoes."</demonstracao>";
		
	libxml_use_internal_errors(true);
    $sxe = simplexml_load_string($texto_xml);
    if (!$sxe) {
        echo "Failed falha6 loading XML\n";
        foreach(libxml_get_errors() as $error) {
           echo "\t", $error->message;
        }
    }
    else 
    {

        $xml= new SimpleXMLExtended($texto_xml);
        

		//para cada id a ser substituído, localiza o nó e substitui o texto
		foreach ($array_ids_para_substituir as $id_interacao_para_alterar){

			$noh_interacao = $xml->xpath("//interacao[contains(./Id_interacao, '".$id_interacao_para_alterar."')]");

			//substitui texto <informacao>
			$velha_info = strval($noh_interacao[0]->informacao);
            $nova_info = str_replace($termo_procurado, $termo_novo, $velha_info);
            $noh_interacao[0]->informacao = NULL; // VERY IMPORTANT! We need a node where to append
            $noh_interacao[0]->informacao->addCData($nova_info);       
        
			
			//substitui texto <instrucao>
			$velha_info = strval($noh_interacao[0]->instrucao);
            $nova_info = str_replace($termo_procurado, $termo_novo, $velha_info);
            $noh_interacao[0]->instrucao = NULL; // VERY IMPORTANT! We need a node where to append
            $noh_interacao[0]->instrucao->addCData($nova_info);   
  
			
			//substitui texto <apres_funcao>
			$velha_info = strval($noh_interacao[0]->apres_funcao);
            $nova_info = str_replace($termo_procurado, $termo_novo, $velha_info);
            $noh_interacao[0]->apres_funcao = NULL; // VERY IMPORTANT! We need a node where to append
            $noh_interacao[0]->apres_funcao->addCData($nova_info);   
         
			
			//substitui texto <apres_import>
			$velha_info = strval($noh_interacao[0]->apres_import);
            $nova_info = str_replace($termo_procurado, $termo_novo, $velha_info);
            $noh_interacao[0]->apres_import = NULL; // VERY IMPORTANT! We need a node where to append
            $noh_interacao[0]->apres_import->addCData($nova_info);   
          
            //echo "+++".$velha_info."<br>";
			//echo "*****ANTES: ".$noh_interacao[0]->informacao."<br>";            
		}
		//$xml->saveXML($texto_xml); 
		//echo "<textarea style='width:1000px;height:500px'>substituir:".$xml_out."</textarea>";   
    
        //manda remontar os arquivos com o novo xml
		remonta_xml($xml->asXML(), $sistema, $dir_raiz);		
		//redireciona para página da demonstração, no ponto que o usuário estava editando
		redireciona ($dir_raiz."DEMOS/".$sistema."/professor/".$html);	
	}	
}


// localiza todas as ocorrências do texto enviado via formulario
function localizar(){
	//recebe valores do formulário em variáveis
	$sistema  = $_POST["sistema_form"];
    $texto_interacoes = $_POST["xml_form"];
    $html  = $_POST["html_form"];
    $dir_raiz = "../../";
	$termo = $_POST["inputLocalizar"];
	$termo_novo = $_POST["inputSubstituir"];
	
	
	if($_POST["expressao_regular"]=='on'){
	    $tipo_busca="matches";	
	}
	else{
	    $tipo_busca="contains";
	}
	
	//concatena cabeçalho para tratar pelo simplexml
	$cabecalho = busca_metainfo($sistema, $dir_raiz);
	$texto_xml = '<?xml version="1.0" encoding="UTF-8"?>'.$cabecalho.$texto_interacoes."</demonstracao>";
		
	libxml_use_internal_errors(true);
    $sxe = simplexml_load_string($texto_xml);
    if (!$sxe) {
        echo "Failed falha6 loading XML\n";
        foreach(libxml_get_errors() as $error) {
           echo "\t", $error->message;
        }
    }
    else 
    {
      //para cada interação, verifica se há o texto enviado e retorna uma lista cujo valor do id é o número do Id da interação.
	  $xml = new SimpleXMLElement($texto_xml);
	  $numero_tela = 0;
	  $subtopico = 0; 
      foreach ($xml->xpath('//interacao['.$tipo_busca.'(./informacao, "'.$termo.'") or 
							            '.$tipo_busca.'(./instrucao, "'.$termo.'") or
							            '.$tipo_busca.'(./apres_funcao, "'.$termo.'") or
							           '.$tipo_busca.'(./apres_import, "'.$termo.'")]') as $character)
	  {
            //acumula itens do array que armazena os IDs de quais interações possuem o termo procurado
            $array_ids_para_substituir[] = $character->Id_interacao;		
            
            //para referência na interface dos resultados, obtém o número da etapa de cada resultado na demonstração (nem Id nem rótulo. é a referência da etapa para o conteudista)://<Id_interacao> da interação MENOS <Id_interacao> da primeira interação da demonstração MAIS 1
             $subtopico = intval($character->Id_subtopico);
             $primeira_interacao = $xml->xpath("//interacao[contains(./Id_subtopico, '".$subtopico."')]");
             $numero_tela = intval($character->Id_interacao) - intval($primeira_interacao[0]->Id_interacao) + 1;

             //Testa se é a primeira interação do subtopico. 
            if ($resultado[$subtopico] == "") {
                //Testa se é o primeiro subtópico
                if($subtopico > 1){ 
                    //se não for, fecha rodapé divs do subtopico anterior
                    $resultado[$subtopico-1] .= '</div></div></div>';                   
                }
                //sendo a primeira interação do subtópico, cria o bloco de html de cabeçalho
                $resultado[$subtopico] = '<div class="accordion-group"><div class="accordion-heading"><a class="resultado_titulo_demo accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#'.$subtopico.'">'.$character->modulo.'&nbsp;&nbsp;→&nbsp;&nbsp;'.$character->topico.'&nbsp;&nbsp;→&nbsp;&nbsp;'.$character->subtopico.'</a></div><div id="'.$subtopico.'" class="accordion-body collapse in"><div class="accordion-inner">';
                //se o termo está presente na apresentação, concatena no cabeçalho 
                if(stristr($character->apres_funcao,$termo)){
                    $html_apres_funcao = str_replace(htmlspecialchars($termo),'<span class="termo_localizar">'.htmlspecialchars($termo).'</span>', htmlspecialchars($character->apres_funcao));
                    $resultado[$subtopico] .= '<div class="resultado_apresentacao"><strong>Apresentação:</strong> '.$html_apres_funcao.'</div>';
                }
                 if(stristr($character->apres_import,$termo)){
                    $html_apres_import = str_replace(htmlspecialchars($termo),'<span class="termo_localizar">'.htmlspecialchars($termo).'</span>', htmlspecialchars($character->apres_import));
                    $resultado[$subtopico] .= '<div class="resultado_apresentacao_importante"><strong>Importante:</strong> '.$html_apres_import.'</div>';
                }
            }
            //Início do bloco de conteúdo
               
            if(stristr($character->informacao,$termo)==true|| stristr($character->instrucao,$termo)==true){
                    
                
                    //insere destaque no termo procurado, dentro do conteúdo dos nós <informacao> e <instrucao>
                    $html_informacao = str_replace(htmlspecialchars($termo),'<span class="termo_localizar">'.htmlspecialchars($termo).'</span>', htmlspecialchars($character->informacao));
                    $html_instrucao = str_replace(htmlspecialchars($termo),'<span class="termo_localizar">'.htmlspecialchars($termo).'</span>', htmlspecialchars($character->instrucao));
                    //MONTA cada registro numa string html que será concatenada num html que será exibido na página para o usuário
                    $html_retorno_interacao = '<p><a href="demo_'.$character->Id_subtopico.'.html?interacao='.$character->rotulo_interacao.'">Tela '.$numero_tela.'</a>: '.$html_informacao.' - '.$html_instrucao.'</p><hr/>';   
                    //reúne strings do subtópico para serem concatenadas em um único html
                    $resultado[$subtopico] .= $html_retorno_interacao;      
              
            }      
           
         }     
          //se encontrar termo procurado retorna ou resultado e div com a lista de IDs das interações que possuem o termo pesquisado
          if(count($array_ids_para_substituir) > 0){
            echo "<div class='accordion' id='html_retorno_localizar'>".implode("", $resultado)."</div>";
            echo "<div id='ids_substituir'>".implode(",", $array_ids_para_substituir)."</div>";
          }
          //se não encontrar, retorna mensagem informando ao usuário e div com a lista de IDs vazio
          else {
            echo "Nenhuma ocorrência de <span class='termo_localizar'>".htmlspecialchars($termo)."</span> encontrada. Por favor, tente novamente.";
            echo "<div id='ids_substituir'></div>";
          }
          
    }  

}



function restaura_backup(){

    //recebe valores do formulário em variáveis
	$sistema  = $_POST["tutorial"];
    //pendente: deixar dinamico
	$dir_raiz = "../../";
    
    
    //unzip do backup
    
    //seleciona xml, imagens e css, xsl e js específicos
           
        //verifica versão do xml        
        //se não for compatível
            //passa xml pelo xsl de migração. 
                //troca nomes das tags
                //complementa com dados padrão 
                //solicita informações adicionais se necessário
                //cria xml migrado
    //verifica se xsl do aluno tem personalização, com relação à versão mais recente do matricial
        //se tem, mantem o personalizado 
        //verifica versão do xsl e compatibilidade com a versão do xml
    //copia matricial e sobrescreve arquivos
    //envia novamente o xml para criar novamente os arquivos
}

function seleciona_xml_interacoes($texto_xml){
	//cria xml com as interações independentes do cabeçalho
	//echo "<textarea style='width:1000px;height:500px'>seleciona(com cabecalho):".$texto_xml."</textarea>";
	libxml_use_internal_errors(true);
    $sxe = simplexml_load_string($texto_xml);
    if (!$sxe) {
        echo "Failed falha4 loading XML\n";
        foreach(libxml_get_errors() as $error) {
           echo "\t", $error->message;
        }
        
    }
    else 
    {      
	  $xml = new SimpleXMLElement($texto_xml);
      foreach ($xml->xpath('//interacao') as $character) {              
                $conj_interacoes .= $character->asXML();
      } 
    }  
    $t_xml = new DOMDocument();
    $t_xml->loadXML($conj_interacoes);
    $xml_out = $t_xml->saveXML($t_xml->documentElement);
   
    //echo "<textarea style='width:1000px;height:500px'>-->na função independente".$xml_out."</textarea>"; 
    return $xml_out;
}

function seleciona_txt_interacoes($texto_xml){
	//cria texto com as interações independentes do cabeçalho
	//echo "<textarea style='width:1000px;height:500px'>seleciona(com cabecalho):".$texto_xml."</textarea>";
	libxml_use_internal_errors(true);
    $sxe = simplexml_load_string($texto_xml);
    if (!$sxe) {
        echo "Failed falha5 loading XML\n";
        foreach(libxml_get_errors() as $error) {
           echo "\t", $error->message;
        }
        
    }
    else 
    {      
	  $xml = new SimpleXMLElement($texto_xml);
      foreach ($xml->xpath('//interacao') as $character) {              
                $conj_interacoes .= $character->asXML();
      } 
    }  
   
    //echo "<textarea style='width:1000px;height:500px'>-->na função independente".$xml_out."</textarea>"; 
    return $conj_interacoes;
}


function simular_modoaluno($sistema, $url, $dir_raiz){
     //pendente: deixar dinamico
	$dir_raiz = "../../";
	
	//echo $_POST["html_form"];
    redireciona($dir_raiz."nucleo/modo_aluno.php?sistema=".$_POST["sistema_form"]. "&url=".$_POST["html_form"]."");

}


function redimensiona_coordenadas($sistema, $dir_raiz){
    
    //recebe valores do formulário 
   // $sistema  = $_POST["tutorial_existente"];
   // $dir_raiz = "../../";    
   
   //ALTERA CSS
    
    $arquivos = array($dir_raiz."DEMOS/".$sistema."/demo/css/".$sistema."_aluno.css", $dir_raiz."DEMOS/".$sistema."/professor/css/".$sistema."_professor.css");
    //lê arquivo
    foreach ($arquivos as $arquivo){
        $conteudo_css = le($arquivo);
        //echo "<textarea>velho css:".$conteudo_css."</textarea>";
        
        //identifica largura antiga
        $pattern = '/(?<=\/\*controlado por script - nao alterar\*\/(max-width:))[0-9]*/i';
        $trecho = preg_match_all($pattern, $conteudo_css, $matches);
        foreach ($matches[0] as $largura_antiga){
           //echo "lan".$largura_antiga;	   
        }
        
        //calcula proporção de redimensionamento
        $proporcao = $_POST['largura_nova']/$largura_antiga;    
        
          
        //localiza os trechos de css por expressão regular
        $pattern = '/\/\*controlado por script - nao alterar\*\/(width|max-width):[0-9]*px;/i';
        $xml_novo = preg_match_all($pattern, $conteudo_css, $matches);
        foreach ($matches[0] as $trecho){
           //echo "<br>trecho".$trecho;
                
            $conteudo_css = str_replace("/*controlado por script - nao alterar*/max-width:".$largura_antiga, "/*controlado por script - nao alterar*/max-width:".$_POST['largura_nova'], $conteudo_css);
            $conteudo_css = str_replace("/*controlado por script - nao alterar*/width:".$largura_antiga, "/*controlado por script - nao alterar*/width:".$_POST['largura_nova'], $conteudo_css);
            
        
        }
        escreve( $arquivo, $conteudo_css);
        //echo "<textarea>novo css:".$conteudo_css."</textarea>";
        //escreve      
    }
   
   
    //ALTERA XML
    //busca último xml
    $texto_xml = le(busca_xml_mais_recente($sistema, $dir_raiz));
	//echo "<textarea>velho xml:".$texto_xml."</textarea>";
	
    libxml_use_internal_errors(true);
    $sxe = simplexml_load_string($texto_xml);
    if (!$sxe) {
        echo "Failed falha7 loading XML\n";
        foreach(libxml_get_errors() as $error) {
           echo "\t", $error->message;
        }
    }
    else 
    {      
	  $xml = new SimpleXMLElement($texto_xml);      
      foreach ($xml->xpath('//interacao') as $character) {
           //echo "coov".$character;
          //separa e faz a conversão de valor de cada variavel
           $coordenada = explode("_" , $character->coordenada);
           $X_hit = intval($coordenada[0]*$proporcao);
           $Y_hit =	intval($coordenada[1]*$proporcao);
           $L= intval($coordenada[2]*$proporcao);
           $H= intval($coordenada[3]*$proporcao);
           $X_caixa	= intval($coordenada[4]*$proporcao);
           $Y_caixa	= intval($coordenada[5]*$proporcao);
           $X_seta	= $coordenada[6];
           $Y_seta = $coordenada[7];        
           
           //concatena
           $nova_coordenada = $X_hit."_".$Y_hit."_".$L."_".$H."_".$X_caixa."_".$Y_caixa."_".$X_seta."_".$Y_seta;
           //echo "coon".$nova_coordenada;
           //substitui no xml         	   	
           $character->coordenada  = $nova_coordenada;         	         	
        }
    }      
    //echo "<textarea>novo xml:".$xml->asXML()."</textarea>";
    //manda remontar os arquivos com o novo xml
	remonta_xml($xml->asXML(), $sistema, $dir_raiz);
    
	
}



function redireciona ($local){
	echo "<script>location.href = '".$local."'</script>";	
}
?>

